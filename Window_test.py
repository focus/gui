# import re
import ast
import subprocess

from PySide2.QtWidgets import (QMainWindow, QApplication, QTreeWidgetItem, QStyledItemDelegate)
from PySide2.QtUiTools import QUiLoader
from PySide2 import QtCore, QtGui, QtWidgets
from PySide2.QtCore import Signal, Slot, QObject, QMutex, QRunnable, QThreadPool, QProcess, QAbstractListModel, QTimer, \
    QRect, QDateTime, QThread
from PySide2.QtGui import QIcon, QPixmap, Qt, QColor, QBrush, QPen


from GUI.windows.configurationWindow import config_win
from GUI.windows.strategieWindow import stra_win
from GUI.windows.ComponenteWindow import comp_win
from GUI.windows.profileWindow import profile_win
from GUI.windows.optimisationWindow import opt_win
from GUI.windows.file_dialog import open_file_Dialog
from GUI.cloud_database.ui_database import Cloud_Database

import GUI.imag.resource
import GUI.imag.Icon  # Icon must add
import qtmodern.windows
import sys
from GUI.compDatabase import comp_db
import GUI.windows.window_db as window_db
import os
os.chdir('/project/prosumer_modeling')
from GUI.flowchart.figure import Figure_Canvas

STATUS_WAITING="waiting"
STATUS_RUNNING = "running..."
STATUS_ERROR = "error"
STATUS_COMPLETE = "finished"

STATUS_COLORS = {
    STATUS_RUNNING: "#33a02c",
    STATUS_ERROR: "#e31a1c",
    STATUS_COMPLETE: "#b2df8a",
}

DEFAULT_STATE = {"progress": 0, "status": STATUS_WAITING}


class prosumerSignals(QObject):
    '''
    defines the signals available from a running prosumer thread
    Supported signals are:

    finished
            No data
    error
            `tuple` (exctype, value, traceback.format_exc() )
    result
            `object` data returned from processing, anything
    progress
            `int` indicating % progress
    '''
    error=Signal(str,str)
    # result=Signal(str,object)
    result=Signal(str)
    finished=Signal()
    # finished=Signal(str)
    progress = Signal(str, int)
    status = Signal(str,str)

qmut1 = QMutex()
class flowchart(QThread):
    result = Signal(str)

    def __init__(self, name):
        super().__init__()
        self.signals = prosumerSignals()
        self.scenario_name = name

    def run(self):

        flowchart = Figure_Canvas(self.scenario_name)
        self.signals.result.emit('flowchart generated')

        pass


class Prosumer(QRunnable):
    """"
    Prosumer thread
    Inherits from QRunnable to handle prosumer thread setup, signals and wrap-up.

    :param args: Arguments to pass for the prosumer
    :param kwargs: Keywords to pass for the prosumer

    """
     # Create an instance of our signals class.
    def __init__(self,command):
        super().__init__()
        self.signals = prosumerSignals()
        # self.prosumer_name=args[0]
        self.command=command
        # self.signals.status.emit(self.prosumer_name,STATUS_WAITING)

    @Slot()
    def run(self):
        qmut1.lock()
        # self.signals.status.emit(self.prosumer_name,STATUS_RUNNING)
        result=''
        try:
            output=subprocess.getoutput(self.command)
            self.signals.result.emit(output)
            self.signals.finished.emit()
        except Exception as e:
            print(e)
            # self.signals.error.emit(self.prosumer_name,str(e))
            # self.signals.status.emit(self.prosumer_name,STATUS_ERROR)
        else:
            pass
            # self.signals.result.emit(self.prosumer_name,result)
            # self.signals.status.emit(self.prosumer_name,STATUS_COMPLETE)

        # self.signals.finished.emit(self.prosumer_name)
        qmut1.unlock()


class prosumerManager(QAbstractListModel):
    """
       Manager to handle our prosumer queues and state.
       Also functions as a Qt data model for a view
       displaying progress for each prosumer.

       """
    _prosumers={}
    _state={}

    status=Signal(str)
    def __init__(self):
        super().__init__()

        # Create a threadpool for our workers.
        self.threadpool = QThreadPool()
        # self.threadpool.setMaxThreadCount(1)
        self.max_threads = self.threadpool.maxThreadCount()
        # print("Multithreading with maximum %d threads" % self.max_threads)

        self.status_timer = QTimer()
        self.status_timer.setInterval(100)
        self.status_timer.timeout.connect(self.notify_status)
        self.status_timer.start()

    def notify_status(self):
        n_prosumers = len(self._prosumers)
        running = min(n_prosumers, self.max_threads)
        waiting = max(0, n_prosumers - self.max_threads)
        self.status.emit(
            "{} prosumer running, {} waiting, {} threads".format(
                running, waiting, self.max_threads
            )
        )

    def enqueue(self, prosumer):
        """
        Enqueue a worker to run (at some point) by passing it to the QThreadPool.
        """
        prosumer.signals.error.connect(self.receive_error)
        prosumer.signals.status.connect(self.receive_status)
        prosumer.signals.progress.connect(self.receive_progress)
        prosumer.signals.finished.connect(self.done)

        self.threadpool.start(prosumer)
        self._workers[prosumer.prosumer_name] = prosumer

        # Set default status to waiting, 0 progress.
        self._state[prosumer.prosumer_name] = DEFAULT_STATE.copy()

        self.layoutChanged.emit()

    def receive_status(self, prosumer_name, status):
        self._state[prosumer_name]["status"] = status
        self.layoutChanged.emit()

    def receive_progress(self, prosumer_name, progress):
        self._state[prosumer_name]["progress"] = progress
        self.layoutChanged.emit()

    def receive_error(self, prosumer_name, message):
        print(prosumer_name, message)

    def done(self, prosumer_name):
        """
        Task/prosumer complete. Remove it from the active prosumers
        dictionary. We leave it in worker_state, as this is used to
        to display past/complete prosumers too.
        """
        del self._workers[prosumer_name]
        self.layoutChanged.emit()

    def cleanup(self):
        """
        Remove any complete/failed prosumers from worker_state.
        """
        for prosumer_name, s in list(self._state.items()):
            if s["status"] in (STATUS_COMPLETE, STATUS_ERROR):
                del self._state[prosumer_name]
        self.layoutChanged.emit()

        # Model interface

    def data(self, index, role):
        if role == Qt.DisplayRole:
            # See below for the data structure.
            prosumer_names = list(self._state.keys())
            prosumer_name = prosumer_names[index.row()]
            return prosumer_name, self._state[prosumer_name]

    def rowCount(self, index):
        return len(self._state)

class ProgressBarDelegate(QStyledItemDelegate):
    def paint(self, painter, option, index):
        # data is our status dict, containing progress, id, status
        job_id, data = index.model().data(index, Qt.DisplayRole)
        if data["progress"] > 0:
            color = QColor(STATUS_COLORS[data["status"]])

            brush = QBrush()
            brush.setColor(color)
            brush.setStyle(Qt.SolidPattern)

            width = option.rect.width() * data["progress"] / 100

            rect = QRect(option.rect)
            rect.setWidth(width)

            painter.fillRect(rect, brush)

        pen = QPen()
        pen.setColor(Qt.black)
        painter.drawText(option.rect, Qt.AlignLeft, job_id)





class mainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        # Dynamically create a corresponding window object from the UI definition
        self.ui = QUiLoader().load('GUI/ui_files/MainWindow.ui')
        # static Load the UI definition from the file
        # self.ui=Ui_Prosumer_Modeling()
        # self.ui.setupUi(self)
        #todo: design UI Main Window
        self.setup_ui()
        # test Window in multiple threading
        self.timer=QTimer()
        self.timer.timeout.connect(self.showtime)
        self.timer.start(1000)

        self.threadpool = QThreadPool()
        print(
            "Multithreading with maximum %d threads" % self.
            threadpool.maxThreadCount()
        )
        #Button Action
        self.ui.btn_save.clicked.connect(self.scenario_parameter)
        self.ui.actionStart.triggered.connect(self.run_Scenario)
        self.ui.actionStart.triggered.connect(self.test)
        self.ui.actiondatabase.triggered.connect(self.comp_database)
        self.ui.btn_clear.clicked.connect(self.clearButton) # clear result and graphic View

        # self.prosumers=prosumerManager()
        # self.prosumers.status.connect(self.statusBar().showMessage)
        # self.ui.progress.setModel(self.prosumers)
        delegate=ProgressBarDelegate()
        # self.ui.progress.setItemDelegate(delegate)

        #Todo every window as thread
        # self.config = config_win()
        self.ui.btn_Configuration.clicked.connect(self.config_btnClick)
        # self.config.config_signal.connect(self.configuration_win_open)
        # self.config.data_signal.connect(self.configuration_data)


        self.strategies = stra_win()
        self.ui.btn_Strategie.clicked.connect(self.stra_btnClick)
        self.strategies.stra_signal.connect(self.strategie_win_open)

        self.comp = comp_win()
        self.ui.btn_Components.clicked.connect(self.comp_btnClick)
        self.comp.comp_signal.connect(self.componente_win_open)
        # self.comp.comp_signal.connect(self.config.comp_signal.terminate)

        self.profile = profile_win()
        self.ui.btn_Profile.clicked.connect(self.profile_btnClick)
        self.profile.profile_signal.connect(self.profile_win_open)

        self.opt = opt_win()
        self.ui.btn_Optimization.clicked.connect(self.opt_btnClick)
        self.opt.opt_signal.connect(self.optimization_win_open)


    def test(self):
        # flowchart_thread = flowchart('heatpump_household_AC')
        # scenario = self.ui.combox_scenario.currentText()
        flowchart = Figure_Canvas('heatpump_household_AC')
        self.Imageadd()

    # Initialize the necessary widgets
    def setup_ui(self):
        self.ui.setWindowTitle('Digital Twin of Urban Energy System')
        # choose a data bank from three data bank
        '''
        default db: it is currently seen as a local database
        local data bank: waiting for developing: local file upload-->use 
        cloud data bank: open source platform： already download but currently not to be use 
        '''
        self.ui.rbtn_default_db.setChecked(True)

        self.ui.rbtn_local_db.toggled.connect(self.file_dialog)
        self.ui.rbtn_cloud_db.toggled.connect(self.cloud_db)
        if self.ui.rbtn_default_db.isChecked()==True:
            self.data_source=2

        # default db
        # self.ui.textEdit.setReadOnly(True)
        # self.ui.combox_scenario.clear()
        # scenario_list=self.get_scenario_name()
        scenario_list=['heatpump_household_AC']  # only for test
        # self.ui.combox_scenario.addItems(scenario_list)
        self.ui.prosumer_name.setPlaceholderText("Please give your prosumer name")

        # simulation time set up
        self.ui.t_start.setDate(QtCore.QDate(2019, 7, 25))
        self.ui.t_start.setTime(QtCore.QTime(0, 0, 0))
        self.ui.t_start.setDisplayFormat("yyyy-MM-dd HH:mm:ss")
        self.ui.t_start.setCalendarPopup(True)
        # self.ui.t_horizon.setValidator(QtGui.QIntValidator())
        # self.ui.t_step.setValidator(QtGui.QIntValidator())
        self.ui.t_horizon.setPlaceholderText("Please give the interval of in integer in hour ")
        self.ui.t_step.setPlaceholderText("Please give the step of time in integer in hour")

        self.ui.prosumer_name.setText("household")

        self.ui.t_horizon.setValidator(QtGui.QIntValidator())
        self.ui.t_horizon.setText("720")  # time in [h]
        self.ui.t_step.setValidator(QtGui.QIntValidator())
        self.ui.t_step.setText("1")  # time step in [h]

        # self.init_treeView()
        self.ui.treeView.setItemsExpandable(False)
        self.ui.treeView.expandAll()

        # self.ui.graphicview.setStyleSheet("background:blue")
        self.ui.status.setText("Status: {}".format(STATUS_WAITING))

        self.ui.showMaximized()
        self.ui.show()

    def showtime(self):
        time = QDateTime.currentDateTime()
        timedisplay = time.toString("yyyy-MM-dd hh:mm:ss ")
        self.ui.time.setText(timedisplay)

    def get_scenario_parameters(self):
        '''
        scenario_parameters update in database
        '''
        data_source=self.data_source
        scenario_name=self.ui.combox_scenario.currentText()
        pro_name=self.ui.prosumer_name.text()
        pro_type = self.ui.prosumer_type.currentText()
        t_start=self.ui.t_start.text()
        t_horizon=self.ui.t_horizon.text()
        t_step=self.ui.t_step.text()

        scenario_para={'data_source':data_source,
                        'scenario_name':scenario_name,
                       'prosumer_name': pro_name,
                       'prosumer_type':pro_type,
                       't_start':t_start,
                       't_horizon':t_horizon,
                       't_step':t_step,
        }
        window_db.add_df_to_sql(scenario_para, tbname='scenario_parameters')

    def get_scenario_name(self):
        scenario_list = []
        filePath = './input_files/'
        file_list = os.listdir(filePath)
        for i in file_list:
            if '.' not in i:
                scenario_list.append(i)
        return scenario_list

    def scenario_parameter(self):
        '''
        update scenario parameter to transport runme db
        '''
        self.get_scenario_parameters()

    # Running external commands & processes
    def run_Scenario(self):


        # self.ui.status.setText("Status: {}".format(STATUS_RUNNING))
        #

        # self.Imageadd()
        # p=Prosumer(scenario)
        # p.signals.result.connect(self.scenario_output)
        # p.signals.error.connect(self.scenario_output)
        # self.prosumers.enquene(p)
        # self.prosumer_thread = QProcess()\



        self.prosumer_thread = Prosumer("python runme_db.py")
        self.prosumer_thread.signals.result.connect(self.scenario_output)
        self.prosumer_thread.setAutoDelete(True)
        # self.threadpool.start(flowchart_thread)
        self.threadpool.start(self.prosumer_thread)




    # def flowchart(self, scenario):
    #     self.diagram = Figure_Canvas(scenario, width=self.ui.graphicview.width()/101,
    #                                  height=self.ui.graphicview.height()/101
    #                                  )
    #     # self.diagram = Figure_Canvas(scenario)
    #     # self.diagram.figure.clear()  # clear the old diagram
    #     self.diagram.generate(scenario)
    #     self.graphicscene = QtWidgets.QGraphicsScene()
    #     self.graphicscene.addWidget(self.diagram)
    #     self.ui.graphicview.setScene(self.graphicscene)
        # self.ui.graphicview.show()
    # def image_scenario(self):
    #     self.flowchart_thread = QProcess()
    #     scenario = 'heatpump_household_AC'
    #     self.flowchart_thread= self.flowchart(scenario)
    #     self.threadpool.start(self.flowchart_thread)

    def comp_database(self):
        self.comp_database=comp_db()
        self.comp_database.show()

    def get_image_path(self):
        scenario = self.ui.combox_scenario.currentText()
        diagram_save_path = os.getcwd() + '\\GUI\\flowchart\\pictures\\flowchart_' + scenario
        image_path = diagram_save_path + '.png'
        return image_path

    def Imageadd(self):
        image_path=self.get_image_path()
        if os.path.isfile(image_path):
            self.scene = QtWidgets.QGraphicsScene(self.ui.graphicview)
            pixmap = QPixmap(image_path)
            item = QtWidgets.QGraphicsPixmapItem(pixmap)
            self.scene.addItem(item)
            self.ui.graphicview.setScene(self.scene)

    def Imageremove(self):
        # self.Imageadd()
        image_path = self.get_image_path()
        if os.path.isfile(image_path):
            scene = QtWidgets.QGraphicsScene(self.ui.graphicview)
            pixmap = QPixmap(image_path)
            item = QtWidgets.QGraphicsPixmapItem(pixmap)
            scene.addItem(item)
            self.scene.removeItem(item)
            self.ui.graphicview.setScene(scene)

    def scenario_output(self,s):
        self.ui.textEdit.clear()
        # scenario = self.ui.combox_scenario.currentText()
        # flowchart = Figure_Canvas(scenario)
        # self.Imageadd()
        self.ui.actionStart.setEnabled(False)
        cursor = self.ui.textEdit.textCursor()
        cursor.movePosition(QtGui.QTextCursor.End)
        cursor.insertText("THREAD COMPLETE!\n")
        cursor.insertText(s)
        cursor.insertText('\n')
        self.ui.textEdit.setTextCursor(cursor)
        self.ui.textEdit.ensureCursorVisible()
        self.ui.actionStart.setEnabled(True)
        self.ui.status.setText("Status: {}".format(STATUS_COMPLETE))



    def init_treeView(self):
        # Set the number of columns
        self.ui.treeview.setColumnCount(1)
        # Set the title of the tree control header
        self.ui.treeview.setHeaderLabels([ 'Files name'])

        # Setting the root node
        root=[]
        root_namelist = ['Scenario', 'Components lib', 'Tools', 'Input Files', 'Result']
        for i in range(len(root_namelist)):
            root.append(QTreeWidgetItem(self.ui.treeview))
            root[i].setText(0, root_namelist[i])

        # Set child node 1
        child1 = QTreeWidgetItem(root[0])
        child1.setText(0, 'Prosumer')
        child1.setText(1, 'component')
        # Set child node 2
        child2 = QTreeWidgetItem(root[0])
        child2.setText(0, 'configuration')
        child2.setText(1, 'data')

        # Set child node 3
        child3 = QTreeWidgetItem(child2)
        child3.setText(0, 'xxx')
        child3.setText(1, 'xxxxx')

        #Load all properties of the root node with child controls
        for i in root:
            self.ui.treeview.addTopLevelItem(i)

    #     #
        self.ui.treeview.clicked.connect(self.onClicked)
    #
        # Nodes all expanded
        self.ui.treeview.expandAll()

    def onClicked(self, qmodeLindex):
        item = self.ui.treeview.currentItem()
        print('Key=%s,value=%s' % (item.text(0), item.text(1)))

    # clear image and textedit zone
    def clearButton(self):
        self.ui.textEdit.clear()
        self.Imageremove()


    def config_btnClick(self):
        # self.config.config_queue.put('show')
        self.prosumer_thread = Prosumer("python GUI/windows/configurationWindow.py")
        # self.prosumer_thread.signals.result.connect(self.scenario_output)
        self.prosumer_thread.setAutoDelete(True)
        # self.threadpool.start(flowchart_thread)
        self.threadpool.start(self.prosumer_thread)

    def comp_btnClick(self):
        self.comp.comp_queue.put('show')
    def stra_btnClick(self):
        self.strategies.stra_queue.put('show')
    def profile_btnClick(self):
        self.profile.profile_queue.put('show')
    def opt_btnClick(self):
        self.opt.opt_queue.put('show')

    # def initialization_win_open(self):
    #     SW.init_win = initi_win()
    #     self.windowList.append(SW.init_win)
    #     SW.init_win.ui.show()
    #configuration window @slot
    def configuration_win_open(self):
        self.config.ui.show()

    def configuration_data(self,data):
        print('ss')
        print(type(data))
        print(str(data))
        window_db.add_df_to_sql(ast.literal_eval(data))

    def strategie_win_open(self):
        self.strategies.ui.show()

    def componente_win_open(self):
        self.comp.ui.show()

    def profile_win_open(self):
        self.profile.ui.show()

    def optimization_win_open(self):
        self.opt.ui.show()

    def file_dialog(self):
        if self.ui.rbtn_default_db.isChecked() == False and \
                self.ui.rbtn_cloud_db.isChecked() == False:
            file_dialog= open_file_Dialog()
            self.windowList.append(file_dialog)
            file_dialog.ui.show()

    def cloud_db(self):
        if self.ui.rbtn_cloud_db.isChecked() == True:
            Database = Cloud_Database()
            Database.ui.show()

if __name__ == '__main__':
    # app = QApplication([])
    app = QApplication(sys.argv)
    app.setStyle('Fusion')
    # qtmodern.styles.dark(app)
    app.setWindowIcon(QIcon('images/RWTH_Logo.png'))
    main_win = mainWindow()
    qtmodern.windows.ModernWindow(main_win)
    sys.exit(app.exec_())

