import csv
import sys
import os
import pandas as pd
from PySide2 import QtCore,QtGui
from PySide2.QtCore import QSize, Qt, QSortFilterProxyModel, QThread
from PySide2.QtSql import QSqlDatabase, QSqlTableModel

from PySide2.QtUiTools import QUiLoader
from PySide2.QtWidgets import (
    QApplication,QComboBox,QDataWidgetMapper,QDoubleSpinBox,QFormLayout,
    QHBoxLayout,QLabel,QLineEdit,QMainWindow,QPushButton,QSpinBox,
    QTableView,QVBoxLayout,QWidget,
)
os.chdir('/project/prosumer_modeling')

# db = QSqlDatabase("QSQLITE")
# # db.setDatabaseName("xxx.sqlite")
# db.open()

class TableModel(QtCore.QAbstractTableModel):
    '''
    this is empty table model based on QTableView()
    and it can generate a table by adding data
    input data type: pd.DataFrame

    '''
    def __init__(self, data):
        super().__init__()
        self._data = data
    def data(self, index, role):
        if role == Qt.DisplayRole:
            value = self._data.iloc[index.row(), index.column()]
            return str(value)
    def rowCount(self, index):
        return self._data.shape[0]
    def columnCount(self, index):
        return self._data.shape[1]
    def headerData(self, section, orientation, role):
        if role == Qt.DisplayRole:
            if orientation == Qt.Horizontal:
                return str(self._data.columns[section])
        if orientation == Qt.Vertical:
            return str(self._data.index[section])

class comp_db(QMainWindow):
    def __init__(self):
        super().__init__()


        hlayout = QHBoxLayout()

        layout = QVBoxLayout()

        self.filtert = QLineEdit()
        self.filtert.setPlaceholderText("Search...")
        self.table = QTableView()

        vlayout = QVBoxLayout()
        vlayout.addWidget(self.filtert)
        vlayout.addWidget(self.table)

        # Left/right pane.
        hlayout.addLayout(vlayout)
        hlayout.addLayout(layout)

        form = QFormLayout()

        self.component=QLabel()

        self.sector = QComboBox()
        self.type = QComboBox()

        self.type.setSizeAdjustPolicy(QComboBox().AdjustToContents)
        self.models= QComboBox()
        # self.type.addItems(["StandardACGrid"])
        # self.models.addItems(["GRD1"])

        form.addRow(QLabel("Component"), self.component)
        form.addRow(QLabel("Sector"), self.sector)
        form.addRow(QLabel("Equipment"), self.type)
        form.addRow(QLabel("Model"), self.models)

        filePath = './Model_library/Component/model'
        component_models = os.listdir(filePath)
        component_sector=['please choose']
        for i in component_models:
            if '_components' in i and 'EMS' not in i:
                i=i.upper().split('_COMPONENTS')[0]
                component_sector.append(i)
        self.sector.addItems(component_sector)


        self.sector.currentIndexChanged.connect(self.component_type)
        self.type.currentIndexChanged.connect(self.component_model)
        self.models.currentIndexChanged.connect(self.datatable)

        self.proxy_model = QSortFilterProxyModel()
        # self.proxy_model.setSourceModel(self.model)
        self.proxy_model.sort(1, Qt.AscendingOrder)
        self.proxy_model.setFilterKeyColumn(-1) # all columns
        self.table.setModel(self.proxy_model)

        # Update search when filter changes.
        self.filtert.textChanged.connect(self.proxy_model.setFilterFixedString)

        self.mapper = QDataWidgetMapper()
        self.mapper.setModel(self.proxy_model)

        # Change the mapper selection using the table.
        self.table.selectionModel().currentRowChanged.connect(self.mapper.setCurrentModelIndex)

        self.mapper.toFirst()

        self.setMinimumSize(QSize(800, 400))

        controls = QHBoxLayout()

        layout.addLayout(form)
        layout.addLayout(controls)

        widget = QWidget()
        widget.setLayout(hlayout)
        self.setCentralWidget(widget)
        self.setGeometry(600,600,1600,800)
    def component_type(self):
        self.type.clear()
        choose_component_sector = self.sector.currentText().lower()+'_components'
        filePath = 'Model_library/Component/model/'+choose_component_sector


        component_type_order= os.listdir(filePath)


        # print(component_type_order)
        component_type_order.remove('__pycache__')
        component_type_order.remove('__init__.py')
        component_type = []
        if '.py' in component_type_order[0]:
            component_type_order = component_type_order[0].split('.py')[0]
            self.type.addItems([component_type_order])
        else:
            for i in component_type_order:
                filePath_order =filePath+'/'+ i
                component_type_item=os.listdir(filePath_order)
                component_type_item.remove('__init__.py')
                component_type_item.remove('__pycache__')

                if '.py' not in component_type_item[0]:
                    filePath_order=filePath_order+'/'+component_type_item[0]
                    component_type_item_2 = os.listdir(filePath_order)
                    component_type_item_2.remove('__init__.py')
                    component_type_item_2.remove('__pycache__')
                    for i in component_type_item_2:
                        i=i.split('.py')[0]
                        component_type.append(i)
                else:
                    for i in component_type_item:
                        i=i.split('.py')[0]
                        component_type.append(i)
            self.type.addItems(component_type)

    def component_model(self):
        choose_component_type=self.type.currentText()
        self.models.clear()
        try:
            filePath = './Model_library/Component/data/'+choose_component_type
            component_model_order = os.listdir(filePath)
        except :
            self.models.addItems(["no data"])
        else:

            component_model=[]
            for i in component_model_order:
                i = i.split('.csv')[0]
                component_model.append(i)
            self.models.addItems(component_model)

    def datatable(self):

        choose_component_type = self.type.currentText()
        choose_component_model = self.models.currentText()
        path='Model_library/Component/data/'+choose_component_type+'/'+choose_component_model+'.csv'
        try:
            csv_data=pd.read_csv(path)
            data=pd.DataFrame(csv_data)
            # print(data)
            self.datamodel=TableModel(data)
            self.table.setModel(self.datamodel)
        except:
            pass


if __name__ == '__main__':
    app = QApplication([])
    window = comp_db()
    window.show()
    sys.exit(app.exec_())
