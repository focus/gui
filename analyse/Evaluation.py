
import sys
from PySide2 import QtWidgets, QtCore
from PySide2.QtGui import QIcon, Qt
import pandas as pd
from PySide2.QtWidgets import QMainWindow, QApplication, QMessageBox, QHeaderView, QGraphicsScene
from PySide2.QtUiTools import QUiLoader

import numpy as np
import qtmodern.styles
from MyFigureCanvas import MyFigureCanvas
import os
os.chdir('/project/prosumer_modeling')
path=os.getcwd()
import analyse_result as analyResult
filename=path+'/output_files/results_0.xlsx'
result= pd.read_excel(filename)
colname=list(result)
comp_size, comp_cost, comp_waterstorage, comp_therm_cns, comp_elec_cns, comp_heatpump, pv_inv = analyResult.get_result_data(colname, result)

class TableModel(QtCore.QAbstractTableModel):
    def __init__(self, data,HEADERS):
        super(TableModel, self).__init__()
        self._data = data
        self.headers = HEADERS

    def data(self, index, role):
        if role == Qt.DisplayRole:
            # See below for the nested-list data structure.
            # .row() indexes into the outer list,
            # .column() indexes into the sub-list
            return self._data[index.row()][index.column()]

    def rowCount(self, index):
        # The length of the outer list.
        return len(self._data)

    def columnCount(self, index):
        # The following takes the first sub-list, and returns
        # the length (only works if all rows are an equal length)
        return len(self._data[0])
    def headerData(self, section, orientation, role=Qt.DisplayRole):
        if role!=Qt.DisplayRole:
            return None
        if orientation!=Qt.Vertical:
            return self.headers[section]
        return int(section+1)

class Evaluation(QMainWindow):
    def __init__(self):
        super().__init__()
        # Dynamically create a corresponding window object from the UI definition
        self.ui = QUiLoader().load('GUI/ui_files/Evaluation.ui')

        self.setup_ui()

    def setup_ui(self):
        self.ui.cb_energyDemand.stateChanged.connect(self.energyDemand)
        self.ui.cb_size.stateChanged.connect(lambda: self.sizeView(comp_size))
        self.ui.cb_cost.stateChanged.connect(lambda: self.costView(comp_cost))
        self.ui.cb_energy_cns.stateChanged.connect(lambda: self.energy_cns(comp_therm_cns, comp_elec_cns))
        # self.ui.cb_heatpump.stateChanged.connect(self.hpflow)
        # self.ui.cb_chp.stateChanged.connect(self.chpflow)
        self.ui.cb_peak.stateChanged.connect(self.peakPower)
        self.ui.cb_gridnet.stateChanged.connect(self.gridnet)
        self.ui.cb_CO2e.stateChanged.connect(self.CO2_emission)
        self.ui.action_Result.triggered.connect(lambda: self.openResult)

        self.ui.setWindowIcon(QIcon('GUI/imag/RWTH_Logo.png'))
        self.ui.setWindowTitle('Evaluation of simulation scenario')
        self.ui.showMaximized()
        self.ui.show()

    def CO2_emission(self):
        CO2_emission= analyResult.calculate_CO2_emission(colname, result)
        data, sum_co2 = analyResult.compCost(CO2_emission)
        # print(data)
        headers = ['', ' Annual emissions (t/a)']
        self.datamodel = TableModel(data, headers)
        self.ui.tb_CO2e.setModel(self.datamodel)
        self.ui.tb_CO2e.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.ui.sum_CO2.setText(str(sum_co2))

    def gridnet(self):
        grid_cost,grdnet = analyResult.get_grid_demand(colname, result)
        data = analyResult.data_format_Intb(grdnet)
        headers = ['Import/Export grid net', ' Value in kWh']
        # print(data)
        self.datamodel = TableModel(data, headers)
        self.ui.tb_gridnet.setModel(self.datamodel)
        self.ui.tb_gridnet.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)


    def hpflow(self):
        hp_flow,chp_flow=analyResult.get_hp_chp(colname,result)
        data = analyResult.data_format_Intb(hp_flow)
        headers = ['Energy flow', ' Value in kWh']
        self.datamodel = TableModel(data, headers)
        self.ui.tb_heatpump.setModel(self.datamodel)
        self.ui.tb_heatpump.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
    def chpflow(self):
        hp_flow,chp_flow=analyResult.get_hp_chp(colname,result)
        data = analyResult.data_format_Intb(chp_flow)
        headers = ['Energy flow', ' Value in kWh']
        self.datamodel = TableModel(data, headers)
        self.ui.tb_chp.setModel(self.datamodel)
        self.ui.tb_chp.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)

    def peakPower(self):
        # PVinv_flow=analyResult.get_PV(colname,result)
        # data = analyResult.data_format_Intb(PVinv_flow)
        # headers = ['PV flow', ' Value in kWh']
        # self.datamodel = TableModel(data, headers)
        # self.ui.tb_pv.setModel(self.datamodel)
        # self.ui.tb_pv.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        # peak power
        grid_cost, grdnet, power_peak = analyResult.get_grid_demand(colname, result)
        data = analyResult.data_format_Intb(power_peak)
        headers = ['Peak power grid', ' Value in kW(h)']
        self.datamodel = TableModel(data, headers)
        self.ui.tb_peak.setModel(self.datamodel)
        self.ui.tb_peak.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)

        # annual_energy_demand, hourly_energy_demand = analyResult.get_energy_demand(colname, result)
        # PVinv_flow['']/annual_energy_demand['elec_demand']

    def energyDemand(self):
        annual_energy_demand,hourly_energy_demand=analyResult.get_energy_demand(colname,result)
        data = analyResult.data_format_Intb(annual_energy_demand)
        headers = ['Energy Type', 'Annual Energy Demand in kWh']
        self.datamodel = TableModel(data, headers)
        self.ui.tb_energyDemand.setModel(self.datamodel)
        self.ui.tb_energyDemand.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.graphicView_energydmd(hourly_energy_demand)

    def graphicView_energydmd(self,hourly_energy_demand):
        figure=MyFigureCanvas()
        figure.draw_energyDemand(hourly_energy_demand)
        self.GraphicsScene(figure)


    def energy_cns(self,comp_therm_cns, comp_elec_cns):
        therm_data = analyResult.data_format_Intb(comp_therm_cns)
        HEADERS = ['Energy Flow', 'Annual Energy in MWh/a']
        therm_datamodel = TableModel(therm_data,HEADERS)
        self.ui.tb_heatdmd.setModel(therm_datamodel)
        self.ui.tb_heatdmd.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        elec_data = analyResult.data_format_Intb(comp_elec_cns)
        elec_datamodel = TableModel(elec_data,HEADERS)
        self.ui.tb_elecdmd.setModel(elec_datamodel)
        self.ui.tb_elecdmd.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.graphicView_cns_energydmd()

    def graphicView_cns_energydmd(self):
        figure=MyFigureCanvas()
        figure.draw_pie_cns()
        self.GraphicsScene(figure)

    def sizeView(self,comp_size):
        # table View
        data = analyResult.data_format_Intb(comp_size)
        headers=['Equipment', 'Capacity in kW']
        self.datamodel = TableModel(data,headers)
        self.ui.tb_size.setModel(self.datamodel)
        self.ui.tb_size.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        # graphic View
        self.graphicView_size()

    def graphicView_size(self):
        figure=MyFigureCanvas()
        # The loaded graphics (FigureCanvas) cannot be placed directly into the graphicview control.
        # It must be put into the graphicScene first, and then the graphicscene into the graphicview
        figure.draw_sizeView()
        self.GraphicsScene(figure)

    def GraphicsScene(self,figure):
        # create a QGraphicsScene
        self.graphic_scene = QGraphicsScene()
        # Put the graphics into the QGraphicsScene,
        # note: the graphics are put into the QGraphicsScene as a QWidget
        self.graphic_scene.addWidget(figure)
        # put QGraphicsScene in QGraphicsView
        self.ui.graView.setScene(self.graphic_scene)
        self.ui.graView.show()


    def costView(self,comp_cost):
        data,sum_cots = analyResult.compCost(comp_cost)
        # data = pd.DataFrame(csv_data)
        headers=['Equipment', 'Annual cost in € ']
        self.datamodel = TableModel(data,headers)
        self.ui.tb_cost.setModel(self.datamodel)
        self.ui.tb_cost.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.graphicView_cost()
        self.ui.sum_cost.setText(str(sum_cots))

    def graphicView_cost(self):
        figure=MyFigureCanvas()
        figure.draw_costView()
        self.GraphicsScene(figure)

    def openResult(self):
        result_file_path=os.getcwd()+'/output_files/results_0.xlsx'
        # print(result_file_path)
        if os.path.exists(result_file_path):
            os.startfile(result_file_path)



if __name__ == '__main__':
    # app = QApplication([])
    app = QApplication(sys.argv)
    app.setStyle('Fusion')
    # qtmodern.styles.dark(app)
    main_win = Evaluation()
    sys.exit(app.exec_())
