import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
matplotlib.use("Qt5Agg")
import analyse_result as analyResult
os.chdir('/project/prosumer_modeling')
path = os.getcwd()
filename = path + '/output_files/results_0.xlsx'
result = pd.read_excel(filename)
colname = list(result)
comp_size, comp_cost, comp_waterstorage, comp_therm_cns, comp_elec_cns, comp_heatpump, pv_inv = analyResult.get_result_data(colname,
                                                                                                               result)

class MyFigureCanvas(FigureCanvas):
    '''
    By inheriting the FigureCanvas class,
    this class is both a PyQt5 Qwidget and a FigureCanvas for matplotlib.
    '''

    def __init__(self, parent=None, width=15, height=10,  dpi=100):
        # Create a figure and tight_layout used to remove the white space on both sides of the drawing
        self.fig = plt.Figure(figsize=(width, height), dpi=dpi, tight_layout=True)

        FigureCanvas.__init__(self, self.fig)
        self.setParent(parent)

    def draw_sizeView(self):
        self.axes = self.fig.add_subplot(111)
        title = 'The optimal size of components'
        ylabel = 'Capacity in kW'
        self.draw_bar_diagram(comp_size, title, ylabel)

    def draw_energyDemand(self,hourly_energy_demand):
        # electricity
        self.axes = self.fig.add_subplot(211)
        title = 'Annual hourly electricity energy demand'
        ylabel = 'Electricity in kWh'
        self.draw_line_diagram(hourly_energy_demand['elec_demand'], title, ylabel)
        # heating
        self.axes = self.fig.add_subplot(212)
        title = 'Annual hourly heat energy demand'
        ylabel = 'Heating in kWh'
        self.draw_line_diagram(hourly_energy_demand['therm_demand'], title, ylabel,sign='ro-')

    def draw_line_diagram(self,data, title, ylabel,sign='g+-'):
        t=range(1,8761)
        self.axes.plot(t, data,sign)
        self.axes.set_title(title, fontweight="bold", fontsize=18)
        self.axes.set_ylabel(ylabel, fontweight="bold", fontsize=16)
        self.axes.grid(True)


    def comp(self,comp_size):
        bars = list(comp_size)
        comp_name = []
        for i in bars:

            x = i.strip("\')")
            if 'power' in x:
                x = x.strip("(\'power\',\'")
            elif 'cap' in x:
                x = x.strip("('cap','")
            comp_name.append(x)
        return comp_name

    def draw_bar_diagram(self,comp_size, title, ylabel):
        # mplStyle.use("classic")
        comp_name = self.comp(comp_size)
        bars = tuple(comp_name)
        y_pos = np.arange(len(bars))+1
        y_pos[0] = 1
        # print(y_pos)
        height = list(comp_size.values())
        # print(height)
        self.axes.bar(y_pos, height, align='center')


        self.axes.set_xticks([r+1 for r in range(len(y_pos))],minor=False)
        # self.fig.set_xticks([r for r in range(len(bars))],
        #            comp_name, rotation=angle, size=10)
        # Text on the top of each bar
        for i in range(len(bars)):
            self.fig .text(x=y_pos[i] - 0.2, y=height[i] + 1, s=height[i], size=10)
        self.axes.set_title(title,fontweight ="bold", fontsize = 18)
        self.axes.set_ylabel(ylabel, fontweight ="bold", fontsize = 16)
        self.axes.grid(axis="y", linestyle='--')
        self.axes.spines['top'].set_visible(False)  # Remove the top horizontal line
        self.axes.spines['right'].set_visible(False)  # Remove the horizontal line on the right side of the drawing
        # pprint(comp_size)
        # plt.show()

    def draw_costView(self):
        self.axes = self.fig.add_subplot(111)
        title = 'Annual cost view of scenario'
        ylabel = 'Cost in €/a'
        self.draw_bar_diagram(comp_cost, title, ylabel)

    def draw_pie_diagram(self,comp2therm_cns, title):
        # fig, ax = plt.subplots(figsize=(6, 3), subplot_kw=dict(aspect="equal"))
        comp_therm_cns = {}
        for key, value in comp2therm_cns.items():
            if str(value) != '0':
                comp_therm_cns[key] = value

        data = list(comp_therm_cns.values())
        ingredients = list(comp_therm_cns.keys())

        def func(pct, allvals):
            absolute = int(np.round(pct / 100. * np.sum(allvals)))
            return "{:.1f}%".format(pct, absolute)

        wedges, texts, autotexts = self.axes.pie(data, autopct=lambda pct: func(pct, data),
                                          textprops=dict(color="w"))
        self.axes.legend(wedges, ingredients,
                  title="Ingredients",
                  title_fontsize=18,
                  loc="center left",
                  fontsize=16,
                  bbox_to_anchor=(1, 0, 0.5, 1))
        plt.setp(autotexts, size=16, weight="bold")
        self.axes.set_title(title,fontweight ="bold", fontsize = 18)
        # pprint(comp2therm_cns)
        # plt.show()

    def draw_pie_cns(self):
        self.axes = self.fig.add_subplot(211)
        title = 'Annual heat demand '
        self.draw_pie_diagram(comp_therm_cns, title)
        self.axes = self.fig.add_subplot(212)
        title = 'Annual electricity demand '
        self.draw_pie_diagram(comp_elec_cns, title)



if __name__ == '__main__':
    fig=MyFigureCanvas()

