import os, xlrd
from pprint import pprint
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.style as mplStyle
import pandas as pd


os.chdir('/project/prosumer_modeling')
path = os.getcwd()
filename = path + '/output_files/results_0.xlsx'
result = pd.read_excel(filename)
colname = list(result)

months_period={'Jan':31,'Feb':28,'Mar':31,'Apr':30,'May':31,'Jun':30,
               'Jul':31,'Aug':31,'Sep':30,'Octo':31,'Nov':30,'Dec':31}



def get_hp_chp(colname,df):
    components = ['Heat Pump', 'CHP']
    hp_flow={}
    chp_flow = {}
    for i in components:
        for comp in colname:
            if 'heat_pump' in comp:
                hp_flow[comp]=round(df[comp].sum())
            if "bhkw" in comp:
                chp_flow[comp]=round(df[comp].sum())

    return hp_flow,chp_flow


def get_PV(colname,df):
    PVinv_flow={}
    for x in colname:
        # if 'pv_roof' in i and 'inv_pv' in i:
        #     PVinv_flow[i]=round(df[i].sum())
        if "'cap'" in x and "battery" in x:
            cap_battery = round(df[x].sum())
            if cap_battery==0:
                for i in colname:
                    if 'inv_pv' in i and 'elec_cns' in i:
                        PVinv_flow[i] = round(df[i].sum())
                    if 'inv_pv' in i and 'grd' in i:
                        PVinv_flow[i] = round(df[i].sum())
                    if 'inv_pv' in i and 'heat_pump' in i:
                        PVinv_flow[i] = round(df[i].sum())

    return PVinv_flow

def get_energy_demand(colname,df):
    energy_type = ['elec_demand', 'therm_demand','cooling_demand','Hydrogen']
    energy_demand={}
    hourly_energy_demand={}
    for i in energy_type:
        if i in colname:
            energy_demand[i]=round(df[i].sum())
            hourly_energy_demand[i]=df[i].tolist()

    return energy_demand,hourly_energy_demand

# energy_demand,hourly_energy_demand=get_energy_demand(colname,result)
# # pprint(energy_demand)
# pprint(hourly_energy_demand)

def get_power_peak(hourly_grid):
    power_peak=0
    power_list=[]
    for i in list(hourly_grid.keys()):
        if len(power_list) == 0:
            power_list=hourly_grid[i]
        else:
            power_list = np.sum([power_list, hourly_grid[i]], axis=0).tolist()
            # power_list = np.array(power_list) + np.array(i)

    power_peak = max(power_list)

    return power_peak



def get_grid_demand(colname, df):
    grid_type = ['elec_grid', 'therm_grid', 'gas_grid']
    grid_cost = {}
    gridnet={}
    power_peak = {}
    elec_grid = []
    hourly_elec_grid={}
    for i in colname:
        if "grd" in i and 'elec_cns' in i:
            elec_grid.append(round(df[i].sum()))
            hourly_elec_grid[i] = df[i].tolist()
        if "grd" in i and 'heat_pump' in i:
            elec_grid.append(round(df[i].sum()))
            hourly_elec_grid[i] = df[i].tolist()
    elec_power_peak=get_power_peak(hourly_elec_grid)
    gridnet['Electricity import']=sum(elec_grid)
    grid_cost['elec_grid'] = sum(elec_grid) * (1.0025 / 100 + 0.2885) + 785.4 + 16.66 * elec_power_peak
    power_peak['elec power peak'] = elec_power_peak

    gas_grid=[]

    hourly_gas_grid = {}
    for i in colname:
        if "gas_grd" in i  and 'bhkw' in i:
            gas_grid.append(round(df[i].sum()))
            hourly_gas_grid[i] = df[i].tolist()
        if "gas_grd" in i  and 'boild' in i:
            gas_grid.append(round(df[i].sum()))
            hourly_gas_grid[i] = df[i].tolist()
    gas_power_peak = get_power_peak(hourly_gas_grid)
    gridnet['gas import'] = sum(gas_grid)
    grid_cost['gas_grid'] = sum(gas_grid)*(0.525/100+0.0672)+117.81*12+21.42*gas_power_peak
    power_peak['gas power peak']=gas_power_peak
    heat_grid=0
    hourly_heat_grid = {}
    for i in colname:
        if 'heat_grid' in i and 'cns' in i:
            heat_grid=round(df[i].sum())
            hourly_heat_grid[i]=df[i].tolist()
            heat_power_peak = get_power_peak(hourly_heat_grid)
            power_peak['heat power peak'] = heat_power_peak
    gridnet['heat import'] = heat_grid
    grid_cost['heat_grid'] = heat_grid*(0.09+0.86/100)

    export_chp_grd=0
    for i in colname:
        if 'bhkw' in i and 'grd' in i and ('gas' not in i):
            export_chp_grd=round(df[i].sum())
        grid_cost["export_chp_grd"] = export_chp_grd*(-0.1115)
    gridnet['CHP export electricity'] = export_chp_grd
    export_elec_grid = 0
    for i in colname:
        if 'inv_pv' in i and 'grd' in i:
            export_elec_grid=round(df[i].sum())
        grid_cost["export_PVinv_grd"] = export_elec_grid*(-0.1115)
    gridnet['PVinv export electricity'] = export_elec_grid

    return grid_cost,gridnet,power_peak






def calculate_CO2_emission(colname, df):
    grid_cost,gridnet,power_peak=get_grid_demand(colname, df)
    CO2e={}
    for  key, value in gridnet.items():
        if key=='Electricity import' :
            CO2e[key]=value*401/1000000
        if key=='PVinv export electricity' or key=='CHP export electricity':
            CO2e[key] = value * (-401) / 1000000
        if key=='heat import':
            CO2e[key]=value*344/1000000
        if key=='gas import':
            CO2e[key]=value*210/1000000
    return CO2e



def get_result_data(colname, df):
    # result = dict.fromkeys(['comp_size','comp_cost','comp_waterstorage','comp_therm_cns'], {})
    comp_size = {}
    annual_cost = {}
    comp_waterstorage = {}
    comp_therm_cns = {}
    comp_elec_cns = {}
    comp_heatpump = {}
    pv_inv = {}

    for i in colname:
        # print(i)
        # size of components
        if "'power'" in i:
            comp_size[i] = round(df.loc[0, i])
        elif "'cap'" in i:
            comp_size[i] = round(df.loc[0, i])
        # annual cost of components
        elif 'annual_cost' in i:
            annual_cost[i] = round(df.loc[0, i])
        elif 'heat_pump' in i:
            comp_heatpump[i] = round(df[i].sum())
        elif ("pv_roof" in i) and ('inv_pv_bat' in i):
            pv_inv[i] = round(df[i].sum())
        # energy flow from comp to hot water storage
        elif ('water_tes' in i) \
                and ('annual_cost' not in i) \
                and ('energy' not in i) \
                and ('therm_cns' not in i) \
                and ('init_soc' not in i) \
                and i != 'water_tes':
            water_energy_flow = round(df[i].sum())
            comp_waterstorage[i] = water_energy_flow
        # energy flow from comp to thermal consumption
        elif "'therm_cns')" in i and ('therm_dmd' not in i):
            therm_energy_flow = round(df[i].sum())
            comp_therm_cns[i] = therm_energy_flow
        # energy flow from comp to electric consumption
        elif "'elec_cns')" in i and 'elec_dmd' not in i:
            elec_energy_flow = round(df[i].sum() )
            comp_elec_cns[i] = elec_energy_flow

        else:
            pass

        grid_cost,grdnet,power_peak=get_grid_demand(colname, df)
        for i in list(grid_cost.keys()):
            annual_cost[i] = grid_cost[i]

    return comp_size, annual_cost, comp_waterstorage, comp_therm_cns, comp_elec_cns, \
           comp_heatpump, pv_inv


comp_size, annual_cost, comp_waterstorage, comp_therm_cns, comp_elec_cns, comp_heatpump, pv_inv = get_result_data(colname,
                                                                                                               result)
# print('optimal component size : in kW')
# pprint(comp_size)
# print('\n')
# print('annual component cost: in €/a')
# pprint(comp_cost)
# print('\n')
# print('energy flow of component to hot water storage: in kW')
# pprint(comp_waterstorage)
# print('\n')
# print('energy flow of component to thermal consumption: in kW')
# pprint(comp_therm_cns)
# print('\n')
# print('energy flow of component to electric consumption: in kW')
# pprint(comp_elec_cns)
# print('\n')
# print('energy flow of component to heat pump: in kW')
# pprint(comp_heatpump)

# def components_therm_cns(comp_therm_cns,comp_waterstorage):
#     In_storage=sum(list(comp_waterstorage.values()))
#     out_storage=comp_therm_cns["('water_tes', 'therm_cns')"]
#     for In_i , v in comp_waterstorage.items():
#         for out_i, z in comp_therm_cns.items():
#             if
#
#     print('energy flow of component to hot water storage: in kW')
#     pprint(comp_waterstorage)
#     print('\n')
#     print('energy flow of component to thermal consumption: in kW')
#     pprint(comp_therm_cns)
#     print('\n')
# components_therm_cns(comp_therm_cns, comp_waterstorage)

def data_format_Intb(comp_size):
    data=[]
    for key, value in comp_size.items():
        line=[]
        line.append(key)
        line.append(value)
        data.append(line)

    # data = pd.DataFrame(comp_size,index=[0]).T
    return data

# size = compSize(comp_size)
# print(size)
def compCost(comp_cost):
    data=[]
    sum_cots=0
    for key, value in comp_cost.items():
        line=[]
        line.append(key)
        line.append(value)
        data.append(line)
        sum_cots+=value

    # data = pd.DataFrame(comp_size,index=[0]).T
    return data,int(sum_cots)

def draw_elec_therm_demand(colname, df):
    elec_demand = []
    timeseries = []
    therm_demand = []
    for i in colname:
        if i == 'elec_demand':
            elec_demand = list(df[i])
        elif i == 'TimeStep':
            timeseries = list(df[i])
        elif i == 'therm_demand':
            therm_demand = list(df[i])
        else:
            pass
    # use the plot function
    fig, (ax1, ax2) = plt.subplots(2, 1)
    fig.subplots_adjust(hspace=0.5)

    x = np.arange(0, len(elec_demand)) + 1
    x[0] = 1
    ax1.plot(x, elec_demand)
    # ax1.spines['top'].set_visible(False)
    # ax1.spines['right'].set_visible(False)
    # ax1.spines['left'].set_visible(False)
    ax1.set_title('Annual electricity demand of swimming pool in 2019')
    ax1.set_xlabel('Hour of the year')
    ax1.set_ylabel('Inport from elec. grid in kW')

    ax1.grid(axis="y", linestyle='--')
    # ax1.grid(True)

    ax2.plot(therm_demand)
    ax2.set_title('Annual heat demand of swimming pool in 2019')
    ax2.set_xlabel('Hour of the year')
    ax2.set_ylabel('Inport from heat grid in kW')
    ax2.grid(axis="y", linestyle='--')

    plt.show()


def comp(comp_size):
    bars = list(comp_size)
    comp_name = []
    for i in bars:

        x = i.strip("\')")
        if 'power' in x:
            x = x.strip("(\'power\',\'")
        elif 'cap' in x:
            x = x.strip("('cap','")
        comp_name.append(x)
    return comp_name


# comp_name=comp(comp_size)
# print(comp_name)
def draw_bar_diagram(comp_size, title, ylabel, angle=90):
    # mplStyle.use("classic")
    fig, ax = plt.subplots()
    comp_name = comp(comp_size)
    bars = tuple(comp_name)
    y_pos = np.arange(len(bars))
    height = list(comp_size.values())
    ax.bar(y_pos, height, align='center')
    # Text below each barplot with a rotation at 90°
    plt.xticks([r for r in range(len(bars))],
               comp_name, rotation=angle, size=10)
    # Text on the top of each bar
    for i in range(len(bars)):
        plt.text(x=y_pos[i] - 0.2, y=height[i] + 1, s=height[i], size=10)
    ax.set_title(title)
    ax.set_ylabel(ylabel, size=8)
    ax.grid(axis="y", linestyle='--')
    pprint(comp_size)
    plt.show()


def draw_pie_diagram(comp2therm_cns, title):
    fig, ax = plt.subplots(figsize=(6, 3), subplot_kw=dict(aspect="equal"))
    comp_therm_cns = {}
    for key, value in comp2therm_cns.items():
        if str(value) != '0':
            comp_therm_cns[key] = value

    data = list(comp_therm_cns.values())
    ingredients = list(comp_therm_cns.keys())

    def func(pct, allvals):
        absolute = int(np.round(pct / 100. * np.sum(allvals)))
        return "{:.1f}%".format(pct, absolute)

    wedges, texts, autotexts = ax.pie(data, autopct=lambda pct: func(pct, data),
                                      textprops=dict(color="w"))
    ax.legend(wedges, ingredients,
              title="Ingredients",
              loc="center left",
              bbox_to_anchor=(1, 0, 0.5, 1))
    plt.setp(autotexts, size=6, weight="bold")
    ax.set_title(title)
    pprint(comp2therm_cns)
    plt.show()


# https://www.tutorialspoint.com/matplotlib/matplotlib_bar_plot.htm
def draw_comp_size(comp_size):
    title = 'The optimal size of components'
    ylabel = 'Size in kW'
    draw_bar_diagram(comp_size, title, ylabel)

def draw_comp_cost(comp_cost):
    title = 'Annual cost view of scenario'
    ylabel = 'Cost in €/a'
    draw_bar_diagram(comp_cost, title, ylabel)


def draw_comp_therm_cns(comp_therm_cns):
    title = 'Annual heat demand '
    ylabel = 'MWh/a'
    draw_bar_diagram(comp_therm_cns, title, ylabel, angle=90)


def draw_pie_comp_therm_cns(comp_therm_cns):
    title = 'Annual heat demand in MWh/a'
    draw_pie_diagram(comp_therm_cns, title)


def draw_comp_elec_cns(comp_elec_cns):
    title = 'Annual electricity demand '
    ylabel = 'MWh/a'
    draw_bar_diagram(comp_elec_cns, title, ylabel, angle=90)


def draw_pie_comp_elec_cns(comp_elec_cns):
    title = 'Annual heat demand in MWh/a'
    draw_pie_diagram(comp_elec_cns, title)

# draw_elec_therm_demand(colname,result)

# draw_comp_size(comp_size)
# draw_comp_cost(comp_cost)

# draw_comp_therm_cns(comp_therm_cns)
# draw_pie_comp_therm_cns(comp_therm_cns)

# draw_comp_elec_cns(comp_elec_cns)
# draw_pie_comp_elec_cns(comp_elec_cns)
