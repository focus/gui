import sys
import numpy as np
import matplotlib
from PySide2.QtWidgets import QApplication

matplotlib.use("Qt5Agg")
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
import matplotlib.pyplot as plt
from matplotlib.offsetbox import OffsetImage, AnnotationBbox
import warnings
warnings.filterwarnings('ignore')

import PIL
from PIL import Image

import os
# from flowchart.matrix_Transformation.matrix_transfor import Matrix_transfor
import GUI.flowchart.icon as icons
from GUI.flowchart.matrix_transfor import Matrix_transfor


# add connection with lines and arrow
def line_style(ax,point_group,arrows,color='k',sA=2,sB=30):
    x=point_group[0]
    y=point_group[1]
    ax.annotate('',
                xy=(x[0], x[1]), xycoords='data',
                xytext=(y[0], y[1]), textcoords='data',
                size=5, va="center", ha="center",
                # bbox=dict(boxstyle="round,pad=3", fc="w"),
                fontsize=16,
                arrowprops=dict(arrowstyle=arrows,lw=3,color=color,
                                shrinkA=sA, shrinkB=sB,
                                patchA=None, patchB=None,
                                ),
                )

class Figure_Canvas(FigureCanvas):
    # def __init__(self,scenario):
    def __init__(self,scenario,parent=None,width=18,height=8,dpi=100): # dpi：resolution
        self.figure=plt.Figure(figsize=(width,height),dpi=dpi,tight_layout=True)
        FigureCanvas.__init__(self,self.figure)
        self.setParent(parent)
        self.generate(scenario)

        #  Save image
        save_figure_path = os.getcwd() + '\\GUI\\flowchart\\pictures\\flowchart_'+scenario

        self.figure.savefig(save_figure_path+'.png')


    def generate(self,scenario):
        flow, sector, comp_type,max_x,max_y=self.get_comp_flow(scenario)
        axes=self.init_diagram(max_x, max_y)
        sector_comp_coord,sector_coord=self.get_comp_coordinate(flow, sector, comp_type, max_y)
        self.get_comp_icon(axes, sector_comp_coord)
        self.comp_connection( axes, sector_coord, max_y)

    # TODO get the sector with flow group
    def get_comp_flow(self,scenario):

        flow,comp_type =self.listdir(scenario)

        sector=list(flow.keys())
        sector_number = len(flow)
        '''
        Electricity|thermal | gas
                    xxx|xxx|xxx
                    xxx|xxx|xxx
                    xxx|xxx|xxx
        X and Y axis coordinate
            - max X=sector_number *3
            - max_y=the number of components in single sector  + 3(reserved empty position)
        '''
        max_x=sector_number *3
        sumMaxComp_sector = self.max_sum_comp_sector(comp_type,sector)
        max_y=sumMaxComp_sector+3
        return flow, sector, comp_type,max_x,max_y

    # TODO add figure and add coordinate point
    def init_diagram(self,max_x_value,max_y_value):
        axes = self.figure.add_subplot(1, 1, 1)
        axes.set_title('The Flow Chart of '+scenario,fontsize=20,color= 'blue', fontweight='bold',y= 1.1)
        self.diagram_coordinate(axes,max_x_value,max_y_value)
        return axes

    def get_comp_coordinate(self,flow,sector,comp_type,max_y_value):
        # TODO every flow group with coordinate point
        # 1. every flow group with coordinate point for every sector
        # 2. find and remove common coordinate point among the sector,
                # retain only one coordinate point for every component
        sector_coord={}
        sector_comp_coord={}
        sector_common_comp={}
        for z in sector:
            comp_group=flow[z]
            c2b=comp_group['comp_to_bar']
            b2c=comp_group['bar_to_comp']
            c2c=comp_group['comp_to_comp']
            sector_index = sector.index(z)+1
            for i in comp_type[z]:
                if i in list(sector_comp_coord.keys()):
                    sector_common_comp[i]=sector_comp_coord[i]
            comp_coord,coordinate=self.comp_coord(c2b, b2c, c2c, max_y_value-1, sector_index, sector_common_comp,z)
            sector_comp_coord.update(comp_coord)
            sector_coord[z]=coordinate
        return sector_comp_coord,sector_coord

    def get_comp_icon(self,axes, sector_comp_coord):
        # 3. define and deal with special component
        # TODO add Icon at every coordinate point for every component
        self.coordinate_image(axes, sector_comp_coord)

    def comp_connection(self,axes,sector_coord,max_y_value):
        # TODO add connection lines and arrow according to flow group :
        '''
        tree animation type for every sector  
        : 1. comp to bar 2. bar to comp 3. comp to comp
        : Properties of line: comp coordinate point,color,label, arrow type,
        : shrinkA/B: move the tip and base some percent away from the annotated point
        : define every sector with color
        '''
        color_item = {'DC Electricity': 'b', 'AC Electricity': 'k', 'therm': 'r', 'hydro': 'k', 'gas': 'g'}

        x=type(sector_coord.keys())
        axes_index=0
        text_index=2
        # these are matplotlib.patch.Patch properties
        props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
        for i in sector_coord.keys():
            color=color_item[i]
            for point_group in sector_coord[i]['c2b']:
                line_style(axes,point_group,arrows='<-',color=color,sA=2,sB=30)
            for point_group in sector_coord[i]['b2c']:
                line_style(axes, point_group,arrows='<-',color=color,sA=30,sB=2)
            for point_group in sector_coord[i]['c2c']:
                line_style(axes, point_group,arrows='<-',color=color,sA=30,sB=30)

            # draw bus line
            x = (axes_index + 1) * 3
            if i == 'DC Electricity':
                x -= 2
                line_style(axes, [[x, 0], [x, max_y_value]], arrows='-', color=color, sA=0, sB=0)
            # elif i == 'AC Electricity':
            #     x -= 1
            #     line_style(axes, [[x, 0], [x, max_y_value]], arrows='-', color=color, sA=0, sB=0)
            else:
                line_style(axes, [[x, 0], [x, max_y_value]], arrows='-', color=color, sA=0, sB=0)
            axes_index += 1
            if i=='therm':
                axes.text(text_index , max_y_value, 'Heat', fontsize=18, style='oblique', ha='center',
                          va='top', wrap=True,bbox=props)
            else:
                axes.text(text_index, max_y_value, i, fontsize=18, style='oblique', ha='center',
                          va='top', wrap=True,bbox=props)
            text_index+=3
        axes.axis('off')

    #  get all flow group through matrix Transformation
    def listdir(self,scenario):
        path = 'input_files/' + scenario + '/'
        # Test for swimming pool

        # print(os.listdir(path))
        all_sector = os.listdir(path)  # find all sectors in this scenario
        matrix = Matrix_transfor()
        flow = matrix.transform(path, all_sector)
        comp_type=matrix.sector_comp_type
        return flow,comp_type

    def max_sum_comp_sector(self,comp_type,sector):
        max_sum_comp=0
        for i in sector:
            sum_comp_sector=len(comp_type[i])
            if max_sum_comp<sum_comp_sector:
                max_sum_comp=sum_comp_sector
        return max_sum_comp

    def diagram_coordinate(self,ax,max_x_value, max_y_value):
        # add coordinate point
        y_sticks = []
        for i in range(1, max_y_value+1):
            y_sticks.append(i)
        ax.set_yticks(y_sticks)

        x_sticks = []
        for i in range(1, max_x_value+1):
            x_sticks.append(i)
        ax.set_xticks(x_sticks)


    def comp_coord(self, c2b, b2c, c2c,Y_value,sector_index,sector_common_comp,sector):
        comp_coord = {}
        c2b_coord=[]
        b2c_coord=[]
        c2c_coord=[]
        #comp to bar
        for i in  c2b:
            if i[0] in list(sector_common_comp.keys()):
                comp = sector_common_comp[i[0]]
                c2b_coord.append([comp,[sector_index*3,Y_value]])
            elif sector=='DC Electricity' :
                c2b_coord.append([[sector_index * 3 - 1, Y_value-1], [sector_index, Y_value-1]])
                comp_coord[i[0]] = [sector_index * 3 - 1, Y_value-1]
            # elif sector=='AC Electricity' :
            #     c2b_coord.append([[sector_index * 3 - 2, Y_value-1], [sector_index-1, Y_value-1]])
            #     comp_coord[i[0]] = [sector_index * 3 - 2, Y_value-1]
            else:
                c2b_coord.append([[sector_index*3-1,Y_value],[sector_index*3,Y_value]])
                comp_coord[i[0]]=[sector_index*3-1,Y_value]
            Y_value-=1

        # bar to comp
        common_comp,same_comp_flow,same_comp_flow_coord = self.c2b_vs_b2c(c2b,c2b_coord, b2c)
        for i in b2c:
            if i in same_comp_flow :
                b2c_coord.append(same_comp_flow_coord[same_comp_flow.index(i)])
            elif i[1] in list(sector_common_comp.keys()):
                comp = sector_common_comp[i[1]]
                if sector=='DC Electricity':
                    b2c_coord.append([[sector_index, b2c.index(i) + 1], comp])
                else:
                    b2c_coord.append([[sector_index * 3, b2c.index(i)+1 ],comp])
            else:
                if i[1] in common_comp:
                    comp=comp_coord[i[1]]
                    if sector=='DC Electricity':
                        b2c_coord.append([[comp[0] -1, comp[1]], comp])
                    else:
                        b2c_coord.append([[comp[0]+1,comp[1]],comp])
                elif sector=='DC Electricity':
                    b2c_coord.append([[sector_index, b2c.index(i) + 1], [sector_index * 3 - 1, b2c.index(i) + 1]])
                    comp_coord[i[1]] = [sector_index * 3 - 1, b2c.index(i) + 1]
                else:
                    b2c_coord.append([[sector_index * 3, b2c.index(i)+1 ],[sector_index * 3 - 1,b2c.index(i)+1 ]])
                    comp_coord[i[1]]=[sector_index * 3 - 1, b2c.index(i)+1 ]

        # comp to comp
        y = int(np.median(Y_value))
        for i in c2c:
            if i[0] in list(sector_common_comp.keys()):
                comp_1 = sector_common_comp[i[0]]
            else:
                if i[0] in list(comp_coord.keys()):
                    comp_1 = comp_coord[i[0]]
                else:
                    if sector=='DC Electricity':
                        comp_1 = [3 * sector_index, y+2]
                        comp_coord[i[0]] = [3 * sector_index , y+2]
                        y -= 1
                    else:
                        comp_1=[3*(sector_index-1)+1,y]
                        comp_coord[i[0]]=[3*(sector_index-1)+1,y]
                        y-=1

            if i[1] in list(sector_common_comp.keys()):
                comp_2 = sector_common_comp[i[1]]
                c2c_coord.append([comp_1, comp_2])
            else:
                if i[1] in list(comp_coord.keys()):
                    comp_2 = comp_coord[i[1]]
                    c2c_coord.append([comp_1, comp_2])
                else:
                    comp_2 = [3*(sector_index-1)+1,c2c.index(i)+1]
                    c2c_coord.append([comp_1, comp_2])
                    comp_coord[i[1]] = comp_2

        coordinate={'c2b': c2b_coord,'b2c': b2c_coord,'c2c': c2c_coord}
        return comp_coord,coordinate



    def c2b_vs_b2c(self,c2b,c2b_coord, b2c):
        c2b_comp=[]
        b2c_comp=[]
        for i in c2b:
            c2b_comp.append(i[0])
        for i in b2c:
            b2c_comp.append(i[1])
        same_comp = [x for x in c2b_comp if x in b2c_comp]

        same_comp_flow=[]
        same_comp_flow_coord=[]
        for i_c2b in c2b:
            for i_b2c in b2c:
                if i_c2b[0]==i_b2c[1] and i_c2b[1]==i_b2c[0]:
                    flow=[]
                    index=c2b_coord[b2c.index(i_b2c)]
                    flow.append(index[1])
                    flow.append(index[0])
                    same_comp_flow_coord.append(flow)
                    same_comp_flow.append(i_b2c)
        return same_comp,same_comp_flow,same_comp_flow_coord

    def coordinate_image(self,ax,flow_sector):

        components=list(flow_sector.keys())
        image_coordinates=list(flow_sector.values())
        new_icons={}
        exist_icons=icons.get_icons()
        for comp in components:
            if comp in list(exist_icons.keys()):
                new_icons.update({comp:exist_icons[comp]})
        # Load images
        images = {k: PIL.Image.open(fname) for k, fname in new_icons.items()}

        for image in images.values():
            new_image=image.resize((160,160),Image.ANTIALIAS)
            image_key=list(images.keys())[list(images.values()).index(image)]
            # print(image_key)
            images[image_key]=new_image

        # Replace the points in the coordinates with images
        for i in list(images.keys()):
            imagebox = OffsetImage(images[i], zoom=0.3)
            imagebox.image.axes = ax
            n=list(images.keys()).index(i)
            ab = AnnotationBbox(imagebox,
                                xy=image_coordinates[n],
                                    xycoords='data',
                                    boxcoords='data',
                                    pad=0.5,
                                    frameon = False
                                    )
            ax.add_artist(ab)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    # scenario = 'swimming_pool'
    # scenario = 'heatpump_household_AC'          #ok
    # scenario='household_only_heat_w_gas'        #ok
    # scenario='heatgrid_exchanger'               #ok
    # scenario='heatgrid_exchanger_simp'          #ok
    # scenario='matrix_topology_1'                  #ok
    scenario='swimmingPool_HP'                  #ok
    # scenario='swimmingPool'
    figure = Figure_Canvas(scenario)
    figure.show()
    sys.exit(app.exec_())

