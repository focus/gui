import os,sys
from pprint import pprint
from PIL import Image

os.chdir('/project/prosumer_modeling')
path = os.getcwd()+"\GUI\imag\components_Icon"

def image_path():
    for infile in sys.argv[0:]:
        f, e = os.path.splitext(infile)
        outfile = f + ".jpg"
        if infile != outfile:
            try:
                with Image.open(infile) as im:
                    im.save(outfile)
            except OSError:
                print("cannot convert", infile)


def get_icons():
    # get all components icon path
    icons_name=[]
    for icon in os.listdir(path):
        icon_name, e = os.path.splitext(icon) ## icon_name=icon[:-4]
        icons_name.append(icon_name)
    icons= dict.fromkeys(icons_name)
    for icon in icons.keys():
        icons[icon]=path+"\\"+icon+'.png'
    # pprint(icons)
    return icons
# icons=get_icons()

def image_process(icons):
    for im_name, im_path in icons.items():
        try:
            im = Image.open(im_path)
            im.show()
        except Exception as e:
            print(e)

# image_process(icons)

# icons = {
#             #elec matrix:
#             "StandardPVGenerator": path+"\StandardPVGenerator.png",
#             "Inverter":path+"\Converter.png",
#             "BasicInverter*":path+"\Converter.png",
#             "BasicInverter":path+"\Converter.png",
#             "LiionBattery": path + "\Liionbattery.png",
#             "StandardACGrid": path+"\ACGrid.png",
#             "StandardElectricalConsumption": path + "\Consumption.png",
#             "BasicDCDC": path+"\BasicDCDC.png",
#             "Basicinverter":path+"\Basicinverter.png",
#             # therm matrix:
#             "HeatGrid": path + "\HeatGrid.png",
#             "HeatPump": path + "\Heatpumpe.png",
#             "StandardElecHeatPump": path + "\GUI\imag\components_Icon\Heatpumpe.png",
#             "HeatConsumption": path+"\GUI\imag\components_Icon\Consumption.png",
#             "HotWaterConsumption": path + "\GUI\imag\components_Icon\Consumption.png",
#             "HotWaterStorage": path+"\GUI\imag\components_Icon\HotwaterStorage.png",
#             "heatGrid": path+"\GUI\imag\components_Icon\HeatGrid.png",
#             "ElectricBoiler": path+"\GUI\imag\components_Icon\ElectricBoiler.png",
#             "ElectricRadiator": path+"\GUI\imag\components_Icon\ElectricRadiator.png",
#             "GasBoiler": path+"\GUI\imag\components_Icon\GasBoiler.png",
#             "HeatExchanger": path+"\GUI\imag\components_Icon\HeatExchanger.png",
#             "PressureStorage": path+"\GUI\imag\components_Icon\PressureStorage.png",
#             "Radiator": path+"\GUI\imag\components_Icon\Radiator.png",
#             "StandardGasGrid": path+"\GUI\imag\components_Icon\StandardGasGrid.png",
#             "StandardThermalConsumption": path+"\GUI\imag\components_Icon\Consumption.png",
#             "SolarThermalGenerator": path+"\GUI\imag\components_Icon\SolarThermalGenerator.png",
#             "SolarThermalCollector": path+"\GUI\imag\components_Icon\SolarThermalGenerator.png",
#             "StandardHotWaterConsumption": path + "\GUI\imag\components_Icon\Consumption.png",
#             "StandardPEMElectrolyzer": path+"\GUI\imag\components_Icon\StandardPEMElectrolyzer.png",
#             "StandardPEMFuelCell": path+"\GUI\imag\components_Icon\StandardPEMFuelCell.png",
#             "AirCollector": path+"\GUI\imag\components_Icon\AirCollector.png",
#             "BHKW": path+"\GUI\imag\components_Icon\BHKW.png",
#         }