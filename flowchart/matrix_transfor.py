import os
import csv
import pandas as pd
import pprint
# Input Matrix Transformation (flow from comp to comp) from input file
# main bar according to scenario such as : them, gas , electricity,cool
    # flow Targe:     comp to comp  | comp to bar  | bar to comp

    # sepcial for Elec it is divided into AC-bar and DC-bar
    # other sector such as therm, Gas, hydrogey or cooling are not divided


#  Transformations model
class Matrix_transfor:

    def __init__(self):
        self.sector_comp_type={}

    def transform(self,path, all_sector):

        matrix = {}
        # read all components in every sector under the given scenario
        for i_sector in all_sector:
            with open(path+'/'+str(i_sector),'r') as f:
                next(f) # Remove the first line in csv
                reader=csv.reader(f)


                comp_name= [row[0] for row in reader] # get all the components name
                # get flow from components to components
                df=pd.read_csv(path+'/'+str(i_sector),usecols=comp_name)
                comp_type = pd.read_csv(path + '/' + str(i_sector), usecols=[1])['comp_type'].to_list()
                comp_type=self.check_comp_duplication(comp_type)
                df.index=comp_type
                df.columns=comp_type
                # print(df)

                sector = i_sector.split("_")[0]

                if sector =="elec":
                    # electricity matrix separate AC and DC matrix
                    df_AC,df_DC=self.elec_separate(df,comp_type)
                    # DC Matrix
                    # print('this is DC Matrix transformation')
                    DC_comp_type = list(df_DC.index)
                    df_DC = self.matrix_zero(df_DC,sector)
                    df_DC = self.matrix_transfor(df_DC, DC_comp_type, sector)
                    df_DC = self.matrix_transfor(df_DC, DC_comp_type, sector)
                    # print(df_DC)
                    comp_to_bar, bar_to_comp, comp_to_comp = self.comp_connect(df_DC,sector)

                    matrix.update({'DC Electricity':
                                       {'comp_to_bar': comp_to_bar,
                                        'bar_to_comp': bar_to_comp,
                                        'comp_to_comp': comp_to_comp}})
                    # print(matrix)

                    #AC matrix
                    # print('this is AC Matrix ')
                    AC_comp_type = list(df_AC.index)
                    df_AC = self.matrix_zero(df_AC,sector)
                    df_AC=self.matrix_transfor(df_AC,AC_comp_type,sector)
                    # print(df_AC)
                    comp_to_bar, bar_to_comp, comp_to_comp = self.comp_connect(df_AC,sector)

                    matrix.update({'AC Electricity':
                                            {'comp_to_bar':comp_to_bar,
                                             'bar_to_comp':bar_to_comp,
                                             'comp_to_comp':comp_to_comp}})

                    self.sector_comp_type['DC Electricity']=DC_comp_type
                    self.sector_comp_type['AC Electricity'] = AC_comp_type
                    # print(matrix)

                else:
                    # print('this sector matrix is' + sector)
                    df=self.matrix_zero(df,sector)
                    df=self.matrix_transfor(df,comp_type,sector)
                    comp_to_bar,bar_to_comp,comp_to_comp=self.comp_connect(df,sector)
                    # print(comp_to_bar)
                    # print(bar_to_comp)
                    # print(comp_to_comp)
                    matrix.update({sector:
                                       {'comp_to_bar': comp_to_bar,
                                        'bar_to_comp': bar_to_comp,
                                        'comp_to_comp': comp_to_comp}})
                    self.sector_comp_type[sector] = comp_type
        neu_matrix={}
        sector_item = ['DC Electricity', 'AC Electricity', 'therm', 'hydro', 'gas']
        for i in sector_item:
            if i in matrix.keys():
                neu_matrix.update({i:matrix[i]})
        return neu_matrix

    def check_comp_duplication(self,list):
        set_lst = set(list)
        neu_list = []
        if len(set_lst)!=len(list):
            for i in list:
                if i not in neu_list:
                    neu_list.append(i)
                else:
                    i=str(i)+'2'
                    neu_list.append(i)
        else:
            neu_list=list
        return neu_list


    # matrix elec: separate with df_AC-bar and df_DC bar
    def elec_separate(self,df,comp_type):
        #define AC & DC component
        AC_a_DC_components=['Inverter','BasicInverter','BasicInverter*']
        DC_components = ['StandardPVGenerator', 'LiionBattery'] # define DC component
        # find the common components
        common_comp=[]
        DC_comp=[]
        for i in comp_type:
            if i in AC_a_DC_components:
                common_comp.append(i)
            if i in DC_components:
                DC_comp.append(i)
        #df AC
        AC_comp_type=[x for x in comp_type if x not in DC_comp]
        df_AC = df
        for i in DC_comp:
            if i in list(df_AC):
                df_AC = df_AC.drop([i], axis=1)
                df_AC = df_AC.drop([i])

        # df DC
        AC_comp_without_common = [x for x in AC_comp_type if x not in common_comp]
        df_DC = df
        for i in AC_comp_without_common:
            if i in list(df_DC):
                df_DC = df_DC.drop([i], axis=1)
                df_DC = df_DC.drop([i])
        return df_AC, df_DC

    # matrix ist therm, gas, hygreon...
    ## data processing: find the flow of comp to comp ----> comp to bar or bar to comp
    def matrix_zero(self,df,sector):
        comp_type = list(df.index)
        comp_to_bar = df.sum(axis=1)  # the sum of rows
        bar_to_comp = df.sum(axis=0)  # the sum of columns

        bar = sector+'_bar'
        df[('%s' % bar)] = '0'

        df.loc[bar] = '0'

        for comp in comp_type:
            col=df[comp]
            if isinstance(col,pd.Series):
                if 1 in col.tolist():
                    df.loc[bar, comp] = 'NaN'
            row = df.loc[comp]
            if isinstance(row, pd.Series):
                if 1 in row.tolist():
                    df.loc[comp,bar] = 'NaN'

        df_0=df

        return df_0

# Priority sequence a>b>c>d:
    # a: [1,0] =0         b: [1,1]= 1      c:[0,1] =0    d:[0,0]=0
    def matrix_transfor(self, df,comp_type,sector):
        # comp_type = list(df.index)

        # assumption: a set of  ordered component list to bar =1
        assumed_order=['Consumption','Boiler','Battery','Storage','Inverter','Pump','PV']

        for i in comp_type:
            # for assumed_comp in assumed_order:
            # consumption:'Consumption','Boiler'
                if assumed_order[0] in i or assumed_order[1] in i:
                    assumed_order[0]=i
                    break
            # Battery,Storage
                if assumed_order[2] in i or assumed_order[3] in i:
                    assumed_order[0] = i
                    break

            # start the first assumption : if assumed component is in components -》》
            # bar to Consumption=1
        if assumed_order[0] in comp_type:
            bar = sector+'_bar'
            if df.loc[bar, assumed_order[0]] == 'NaN':
                df.loc[bar, assumed_order[0]] = 1
            else:
                df.loc[assumed_order[0], bar] = 1

            # flow[comp,'comp_to_bar']
            for comp in comp_type:
                comp_bj=[]
                for i in comp_type:
                    if i!=comp:
                        comp_bj.append(i)

                df_NaN = df.loc[[bar, comp], comp_bj]

                # if 'NaN' not in df_NaN['comp_to_bar'].values:
                #     if df_NaN[comp].tolist() == df_NaN['comp_to_bar'].astype('int').tolist():
                #      df.at[comp, 'comp_to_bar'] = 1

                # print(df_NaN['comp_to_bar'].astype('int').tolist())
                # print(df_NaN[comp].tolist())
                # print(df_NaN.duplicated())

                # print(df_NaN)
                if df.at[comp, bar] == 'NaN':
                    for index, row in df_NaN.iteritems():
                        if row.values.tolist() == [1, 0]:
                            df.at[comp, bar] = 0
                            break
                if df.at[comp, bar] == 'NaN':
                    for index, row in df_NaN.iteritems():
                        if  row.values.tolist() == [1, 1]:
                            df.at[comp, bar] = 1
                            break
                        elif row.values.tolist() == [0, 1]:
                            df.at[comp, bar] = 0
                            break



            for comp in comp_type:
                comp_bj = []
                for i in comp_type:
                    if i != comp:
                        comp_bj.append(i)

# bar_to_comp'------------------> comp
                df_NaN = df.loc[comp_bj, [bar, comp]]
                # print(df_NaN)
                if df.at[bar, comp] == 'NaN':
                    for index, row in df_NaN.iterrows():
                        if row.values.tolist() == [1, 0]:
                            df.at[bar, comp] = 0
                            break

                if df.at[bar, comp] == 'NaN':
                    for index, row in df_NaN.iterrows():
                        if row.values.tolist() == [1, 1]:
                            df.at[bar, comp] = 1
                            break
                        # elif row.values.tolist() == [0, 1]:
                        #     df.at['bar_to_comp', comp] = 0
                        #     break


                    # for index, row in df_NaN.iterrows():
                    #     if row.values.tolist() != [0, 1]:
                    #         break
                    # df.at['bar_to_comp', comp] = 0
                    # df_NaN['col'].isnull().any()
                    # for index, row in df_NaN.iterrows():
                    #     if row.values.tolist()==[1,0] or row.values.tolist()==[1,1]:
                    #         pass
                    #     else:
                    #         df.at[ 'bar_to_comp',comp] = 0
# __________________________________________________________________________________________
## comp------------>'comp_to_bar'
            for comp in comp_type:

                comp_bj = []
                for i in comp_type:
                    if i != comp:
                        comp_bj.append(i)
                if df.at[comp, bar] == 'NaN':

                    df_NaN = df.loc[[bar, comp], comp_bj]
                    # print(df_NaN)
                    for index, row in df_NaN.iteritems():
                        if row.values.tolist() == [1, 0]:
                            df.at[comp, bar] = 0
                            break
                        elif row.values.tolist() == [1, 1]:
                            df.at[comp, bar] = 1
                            break
                        elif row.values.tolist() == [0, 1]:
                            df.at[comp, bar] = 0
                            break
        return df

    def comp_connect(self,df,sector):
        comp_type = list(df.index)
        comp_type.remove(sector+'_bar')
        # comp to bar
        comp_to_bar=[]
        bar_to_comp=[]
        comp_to_comp_duplicated=[]
        bar = sector+'_bar'
        for comp in comp_type:
            if df.loc[comp, ('%s' % bar)] ==1:
                comp_to_bar.append([comp, bar])
            if df.loc[bar, comp] ==1:
                bar_to_comp.append([('%s' % bar), comp])
            for comp_2 in comp_type:
                if df.loc[comp,comp_2] ==1:
                    comp_to_comp_duplicated.append([comp, comp_2])

        comp_to_comp=[]
        for i in   range(len(comp_to_comp_duplicated)):
            # print(comp_to_comp[i][0])
            # print(comp_to_comp[i][1])
            # print(df.loc['bar_to_comp',i[1]])
            if df.loc[comp_to_comp_duplicated[i][0], bar] !=1 or df.loc[bar, comp_to_comp_duplicated[i][1]] !=1:
                # comp_to_comp.remove(comp_to_comp[i])
                comp_to_comp.append(comp_to_comp_duplicated[i])
                # i=i-1




        return comp_to_bar,bar_to_comp,comp_to_comp





#+++++++++++++++++++++++++++++++++

#++++++++++++++++++++++++++++





#Testing_________________________________________________________________________________
if __name__ == "__main__":
    # scenario = 'swimmingPool_HP'  # ok
    scenario = 'swimmingPool_CHP'  # ok
    # scenario='heatpump_household_AC'            #Ok
    # scenario='household_only_heat_w_gas'        #Ok
    # scenario='heatgrid_exchanger'               #Ok
    # scenario='heatgrid_exchanger_simp'          #Ok
    # scenario='matrix_topology_1'                  #Ok


     # find all sectors in this scenario
    path = '../../input_files/' + scenario + '/'
    all_sector = os.listdir(path)
    matrix=Matrix_transfor()
    result=matrix.transform(path, all_sector)

    pprint.pprint(result)


# #_________________________________________________________________________________________



