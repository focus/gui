import sys

import pandas as pd
from PySide2.QtCore import (Qt, QAbstractTableModel, QModelIndex)
from PySide2.QtSql import QSqlDatabase
from PySide2.QtWidgets import (QApplication, QHBoxLayout, QItemDelegate, QPushButton, QTableView, QWidget, QComboBox)
import GUI.cloud_database.scripts_db as database
import os
path=os.getcwd()
sql_path=path+'//GUI//cloud_database//database.sqlite'
db = QSqlDatabase("QSQLITE")
db.setDatabaseName(sql_path)
db.open()



class TableModel(QAbstractTableModel):
    def __init__(self,data, parent=None):
        super(TableModel, self).__init__(parent)
        self._data=data

    def rowCount(self, QModelIndex):
        return self._data.shape[0]

    def columnCount(self, QModelIndex):
        return self._data.shape[1]

    def data(self, index, role):
        if role == Qt.DisplayRole:
            value=self._data.iloc[index.row()][index.column()]
            return str(value)

    def headerData(self, section, orientation, role):
        if role == Qt.DisplayRole:
            if orientation == Qt.Horizontal:
                return str(self._data.columns[section])
            if orientation == Qt.Vertical:
                return str(self._data.index[section])

class ButtonDelegate(QItemDelegate):
    def __init__(self,parent=None):
        super(ButtonDelegate,self).__init__(parent)

    def paint(self, painter, option, index):
        if not self.parent().indexWidget(index):

            self.button_write = QPushButton(
                self.tr('Click'),
                self.parent()

            ) # clicked=self.cellButtonClicked
            self.button_write.index = [index.row(), index.column()]
            self.button_write.clicked.connect(self.cellButtonClicked)

            h_box_layout = QHBoxLayout()

            h_box_layout.addWidget(self.button_write)
            h_box_layout.setContentsMargins(0, 0, 0, 0)
            h_box_layout.setAlignment(Qt.AlignCenter)
            widget = QWidget()
            widget.setLayout(h_box_layout)
            self.parent().setIndexWidget(
                index,
                widget
            )


    def cellButtonClicked(self):
        if self.button_write.index[1]>5:
            print("prosumer")
            ID=self.button_write.index[0]
            colindex=self.button_write.index[1]
            value=database.prosumer_topo(ID,colindex,tbname='prosumer')
            print(value)
        else:
            ID=self.button_write.index
            print(ID)

        # print("Cell Button Clicked", self.sender().index)
    #Prosumer
    def colname(self,colindex):
        colname=''
        if colindex==16:
            colname='topo_elec'
        if colindex==18:
            colname = 'topo_therm'
        return colname




class ComboDelegate(QItemDelegate):
    """
    A delegate that places a fully functioning QComboBox in every
    cell of the column to which it's applied
    """

    def __init__(self, parent=None):
        super(ComboDelegate, self).__init__(parent)

    def paint(self, painter, option, index):

        self.combo = QComboBox(self.parent())


        # self.connect(self.combo, QtCore.SIGNAL("currentIndexChanged(int)"), self.parent().currentIndexChanged)
        # self.combo.currentIndexChanged.connect(self.combo_Index)
        li = []
        li.append("Zero")
        li.append("One")


        self.combo.addItems(li)

        if not self.parent().indexWidget(index):
            self.parent().setIndexWidget(
                index,
                self.combo
            )

    def combo_Index(self):
        print(self.combo.currentText())


class MyTableView(QTableView):
    def __init__(self,btn2=None,parent=None):
        super(MyTableView, self).__init__(parent)
        self.setItemDelegateForColumn(btn2, ButtonDelegate(self))
        # self.setItemDelegateForColumn(btn1, ButtonDelegate(self))

        # self.setItemDelegateForColumn(btn2, ComboDelegate(self))
    def cellButtonClicked(self):
        print("Cell Button Clicked")
        print("Cell Button Clicked", self.sender().index)

    # def currentIndexChanged(self, ind):
    #     print("Combo Index changed {0} {1} : {2}".format(ind, self.sender().currentIndex(), self.sender().currentText()))

def prosumer_tb():
    data, colname = database.get_SQLtable_all_value('prosumer_tb', db)
    btn1 = colname.index('topo_elec')
    btn2 = colname.index('topo_therm')
    index = []
    for i in range(1, len(data) + 1):
        index.append(str(i))
    data_tb = pd.DataFrame(data, columns=colname, index=index, )

    return data_tb,btn2,btn1

def timeseries_tb():
    data, colname = database.get_SQLtable_all_value('timeseries', db)
    value = colname.index('value')
    index = []
    for i in range(1, len(data) + 1):
        index.append(str(i))
    data_tb = pd.DataFrame(data, columns=colname, index=index, )
    return data_tb,value

if __name__ == '__main__':
    a = QApplication(sys.argv)
    data, colname = database.get_SQLtable_all_value('timeseries', db)
    # btn1=colname.index('topo_elec')
    # btn2 = colname.index('topo_therm')
    value = colname.index('value')
    index=[]
    for i in range(1,len(data)+1):
        index.append(str(i))
    data_tb = pd.DataFrame(data,columns=colname,index=index,)

    tableView = MyTableView(btn2=value)
    myModel = TableModel(data_tb)
    tableView.setModel(myModel)
    tableView.show()
    a.exec_()