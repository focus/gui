'''
create a new scenario database based on currently available scenario database
'''
import sqlite3
import sys
from PySide2 import QtWidgets, QtGui, QtCore
from PySide2.QtCore import Qt, Signal, QRect
from PySide2.QtGui import QIcon
from PySide2.QtSql import QSqlDatabase
from PySide2.QtUiTools import QUiLoader

import os
import GUI.cloud_database.scripts_db as database
from PySide2.QtWidgets import QHeaderView, QStyleOptionButton, QStyle, QCheckBox, QTableWidgetItem

path=os.getcwd()
sql_path=path+'//GUI//cloud_database//database.sqlite'
tbheader=database.get_all_tableheader()

# TODO use databank
db = QSqlDatabase("QSQLITE")
db.setDatabaseName(sql_path)
db.open()

all_header_combobox = []
class CheckBoxHeader(QHeaderView):
    """Initialize BoxHeader"""

    # setup signal
    select_all_clicked = Signal(bool)
    select_prosumer=Signal(str)
    # These four variables control the style, position and
    # size of the column header checkboxes
    _x_offset = 0
    _y_offset = 0
    _width = 20
    _height = 20

    def __init__(self, orientation=Qt.Horizontal, parent=None):
        super(CheckBoxHeader, self).__init__(orientation, parent)
        self.isOn = False

    def paintSection(self, painter, rect, logicalIndex):
        painter.save()
        super(CheckBoxHeader, self).paintSection(painter, rect, logicalIndex)
        painter.restore()

        self._y_offset = int((rect.height() - self._width) / 2.)

        if logicalIndex == 0:
            option = QStyleOptionButton()
            option.rect = QRect(rect.x() + self._x_offset, rect.y() + self._y_offset, self._width, self._height)
            option.state = QStyle.State_Enabled | QStyle.State_Active
            if self.isOn:
                option.state |= QStyle.State_On
            else:
                option.state |= QStyle.State_Off
            self.style().drawControl(QStyle.CE_CheckBox, option, painter)

    def mousePressEvent(self, event):
        index = self.logicalIndexAt(event.pos())
        if 0 == index:
            x = self.sectionPosition(index)
            if x + self._x_offset < event.pos().x() < x + self._x_offset + self._width and self._y_offset < event.pos().y() < self._y_offset + self._height:
                if self.isOn:
                    self.isOn = False
                else:
                    self.isOn = True
                    # when the user clicks the row header checkbox,emit all check box
                self.select_all_clicked.emit(self.isOn)
                # self.select_prosumer.emit(all_header_combobox)
                self.updateSection(0)
        super(CheckBoxHeader, self).mousePressEvent(event)

    # @slot select_all_clicked
    def change_state(self, isOn):
        # If the row header checkbox is checked
        if isOn:
            # Set all checkboxes to the checked state
            for i in all_header_combobox:
                i.setCheckState(Qt.Checked)
        else:
            for i in all_header_combobox:
                i.setCheckState(Qt.Unchecked)
                # self.select_prosumer.emit(all_header_combobox)


scenario_header_field=tbheader['scenario']
prosumer_header_field=tbheader['prosumer']
component_header_field=tbheader['component']

class create_scenario:
    def __init__(self):
        # Load the UI definition from the file
        # Dynamically create a corresponding window object from the UI definition

        self.ui = QUiLoader().load('/project/prosumer_modeling/GUI/ui_files/create_scenario.ui')
        self.ui.setWindowIcon(QIcon('GUI/imag/RWTH_Logo.png'))
        self.ui.setWindowTitle("Create New Scenario")
        self.ui.resize(2000, 800)
        self.init_table()
        self.choose_prosumer_comp()
        self.ui.show()




    def init_table(self):
        self.setTableWidget(self.ui.tab_scenario, scenario_header_field, 'scenario')
        self.setTableWidget(self.ui.tab_prosumer, prosumer_header_field, 'prosumer')
        self.setTableWidget(self.ui.tab_components, component_header_field, 'component')
        self.generate_new_scenario_db()

        self.ui.tab_prosumer.clicked.connect(self.open_components_tab)
        # self.ui.toolBox.currentChanged.connect(self.get_current_widget)


    def choose_prosumer_comp(self):
        '''

        '''
        pass

    def open_components_tab(self,item):
        current_page=self.get_current_widget()
        if current_page=='Prosumer table':
            sf = "You clicked on {0}x{1}".format(item.column(), item.row())
            data = self.ui.tab_prosumer.currentIndex().data()
            print(data)
            print(type(item.column()))
            print(sf)


    def get_current_widget(self):
        index=self.ui.toolBox.currentIndex()
        # print(index)
        current_page=self.ui.toolBox.itemText(index)
        return current_page



    def setTableWidget(self,ui_tbname,tbheader,tbname):
        # Initialize table widget
        rowmax = database.rows_max(tbname)
        ui_tbname.setRowCount(rowmax)
        self.setTableHeaderField(ui_tbname,tbheader,rowmax,tbname)  # Set up table header
        self.setTableContents(ui_tbname,tbname)
        ui_tbname.setAlternatingRowColors(True)  # Alternating row colors

    def setTableHeaderField(self,ui_tbname,tbheader,rowmax,tbname):
        ui_tbname.setColumnCount(len(tbheader))
        for i in range(len(tbheader)-1):
            header_item = QTableWidgetItem(tbheader[i])
        rowname=[]
        for i in range(0,rowmax):
            rowname.append('row'+str(i+1))
        cbox_id=dict.fromkeys(rowname)

        for i in range(0,rowmax):
            checkbox = QCheckBox(str(i))
            all_header_combobox.append(checkbox) # add all checkbox
            cbox_id['row'+str(i+1)]=checkbox

            checkbox.clicked.connect(lambda: self.checkbox_clicked(list(cbox_id.keys())[i],checkbox,tbname))
            ui_tbname.setCellWidget(i,0,checkbox) # add checkbox for every rows



        header = CheckBoxHeader()               # Instantiating table headers
        ui_tbname.setHorizontalHeader(header)
        ui_tbname.setHorizontalHeaderLabels(tbheader)
        ui_tbname.setColumnWidth(0,20)       # Setup the width
        header.select_all_clicked.connect(header.change_state)      #  row header checkbox click on signal with slot

    def checkbox_clicked(self,row,checkbox,tbname):

        print(row)
        print(tbname)

    def setTableContents(self,ui_tbname,tbname):
        conn = sqlite3.connect(sql_path)
        c = conn.cursor()
        rowcount = c.execute('''SELECT COUNT(*) FROM {};'''.format(tbname)).fetchone()[0]
        ui_tbname.setRowCount(rowcount)
        c.execute('''SELECT * FROM {}'''.format(tbname))
        for row, form in enumerate(c):
            for column, item in enumerate(form):
                ui_tbname.setItem(row, column, QTableWidgetItem(str(item)))

    def generate_new_scenario_db(self):
        continents = ['scenario']
        countries = [['prosumer']]
        provinces = [[['component']]]
        self.model = QtGui.QStandardItemModel()
        continentIndex = 0
        for continent in continents:
            continentModel = QtGui.QStandardItem(continent)
            countryIndex = 0
            for country in countries[continentIndex]:
                countryModel = QtGui.QStandardItem(country)
                continentModel.appendRow(countryModel)
                for province in provinces[continentIndex][countryIndex]:
                    provincesModel = QtGui.QStandardItem(province)
                    countryModel.appendRow(provincesModel)
                countryIndex += 1

            self.model.appendRow(continentModel)
            continentIndex += 1
        self.ui.colview.setModel(self.model)

if __name__ == '__main__':
    if not QtWidgets.QApplication.instance():
        app = QtWidgets.QApplication(sys.argv)
    else:
        app = QtWidgets.QApplication.instance()
    # app = QApplication(sys.argv)
    app.setStyle('Fusion')
    newScenario = create_scenario()
    sys.exit(app.exec_())