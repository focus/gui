import pandas as pd
from PySide2 import QtCore, QtWidgets, QtSql
from PySide2.QtCore import Qt
from PySide2.QtGui import QPixmap
from PySide2.QtSql import QSqlDatabase, QSqlTableModel, QSqlQuery
from PySide2.QtUiTools import QUiLoader
import os,sys
import csv
os.chdir('/project/prosumer_modeling')


class Timeseries_value_handle:
    def __init__(self,id):
        # Load the UI definition from the file
        # Dynamically create a corresponding window object from the UI definition

        self.ui = QUiLoader().load('/project/prosumer_modeling/GUI/ui_files/Timeseries_Value_View.ui')
        self.ui.setWindowTitle("Timeseries Value Views")
        self.ui.resize(1600, 600)
        data_id=id
        self.ui.data_id.addItems(data_id)

        self.data_tranfo()

        self.ui.csv_file.clicked.connect(self.csv_file_open)
        self.ui.btn_close.clicked.connect(self.close)

        self.ui.data_id.currentIndexChanged.connect(self.data_id_change)

        self.ui.show()
    def data_id_change(self):
        self.data_tranfo()

    def data_tranfo(self):
        path = os.getcwd()
        self.data_id = self.ui.data_id.currentText()
        pre_path = path + '/GUI/cloud_database/timeseries_plot/' + self.data_id
        self.csv_path = pre_path + '.csv'
        df = pd.read_csv(self.csv_path)
        number_value = df.shape[0]
        mean_value = df['value'].mean(axis=0)

        image_path = pre_path + '.png'
        self.Imageadd(image_path)
        self.ui.sum.setText(str(number_value))
        self.ui.mean.setText(str(mean_value))

    def Imageadd(self,image_path):
        if os.path.isfile(image_path):
            self.scene = QtWidgets.QGraphicsScene(self.ui.graphicsView)
            pixmap = QPixmap(image_path)
            item = QtWidgets.QGraphicsPixmapItem(pixmap)
            self.scene.addItem(item)
            self.ui.graphicsView.setScene(self.scene)
    def csv_file_open(self):
        os.startfile(self.csv_path)


    def close(self):
        self.ui.close()

if __name__ == '__main__':
    if not QtWidgets.QApplication.instance():
        app = QtWidgets.QApplication(sys.argv)
    else:
        app = QtWidgets.QApplication.instance()
    # app = QApplication(sys.argv)
    app.setStyle('Fusion')
    id_list=['ID_1','ID_2','ID_3']
    timeseries_value = Timeseries_value_handle(id_list)
    sys.exit(app.exec_())



