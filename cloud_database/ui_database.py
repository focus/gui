import subprocess

import requests
import webbrowser
import sqlite3
import pandas as pd
import numpy as np
from PySide2 import QtCore, QtWidgets, QtSql
from PySide2.QtCore import Qt, QThreadPool, Signal, QObject, QRunnable
from PySide2.QtSql import QSqlDatabase, QSqlTableModel, QSqlQuery
from PySide2.QtWidgets import QMainWindow, QApplication, QMessageBox, QComboBox, QHeaderView, QItemDelegate, \
    QTableWidget, QAbstractItemView, QTableWidgetItem
from PySide2.QtUiTools import QUiLoader
import scripts.Database.use_database as use_db
import GUI.cloud_database.scripts_db as database
import GUI.cloud_database.delegate as delegate
import os,sys
os.chdir('/project/prosumer_modeling')

import os
path=os.getcwd()
sql_path=path+'//GUI//cloud_database//database.sqlite'


# TODO use databank
# sql_name='database.sqlite'
db = QSqlDatabase("QSQLITE")
db.setDatabaseName(sql_path)
db.open()

if db.open():
     print('Database opened successfully')
else:
     print(db.lastError().text())
table_name=['scenario','prosumer','component','data','scalar','timeseries','scenario_prosumer','prosumer_component','component_data']
# database.add_SQLtable(table_name,db)
# database.update_database(table_name,db)
class Signals(QObject):
    result=Signal(str)
    finished=Signal()

class Thread(QRunnable):
    """"
    Prosumer thread
    Inherits from QRunnable to handle prosumer thread setup, signals and wrap-up.

    :param args: Arguments to pass for the prosumer
    :param kwargs: Keywords to pass for the prosumer

    """
     # Create an instance of our signals class.
    def __init__(self,command):
        super().__init__()
        self.signals = Signals()
        self.command=command

    def run(self):
        try:

            output=subprocess.getoutput(self.command)
            self.signals.result.emit(output)
            self.signals.finished.emit()
        except Exception as e:
            print(e)
            # self.signals.error.emit(self.prosumer_name,str(e))
            # self.signals.status.emit(self.prosumer_name,STATUS_ERROR)
        else:
            pass
            # self.signals.result.emit(self.prosumer_name,result)
            # self.signals.status.emit(self.prosumer_name,STATUS_COMPLETE)



class Cloud_Database:
    def __init__(self):
        # Load the UI definition from the file
        # Dynamically create a corresponding window object from the UI definition

        self.ui = QUiLoader().load('/project/prosumer_modeling/GUI/ui_files/Database_Scenario.ui')
        self.ui.setWindowTitle("Database of models")
        self.ui.resize(1600, 800)

        # table_name=['scenario','prosumer','component','data','scalar','timeseries']
        # pre_path='https://openenergy-platform.org/api/v0/schema/model_draft/tables/ineed_'
        # path={}
        # for i in table_name:
        #     path_name=pre_path+i+'/rows'
        #     path[i]=path_name
            # webbrowser.open(path_name)
        # self.ui.search_line.textChanged.connect(self.filter)

        self.scenario_model = QSqlTableModel(db=db)
        self.scenario_model.setEditStrategy(QtSql.QSqlTableModel.OnManualSubmit)
        self.table_model = QSqlTableModel(db=db)
        #All changes will be cached in the model until submitAll() or revertAll() is called
        self.table_model.setEditStrategy(QtSql.QSqlTableModel.OnManualSubmit)
        # for button def : self.tableModel.submitAll()
        self.ui.table_model2.setSelectionBehavior(QAbstractItemView.SelectRows)
        # self.delegate = itemDelegate()
        # self.ui.table_model2.setItemDelegate(self.delegate)
        self.pool = QThreadPool()
        # TODO Mode 1：Select table under every scenario
        self.all_chbox_false()
        self.ui.chbox_scenario.stateChanged.connect(lambda: self.check_combox_db('scenario'))
        self.ui.chbox_prosumer.stateChanged.connect(lambda: self.check_combox_db('prosumer'))
        self.ui.chbox_component.stateChanged.connect(lambda: self.check_combox_db('component'))
        self.ui.chbox_data.stateChanged.connect(lambda: self.check_combox_db('data'))
        self.ui.chbox_scalar.stateChanged.connect(lambda:self.check_combox_db('scalar'))
        self.ui.chbox_timeseries.stateChanged.connect(lambda:self.check_combox_db('timeseries'))
        self.ui.cbox_component_1.currentIndexChanged.connect(self.comp_tb)
        # TODO Mode 2: choose and show every datatable
        self.ui.rbt_scenario.toggled.connect(lambda :self.each_datatable_db('scenario'))
        self.ui.rbt_prosumer.toggled.connect(lambda: self.each_datatable_db('prosumer'))
        self.ui.rbt_component.toggled.connect(lambda: self.each_datatable_db('component'))
        self.ui.rbt_data.toggled.connect(lambda: self.each_datatable_db('data'))
        self.ui.rbt_scalar.toggled.connect(lambda: self.each_datatable_db('scalar'))
        self.ui.rbt_timeseries.toggled.connect(lambda: self.each_datatable_db('timeseries'))
        # self.ui.cbox_component_2.currentIndexChanged.connect(self.refresh_part_box)

        #TODO Button activ
        self.ui.btn_reset.clicked.connect(self.reset_table)
        self.ui.btn_save.clicked.connect(self.save_table)

        # scenario
        self.ui.table_model1.clicked.connect(self.table_scenario_click)
        self.ui.table_model2.clicked.connect(self.table_view_click)

        # self.ui.tabWidget.cur-#rentChanged.connect(self.tabWidget_change)
        self.ui.btn_add.clicked.connect(self.addRows)
        self.ui.btn_delete.clicked.connect(self.deleteRows)
        self.ui.btn_cancel.clicked.connect(self.ui_close)

        self.ui.actionadd_scenario.triggered.connect(self.add_scenario)
        self.ui.actionPull.triggered.connect(self.database_update)

        self.ui.show()



    def check_scenario_combox(self):
        all_chbox_true = [self.ui.chbox_scenario.isChecked() == True,
                          self.ui.chbox_prosumer.isChecked() == True,
                          self.ui.chbox_component.isChecked() == True,
                          self.ui.chbox_data.isChecked() == True,
                          self.ui.chbox_scalar.isChecked() == True,
                          self.ui.chbox_timeseries.isChecked() == True]
        result_chbox=[]
        for i in all_chbox_true:
            result_chbox.append(i)
        sum_True = result_chbox.count(True)
        sum_False = result_chbox.count(False)

        return result_chbox,sum_True,sum_False

    def check_combox_db(self,tbname):

        self.ui.table_model1.setModel(self.scenario_model)
        self.ui.table_model1.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeToContents)
        # Column names can be freely dragged to the position of the column
        self.ui.table_model1.horizontalHeader().setSectionsMovable(True)
        self.ui.table_model1.horizontalHeader().sortIndicatorOrder()
        self.ui.table_model1.setAlternatingRowColors(True)
        result_chbox,sum_True,sum_False=self.check_scenario_combox()
        # print(result_chbox)


        if result_chbox[0]==True and sum_True==1: # scenario table
            self.scenario_model.setTable(tbname)
            self.ui.cbox_scenario_1.clear()
            scenario_name_list = database.get_column_value('scenario', 'scenario')
            self.ui.cbox_scenario_1.addItems(scenario_name_list)
            self.ui.cbox_prosumer_1.clear()
            self.ui.cbox_component_1.clear()
            self.scenario_model.select()
        elif result_chbox[0]==True and result_chbox[1]==True and sum_True==2 : # prosumer table
            scenario_name=self.ui.cbox_scenario_1.currentText()
            # database.scenario_prosumer_tb('scenario','prosumer','scenario',scenario_name,db)
            self.scenario_model.setTable('prosumer_tb')
            self.ui.cbox_prosumer_1.clear()
            prosumer_name_list = database.get_column_value('prosumer_tb', 'prosumer_name')
            self.ui.cbox_prosumer_1.addItems(prosumer_name_list)
            self.ui.cbox_component_1.clear()
            self.scenario_model.select()
            data_tb, btn2, btn1 = delegate.prosumer_tb()
            self.ui.table_model1.setItemDelegateForColumn(btn2, delegate.ButtonDelegate(self.ui.table_model1))
            self.ui.table_model1.setItemDelegateForColumn(btn1, delegate.ButtonDelegate(self.ui.table_model1))

        # component table
        elif result_chbox[0]==True and result_chbox[1]==True and result_chbox[2]==True and sum_True==3:
            component_name = self.ui.cbox_prosumer_1.currentText()
            # database.scenario_component_tb('prosumer','component','prosumer_name',component_name,db)
            self.scenario_model.setTable('component_tb')
            idx = self.scenario_model.fieldIndex("ID")
            self.scenario_model.setSort(idx, Qt.AscendingOrder)
            self.ui.cbox_component_1.clear()
            component_name_list = database.get_column_value('component', 'comp_type')
            self.ui.cbox_component_1.addItems(component_name_list)
            self.scenario_model.select()
        # component data table
        elif result_chbox[4]==False and result_chbox[5]==False and sum_False==2:
            component_name = self.ui.cbox_component_1.currentText()
            tablename='{}_data_tb'.format(component_name)
            self.scenario_model.setTable(tablename)
            self.scenario_model.select()
        # component data scalar table
        elif result_chbox[5]==False and sum_False==1:
            component_name = self.ui.cbox_component_1.currentText()
            tablename = '{}_scalar_tb'.format(component_name)
            self.scenario_model.setTable(tablename)
            self.scenario_model.select()
        # timeseries table
        elif sum_True==6:
            self.scenario_model.setTable(tbname)
            idx = self.scenario_model.fieldIndex("id")
            self.scenario_model.setSort(idx, Qt.AscendingOrder)
            data_tb, value = delegate.timeseries_tb()
            # self.ui.table_model1.setItemDelegateForColumn(value, delegate.ButtonDelegate(self.ui.table_model1))
            # self.ui.search_line.textChanged.connect(self.filter)
            self.scenario_model.select()
        # if sum_False==6:
        else:
            self.reset_table()


    def comp_tb(self):
        result_chbox, sum_True, sum_False = self.check_scenario_combox()
        if result_chbox[4]==False and result_chbox[5]==False  and sum_False==2 and \
                self.ui.cbox_component_1.currentText()!='StandardPVGenerator':
            component_name = self.ui.cbox_component_1.currentText()
            tablename = '{}_data_tb'.format(component_name)
            self.scenario_model.setTable(tablename)
            self.scenario_model.select()
        if  result_chbox[5]==False and sum_False==1 and \
                self.ui.cbox_component_1.currentText() != 'StandardPVGenerator':
            component_name = self.ui.cbox_component_1.currentText()
            tablename = '{}_scalar_tb'.format(component_name)
            self.scenario_model.setTable(tablename)
            self.scenario_model.select()


    def all_chbox_false(self):
         self.ui.chbox_prosumer.setChecked(False)
         self.ui.chbox_component.setChecked(False)
         self.ui.chbox_data.setChecked(False)
         self.ui.chbox_scalar.setChecked(False)
         self.ui.chbox_timeseries.setChecked(False)
         self.ui.chbox_scenario.setChecked(False)
    # Table view
    def all_chbox_clear(self):
        self.ui.cbox_scenario_2.clear()
        self.ui.cbox_component_2.clear()
        self.ui.cbox_prosumer_2.clear()
        self.ui.cbox_parameters_2.clear()
        self.ui.cbox_scalar_2.clear()
        self.ui.cbox_timeseries_2.clear()

    def each_datatable_db(self, tbname):
        try:
            self.ui.table_model2.setModel(self.table_model)
            self.ui.table_model2.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeToContents)

            # Column names can be freely dragged to the position of the column
            self.ui.table_model2.horizontalHeader().setSectionsMovable(True)
            self.ui.table_model2.setAlternatingRowColors(True)
            self.table_model.setTable(tbname)
            idx = self.table_model.fieldIndex("id")
            self.table_model.setSort(idx, Qt.AscendingOrder)
            # self.ui.search_line.textChanged.connect(self.filter)
            self.table_model.select()

        except Exception as exc:
            print('There was a problem : %s' % (exc))
        if tbname == 'scenario':
            self.all_chbox_clear()
            scenario_name_list = database.get_column_value('scenario', 'scenario')
            self.ui.cbox_scenario_2.addItems(scenario_name_list)

        elif tbname == 'prosumer':
            self.all_chbox_clear()
            prosumer_name_list = database.get_column_value('prosumer', 'prosumer_name')
            self.ui.cbox_prosumer_2.addItems(prosumer_name_list)
            data_tb, btn2, btn1 = delegate.prosumer_tb()
            self.ui.table_model2.setItemDelegateForColumn(btn2, delegate.ButtonDelegate(self.ui.table_model2))
            self.ui.table_model2.setItemDelegateForColumn(btn1, delegate.ButtonDelegate(self.ui.table_model2))

        elif tbname == 'component':
            self.all_chbox_clear()
            component_name_list = database.get_column_value('component', 'comp_type')
            self.ui.cbox_component_2.addItems(component_name_list)
        elif tbname == 'data':
            self.all_chbox_clear()
            parameters_list = database.get_column_value('data', 'parameter_name')
            self.ui.cbox_parameters_2.addItems(parameters_list)

        elif tbname == 'scalar':
            self.all_chbox_clear()
            scalar_list = database.get_column_value('scalar', 'data_id')
            self.ui.cbox_scalar_2.addItems(scalar_list)
        elif tbname == 'timeseries':
            self.all_chbox_clear()
            timeseries_list = database.get_column_value('timeseries', 'data_id')
            self.ui.cbox_timeseries_2.addItems(timeseries_list)

        else:
            self.all_chbox_clear()
        # elif tbname == 'timeseries':
        #     value = self.model.fieldIndex("value")
        #     combox = QComboBox()
        #     # combox.addItems([])
        #     combox.setStyleSheet('QComboBox{margin:3px};')
        #     self.ui.table_model2.setCellWidget(1, 3, combox)

    def filter(self,s):
        filter_str = 'Name LIKE "%{}%"'.format(s)
        self.model.setFilter(filter_str)
    def all_rbt_false(self):
        self.ui.rbt_scenario.setChecked(False)
        self.ui.rbt_prosumer.setChecked(False)
        self.ui.rbt_component.setChecked(False)
        self.ui.rbt_data.setChecked(False)
        self.ui.rbt_scalar.setChecked(False)
        self.ui.rbt_timeseries.setChecked(False)

    def get_current_tabWidget(self):
        currentTab = self.ui.tabWidget.currentWidget()
        return currentTab.objectName()  # tab_view or  scenario_view
    def reset_table(self):
        #Scenario View
        if self.get_current_tabWidget()=='scenario_view':
            self.all_chbox_false()
            self.scenario_model.clear()
        # Table view
        if self.get_current_tabWidget() == 'tab_view':
            self.all_rbt_false()
            self.all_chbox_clear()
            self.table_model.clear()

    def save_table(self):
        self.scenario_model.submitAll()
        self.table_model.submitAll()

    def database_update(self):
        database.update_database(table_name, db)

    def which_rbt(self):
        rbt={'scenario':self.ui.rbt_scenario.isChecked(),
        'prosumer':self.ui.rbt_prosumer.isChecked(),
        'component':self.ui.rbt_component.isChecked(),
        'data':self.ui.rbt_data.isChecked(),
        'scalar':self.ui.rbt_scalar.isChecked(),
        'timeseries':self.ui.rbt_timeseries.isChecked()
        }
        for key, value in rbt.items():
            if value==True:
                return key


    def table_view_click(self, item):
        # todo get view table name
        tbname=self.which_rbt()
        if tbname=='timeseries':
            sf = "You clicked on {0}x{1}".format(item.column(), item.row())
            data = self.ui.table_model2.currentIndex().data()
            # print(data)
            # print(type(item.column()))
            # print(sf)
            if item.row()<=2 and item.column()==2:
                self.timeseries_value_open()

    def timeseries_value_open(self):
        self.value_thread = Thread("python GUI/cloud_database/timeseries_value.py")
        self.value_thread.signals.result.connect(self.value_thread_output)
        self.value_thread.setAutoDelete(True)
        self.pool.start(self.value_thread)

    def value_thread_output(self):
        '''
        Here you can continue to develop as needed
        '''
        pass

    def table_scenario_click(self,item):
        scenario_check_result,sum_True,sum_False = self.check_scenario_combox()
        if sum(scenario_check_result)==6:
            sf = "You clicked on {0}x{1}".format(item.column(), item.row())
            data = self.ui.table_model1.currentIndex().data()
            # print(data)
            # print(sf)
            if item.row()<=2 and item.column()==2:
                self.timeseries_value_open()

    def addRows(self):
        tbname=self.which_rbt()
        database.add_SQLtable_empty_row(tbname, db=db)
        self.each_datatable_db(tbname)

    def deleteRows(self):
        """
        delete choose row&s
        """
        s_items = self.ui.table_model2.selectedIndexes() # get current index
        if s_items:
            selected_rows = []
            for i in s_items:
                row = i.row()+1
                print(row)
                if row not in selected_rows:
                    selected_rows.append(row)
            print(selected_rows)
            tbname = self.which_rbt()
            database.delect_SQLtable_rows(tbname, selected_rows)
            self.each_datatable_db(tbname)

    def add_scenario(self):
        self.threadpool = QThreadPool()
        self.cloud_db_thread = Thread("python GUI/cloud_database/create_new_scenario.py")
        self.cloud_db_thread.setAutoDelete(True)
        self.threadpool.start(self.cloud_db_thread)



    def ui_close(self):
        self.ui.close()


if __name__ == '__main__':
    if not QtWidgets.QApplication.instance():
        app = QtWidgets.QApplication(sys.argv)
    else:
        app = QtWidgets.QApplication.instance()
    # app = QApplication(sys.argv)
    app.setStyle('Fusion')
    Database = Cloud_Database()
    sys.exit(app.exec_())


