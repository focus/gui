'''
script for database
The following functions were based on the SQL programming language.
It contains about creating a new database,
adding/deleting/finding data in a database, and so on.
'''
import re
import datetime
from pprint import pprint

import numpy as np
import pandas as pd
import requests
from PySide2 import QtSql, QtCore
from PySide2.QtCore import Qt
from PySide2.QtSql import QSqlDatabase, QSqlQuery
import sqlite3
from matplotlib import pyplot as plt
from pandas import DataFrame

import os
os.chdir('/project/prosumer_modeling')
path=os.getcwd()
sql_path=path+'//GUI//cloud_database//database.sqlite'
db = QSqlDatabase("QSQLITE")
db.setDatabaseName(sql_path)
db.open()

tablelist=['scenario','prosumer','component','data','scalar','timeseries','scenario_prosumer','prosumer_component','component_data']

# print(QtSql.QSqlDatabase.drivers())

def create_database(sql_name):
    #create  a new database
    db = QSqlDatabase("QSQLITE")
    db.setDatabaseName(sql_name)
    db.open()
    if db.open():
        print('Database opened successfully')
    else:
        print(db.lastError().text())
    return db

def delete_SQLtable(tablelist,sqlpath=sql_path):
    '''
    delete the given table list or table
    '''
    for i in tablelist:
        conn = sqlite3.connect(sqlpath)
        c = conn.cursor()
        c.execute('DROP TABLE IF EXISTS {}'.format(i))
        conn.commit()
        conn.close()
        print('{} table deleted'.format(tablelist))

def rename_SQLtable(alt_tbname,neu_tbname):
    # Table name change
    conn = sqlite3.connect(sql_path)
    c = conn.cursor()
    sql='''alter table {} rename to {}'''.format(alt_tbname,neu_tbname)
    c.execute(sql)
    conn.commit()
    conn.close()

def get_SQLtable_column(tbname, colname,databank=db):
    '''
    get all value as list from given table based on given column name
    '''
    q = QSqlQuery(db=databank)
    command = "SELECT {} FROM {};".format(colname, tbname)
    value_list = []
    if q.exec_(command):
        column_index = q.record().indexOf(colname)
        while q.next():
            value = q.value(column_index)
            value_list.append(value)
    return value_list

def delete_all_SQLtable(db):
    '''
      Delete all tables list or table in the given database
      '''
    table_name=get_SQLtable_column('sqlite_sequence', 'name',db)
    delete_SQLtable(table_name)

def get_columns_index(tbname):
    '''
    get given table columns index
        for example:
        scenario: id,scenario,topo,date_start,date_end,timestep,source,comment
    '''
    pre_path = 'https://openenergy-platform.org/api/v0/schema/model_draft/tables/ineed_'
    path = pre_path + tbname + '/rows'
    table = requests.get(path)
    table = pd.DataFrame(table.json())
    table_index = table.columns.values.tolist()
    columns_name = ''
    for i in table_index:
        columns_name = columns_name + i + ',' + '\n'
    columns_name = columns_name[:-2]
    # print(text)
    return columns_name

def add_SQLtable_columnsIndex(tbname,column_name):
    '''
    create a table and
    add columns index for given table
    '''
    conn = sqlite3.connect(sql_path)
    c = conn.cursor()
    c.execute('DROP TABLE IF EXISTS {}'.format(tbname))
    tb_sql = 'CREATE TABLE IF NOT EXISTS ' + tbname + '(' + column_name + ');'
    # column_name format:
    # (ID INT PRIMARY KEY     NOT NULL,
    # SCENARIO           TEXT    NOT NULL,
    # TOPO            TEXT     NOT NULL,
    # DATA START       TEXT,
    # DATE END         TEXT,
    # TIMESTEP        INT,
    # SOURCE          TEXT,
    # COMMENT         TEXT);'''
    c.execute(tb_sql)
    print("{} table created successfully".format(tbname))
    conn.commit()
    conn.close()

def add_SQLtable_column(tbname,colname):
    # add column name in a given table
    conn = sqlite3.connect(sql_path)
    c = conn.cursor()
    try:
        add_column = '''ALTER TABLE {} ADD COLUMN {}'''.format(tbname,colname)
        c.execute(add_column)
    except sqlite3.OperationalError:
        pass
        # print('Column name already exists！')
    conn.commit()
    conn.close()

def create_SQLtable(tbname,db):
    '''
    CREATE TABLE if not exists tablename (
    ID INTEGER PRIMARY KEY AUTOINCREMENT);
    '''
    q = QSqlQuery(db)
    command = "CREATE TABLE if not exists {} (ID INTEGER PRIMARY KEY AUTOINCREMENT);".format(tbname)
    q.exec_(command)
    print("{} table created successfully".format(tbname))

def get_SQLtable_column_name(tbname,db):
    '''
    get list of all column name in a given table
    '''
    q = QSqlQuery(db)
    command = "pragma table_info({})".format(tbname)
    name_list = []
    if q.exec_(command):
        while q.next():
            column_name = q.value(1)
            name_list.append(column_name)
    return name_list

def add_SQLtable_cloumn(tbname, column_name, datatype,db):
    '''
    add columns name with datatype for given table
    '''
    q = QSqlQuery(db)
    command = u"ALTER TABLE {} ADD {} {};".format(tbname, column_name, datatype)
    q.exec_(command)

# tbname='prosumer'
# column_name_list=get_SQLtable_column_name('prosumer',db)
# tablename='tb_scenario_prosumer'
# for i in column_name_list:
#     add_SQLtable_cloumn(tablename, i, 'text',db)

def add_SQLtable_row(tbname, row_num,db):
    '''
    # add a row
    '''
    q = QSqlQuery(db)
    for row in range(1, row_num + 1):
        command = u"INSERT INTO {} (ID) VALUES ({});".format(tbname, str(row))
        q.exec_(command)

def rows_max(tbname,db=db):
    # get the max rowid in a given table
    conn = sqlite3.connect(sql_path)
    c = conn.cursor()
    c.execute("SELECT max(rowid) from {};".format(tbname))
    n = c.fetchone()[0]
    return n

def add_SQLtable_empty_row(tbname, db=db):
    '''
        # INSERT empty row at given table
        '''
    row_num=rows_max(tbname,db=db)
    add_SQLtable_row(tbname, row_num+1, db)

def get_column_value(tbname, colname):
    '''
       get all value at a certain column from given table
       '''
    conn = sqlite3.connect(sql_path)
    cursor = conn.cursor()
    sql = ''' select {} from {}'''.format(colname,tbname)
    results = cursor.execute(sql)
    col_value = results.fetchall()
    value=[]
    for i in col_value:
        value.append(list(i)[0])
    return value
# col_value=get_column_value('scenario', 'ID')
# print(col_value)

def delect_SQLtable_rows(tbname, rowindex):
    # delete rows based on given list of rowid
    conn = sqlite3.connect(sql_path)
    c = conn.cursor()
    ID_list = get_column_value(tbname, 'ID')
    for row in rowindex:
        row_ID=ID_list[row-1]
        data = ' delete from {} where id= {}'.format(tbname, row_ID)
        c.execute(data)

        conn.commit()
    conn.close()

# delect_SQLtable_rows('scenario', [6])
# row_num = rows_max('scenario', db=db)
# print(row_num)
# add_SQLtable_row(tbname, row_num, db)

def set_SQLtable_value(tbname, column, row, value,db,ID='ID'):
    '''
    # UPDATE a value under given table
        according to column name=value WHERE ID=row;
    '''
    q = QSqlQuery(db)
    tbvalue = u"UPDATE {} SET {}='{}' WHERE {}={};".format(tbname, column, str(value), ID,str(row))
    q.exec_(tbvalue)

def add_SQLtable_value(tablename,value):
    conn = sqlite3.connect(sql_path)
    c = conn.cursor()
    data='INSERT INTO {} VALUES ({});'.format(tablename,value)
    c.execute(data)
    conn.commit()
    conn.close()

def get_datatype(column_name):
    '''
    get datatype with a column name
    '''
    if isinstance(column_name, int):
        datatype = "int"
    elif isinstance(column_name, float):
        datatype = "float"
    elif isinstance(column_name, dict):
        datatype = "dict"
    else:
        datatype = "text"
    return datatype

def get_SQLtable_row(tbname, row,db):
    '''
    get a list of all value at a certain row from given table
    '''
    name_list = get_SQLtable_column_name(tbname,db)
    num = len(name_list)
    q = QSqlQuery(db)
    command = "SELECT * FROM {} WHERE ID={};".format(tbname, str(row))
    value_list = []
    if q.exec_(command):
        while q.next():
            for i in range(0, num):
                value = q.value(i)
                value_list.append(value)
    return value_list

# tbname='timeseries'
# value_list=get_SQLtable_row(tbname, 1,db)
# print(value_list)

def get_SQLtable_row_max(tbname,rowid):
    # get the max rowid
    conn = sqlite3.connect(sql_path)
    c = conn.cursor()
    c.execute("SELECT max({}) from {}".format(rowid,tbname))
    n = c.fetchone()[0]
    return n

def get_SQLtable_all_value(tbname,db):
    colname=get_SQLtable_column_name(tbname, db)
    data=[]
    rows=get_SQLtable_row_max(tbname, 'ID')
    if rows==1:
        row_data = get_SQLtable_row(tbname,1, db)
        data.append(row_data)
    else:
        for row in range(1,rows+1):
            row_data=get_SQLtable_row(tbname, row, db)
            data.append(row_data)
    return data,colname

def get_SQLtable_value(tbname, column, row,db):
    '''
    get value based on a certain column name and a certain rowid from given table
    '''
    q = QSqlQuery(db)
    command = "SELECT {} FROM {} WHERE ID={};".format(column, tbname, str(row))
    q.exec_(command)
    if q.next():
        result = q.value(0)
        return result

# tbname='component'
# result=get_SQLtable_value(tbname,'sector',1,db)
# print(result)

def copy_tbname1_to_tbname2(tbname1,tbname2,ID):
    conn = sqlite3.connect(sql_path)
    c = conn.cursor()
    sql='''INSERT INTO {} 
            SELECT * FROM {} WHERE ID={};'''.format(tbname1,tbname2,ID)
    c.execute(sql)
    conn.commit()
    conn.close()

def add_SQLtable_row_value(tbname,data_list,sqlpath=sql_path):
    conn = sqlite3.connect(sqlpath)
    cursor = conn.cursor()
    sql_insert = "INSERT INTO {} VALUES".format(tbname)
    sql_values = ""
    for i in range(0, len(data_list)):
        sql_values += '('
        sql_values += str(data_list[i])
        sql_values += '),'
    sql_values = sql_values.strip(',')
    sql_todo = sql_insert + sql_values
    cursor.execute(sql_todo)
    conn.commit()
    conn.close()

def new_colname_order(tbname,colname):
    if tbname=='component_tb':
        colname[3],colname[6]=colname[6],colname[3]
        colname[4], colname[7] = colname[7], colname[4]
    return colname


def realign_tbname(tbname,sqlpath=sql_path,database=db):
    newtable = 'NEWTABLE'
    delete_SQLtable([newtable])
    create_SQLtable(newtable, database)
    conn = sqlite3.connect(sqlpath)
    df = pd.read_sql_query("SELECT * FROM {}".format(tbname), conn)
    colname=list(df)
    order=new_colname_order(tbname, colname)
    data = df[order]

    for i in order:
        add_SQLtable_column(newtable, i)
    conn.commit()
    data.to_sql(newtable, conn, if_exists='append', index=False)
    conn.close()
    delete_SQLtable([tbname])
    rename_SQLtable(newtable, tbname)

# realign_tbname('component_tb')





def value_of_list(list):
    text=''
    for i in list:
        text+=i
        text += ","
    return text


def add_data_SQLtable_option(value,row):
    if isinstance(value, dict):
        if len(value) == 1:
            for key in value.keys():
                value = key
        elif len(value) > 1:
            value = "image"
        return value
    elif isinstance(value, list):
        if len(value) > 100:
            list_value=scenario_value_processed(value,row)
            return "Click"
        else:
            text=value_of_list(value)
            return text
    elif "T" in str(value) and ":" in str(value):
        # i="2019-2-15T15:00:00"
        value_time = datetime.datetime.strptime(value, '%Y-%m-%dT%H:%M:%S')
        return str(value_time)
    else:
        return value
# Timeseries
def scenario_value_processed(value,row):
    list_value = []
    sum_value = 'the sum of list {}'.format(len(value))
    list_value.append(sum_value)
    mean_value = 'the mean of list {}'.format(np.mean(value))
    list_value.append(mean_value)
    plot = 'the value of plot'
    timeseries_value_plot(value,row)
    list_value.append(plot)
    csv = 'the value of csv.file'
    list_value.append(csv)
    return ','.join(list_value)

def timeseries_value_plot(value,row):
    x_value=[item for item in range(len(value))]
    plt.plot(x_value,value)
    dataframe = pd.DataFrame({'number': x_value, 'value': value})
    path = os.getcwd()
    csv_path=path + '/GUI/cloud_database/timeseries_plot/ID_' + str(row)
    dataframe.to_csv(csv_path+".csv", index=False, sep=',')
    plt.savefig(path+'/GUI/cloud_database/timeseries_plot/ID_'+str(row))
    plt.clf()
    plt.close()
    # plt.show()value_plot
# y_value=[1,2,3]
# timeseries_value_plot(y_value,3)


def scenario_prosumer_tb(tbname1,tbname2,column,item,db):
    middle_table='{}_{}'.format(tbname1,tbname2)
    neu_table_name='{}_tb'.format(tbname2)
    middle_item='{}_id'.format(tbname2)
    delete_SQLtable([neu_table_name,])
    create_SQLtable(neu_table_name, db)
    result=get_SQLtable_column(tbname1, column,db)
    id=result.index(item)+1
    result = get_SQLtable_value(middle_table, middle_item, id,db)
    result=get_SQLtable_row('prosumer', result ,db)
    data = DataFrame(result)
    data = data.T
    column_list = get_SQLtable_column_name(tbname2, db)
    for column in column_list:
        add_SQLtable_cloumn(neu_table_name, column, 'text', db)
    row_num = data.shape[0]
    columns_num = data.shape[1]
    column_name_list = data.columns.values.tolist()
    add_SQLtable_row(neu_table_name, row_num, db)
    for row in range(1, row_num + 1):
        for column_name in column_name_list:
            value = data.at[row - 1, column_name]
            value = add_data_SQLtable_option(value, row)
            set_SQLtable_value(neu_table_name, column_list[column_name], row, value, db)
    print("{} table value insert successfully".format(tbname2))
    return result

# tbname1 = 'scenario'
# tbname2 = 'prosumer'
# columnname='scenario'
# item='Aachen'
# result=scenario_prosumer_tb(tbname1,tbname2,columnname,item,db)
# print(result)
def update_SQLtable_id(tbname,db):
    maxrow = get_SQLtable_row_max(tbname, 'ID')
    for i in range(1,maxrow+1):
        set_SQLtable_value(tbname, 'ID', i, i, db)


def scenario_component_tb(tbname1,tbname2,column,item,db):
    '''
    create prosumer component tb
    '''
    middle_table = '{}_{}'.format(tbname1, tbname2)
    neu_table_name = '{}_tb'.format(tbname2)
    middle_item = '{}_id'.format(tbname2)
    # delete_SQLtable(['component_tb'])
    create_SQLtable(neu_table_name, db)
    neu_tbname1=tbname1+'_tb'
    result = get_SQLtable_column(neu_tbname1, column, db)
    id = result.index(item) + 1
    maxrow=get_SQLtable_row_max(middle_table, 'ID')
    id_list=[]
    column_list = get_SQLtable_column_name(tbname2, db)
    for column in column_list:
        add_SQLtable_cloumn(neu_table_name, column, 'text', db)
    for i in range(1,maxrow+1):
        if get_SQLtable_value(middle_table, 'prosumer_id', i, db)==str(1):
            copy_tbname1_to_tbname2(neu_table_name, tbname2, i)
            update_SQLtable_id(neu_table_name,db)
            id_list.append(i)
    add_colname=['current_size', 'min_size', 'max_size']
    for columnname in add_colname:
        add_SQLtable_column(neu_table_name, columnname)

    for id in id_list:
        for colname in add_colname:
            value=get_SQLtable_value(middle_table, colname, id, db)
            set_SQLtable_value(neu_table_name, colname, id, value, db)
    print("{}_tb value insert successfully".format(tbname2))

    return result

# tbname1 = 'prosumer'
# tbname2 = 'component'
# columnname='prosumer_name'
# item='heatpump_household_AC'
# result=scenario_component_tb(tbname1,tbname2,columnname,item,db)
# print(result)

def scenario_data_tb(tbname1,tbname2,column,item,db):
    '''
    create component data table
    '''
    middle_table='{}_{}'.format(tbname1,tbname2)
    neu_table_name='{}_{}_tb'.format(item,tbname2)
    neu_tbname1='{}_tb'.format(tbname1)
    delete_SQLtable([neu_table_name,])
    create_SQLtable(neu_table_name, db)
    maxrow=get_SQLtable_row_max(neu_tbname1, 'ID')
    for i in range(1,maxrow+1):
        if get_SQLtable_value(neu_tbname1, column, i,db)==item:
            comp_ID=i
            break
    comp_data_ID=[]
    maxrow_comp_data = get_SQLtable_row_max(middle_table, 'ID')
    for i in range(1, maxrow_comp_data+ 1):
        if get_SQLtable_value(middle_table, 'comp_id', i, db) == str(comp_ID):
            data_ID = get_SQLtable_value(middle_table, 'data_id', i, db)
            comp_data_ID.append(data_ID)
    result=comp_data_ID
    column_list = get_SQLtable_column_name(tbname2, db)
    for column in column_list:
        add_SQLtable_cloumn(neu_table_name, column, 'text', db)
    row_num=len(result)
    add_SQLtable_row(neu_table_name, row_num, db)
    for i in range(1,row_num+1):
        for n in range(1,len(column_list)):
            value=get_SQLtable_value(tbname2, column_list[n], result[i-1], db)
            set_SQLtable_value(neu_table_name, column_list[n], i, value, db)
    return result

# tbname1 = 'component'
# tbname2 = 'data'
# columnname='comp_type'
# item='Inverter'
# result=scenario_data_tb(tbname1,tbname2,columnname,item,db)
# print(result)

def scenario_add_scalar_tb(comp,db):
    comp_table_name = '{}_data_tb'.format(comp)
    comp_data_maxID=get_SQLtable_row_max(comp_table_name,'ID')

    columnname='value'
    add_SQLtable_cloumn(comp_table_name,columnname , 'text', db)
    for data_id in range(1,comp_data_maxID+1):
        value = get_SQLtable_value('scalar', columnname, data_id, db)
        set_SQLtable_value(comp_table_name, columnname,data_id , value, db)

# comp='HeatPump'
# scenario_add_scalar_tb(comp,db)

def scenario_delete_scalar_tb(comp,db):
    comp_table_name = '{}_data_tb'.format(comp)
    neu_comp_table_name='{}_data_scalar_tb'.format(comp)
    create_SQLtable(neu_comp_table_name, db)
    comp_data_maxID=get_SQLtable_row_max(comp_table_name,'ID')
    add_SQLtable_row(neu_comp_table_name, comp_data_maxID, db)
    colname=get_SQLtable_column_name(comp_table_name, db)
    colname.remove('value')
    for columnname in colname:
        add_SQLtable_column(neu_comp_table_name, columnname)
    for id in range(1,comp_data_maxID+1):
        for columnname in colname:
            value=get_SQLtable_value(comp_table_name, columnname, id, db)
            set_SQLtable_value(neu_comp_table_name, columnname, id, value, db)
    # delete_SQLtable([comp_table_name,])
    table_name='{}_scalar_tb'.format(comp)
    rename_SQLtable(comp_table_name, table_name)
    rename_SQLtable(neu_comp_table_name, comp_table_name)

# comp='HeatPump'
# scenario_delete_scalar_tb(comp,db)

def df_insert_tb(table_pf,tbname,db):
    row_num = table_pf.shape[0]
    columns_num = table_pf.shape[1]
    column_name_list = table_pf.columns.values.tolist()
    for i in range(len(column_name_list)):
        column_name = column_name_list[i]
        datatype = get_datatype(column_name)
        add_SQLtable_cloumn(tbname, column_name, 'text', db)
        add_SQLtable_row(tbname, row_num, db)
    for row in range(1, row_num + 1):
        for column_name in column_name_list:
            value = table_pf.at[row - 1, column_name]
            value = add_data_SQLtable_option(value, row)
            set_SQLtable_value(tbname, column_name, row, value, db)

    # print("{} table value insert successfully".format(tbname))

def add_SQLtable(table_name,db):
    '''
    add table in database from web open platform
    '''
    delete_SQLtable(table_name) # delect all EXISTS table
    for tbname in table_name:
        # tbname=table_name[5]
        create_SQLtable(tbname, db)
        pre_path = 'https://openenergy-platform.org/api/v0/schema/model_draft/tables/ineed_'
        path = pre_path + tbname + '/rows'
        table_pf = requests.get(path)
        table_pf = pd.DataFrame(table_pf.json())
        df_insert_tb(table_pf, tbname,db)


    # Prosumer table:
    prosumer_name_list=get_SQLtable_column(table_name[0], 'scenario',db)
    for item in prosumer_name_list:
        result=scenario_prosumer_tb(table_name[0],table_name[1],'scenario',item,db)
    # # print(result)
    # component table:
    # tbname1 = 'prosumer'
    # tbname2 = 'component'
    # columnname='prosumer_name'
    # item='heatpump_household_AC'
    comp_name_list = get_SQLtable_column(table_name[1], 'prosumer_name', db)
    for item in comp_name_list:
        result=scenario_component_tb(table_name[1],table_name[2],'prosumer_name',item,db)
        # print(result)
    # tbname1 = 'component'
    # tbname2 = 'data'
    # columnname='comp_type'
    # item='Inverter'
    comp_type_list=get_SQLtable_column(table_name[2]+'_tb', 'comp_type', db)
    for comp in comp_type_list:
        result=scenario_data_tb(table_name[2],table_name[3],'comp_type',comp,db)
        scenario_add_scalar_tb(comp, db)
        scenario_delete_scalar_tb(comp, db)

# table_name=['scenario','prosumer','component','data','scalar','timeseries','scenario_prosumer','prosumer_component','component_data']
# add_SQLtable(table_name,db)



def update_database(tablelist,db):
    '''
    update database that data come from open source web-platform
    '''
    conn = sqlite3.connect(sql_path)
    delete_all_SQLtable(db)
    # table_name=['scenario','prosumer','component','data','scalar','timeseries']
    add_SQLtable(tablelist,db)


def get_SQLtable_id(tbname,columnname,item,db):
    column_value_list=get_SQLtable_column(tbname, columnname, db)
    result=column_value_list.index(item)+1
    return result
# tbname='scenario'
# columnname='scenario'
# item='Aachen'
# result=get_SQLtable_id(tbname,columnname,item,db)
# print(result)


def add_data_SQLtable(table_name):
    delete_SQLtable(table_name)
    for tbname in table_name:
        # tbname=table_name[2]
        # table_name_pd=dict.fromkeys([tbname])
        pre_path = 'https://openenergy-platform.org/api/v0/schema/model_draft/tables/ineed_'
        path = pre_path + tbname+ '/rows'
        table_pf = requests.get(path)
        table_pf = pd.DataFrame(table_pf.json())
        text=get_columns_index(tbname)
        # print(text)
        add_SQLtable_columnsIndex(tbname,text)
        for index, row in table_pf.iterrows():
            row_list=row.tolist()
            value=''
            for i in row_list:
                # print(type(i))
                try:
                        if isinstance(i,int):
                            i=str(i)
                        elif isinstance(i,float):
                            i=str(i)
                        elif len(i) > 100:
                            i = 'the sum of list {}'.format(len(i))
                        elif "T" in i and ":" in i:
                            # i="2019-2-15T15:00:00"
                            a =datetime.datetime.strptime(i, '%Y-%m-%dT%H:%M:%S')
                            i=str(a)
                        elif "/" in i:
                            i = "None"
                        elif isinstance(i,str):
                            pass
                        elif isinstance(i,dict):
                            if len(i)==1:
                                for key in i.keys():
                                    i=key
                            elif i == {}:
                                i = "None"
                            # elif len(i)>1:
                            #     i="image"
                except Exception as e:
                    i ="None"

                value= value + '"'+str(i)+'"'+ ',' + '\n'
            value=value[:-2]
            # print(value)

            add_SQLtable_value(tbname,value)
# add_data_SQLtable(table_name)

# table_pd.to_sql('scenario', conn, if_exists="replace")
# table_index = table_pd.columns.values.tolist()
# table_name_pd[tbname]=table_pd

# ___________________________________________________
def prosumer_topo(ID,colname,tbname='prosumer'):

    pre_path = 'https://openenergy-platform.org/api/v0/schema/model_draft/tables/ineed_'
    path = pre_path + tbname + '/rows'
    table_pf = requests.get(path)
    table_pf = pd.DataFrame(table_pf.json())
    data=table_pf.iloc[ID-1,colname]
    return data


def get_all_tableheader(database=db):
    tblist = get_SQLtable_column('sqlite_sequence','name', database)
    tbheader={}
    for tbname in tblist:
        tbname_header=get_SQLtable_column_name(tbname, database)
        tbheader[tbname] = tbname_header
    return tbheader
#
# result=get_all_tableheader()
# pprint(result)

    #Testing____________________________________________________________________-
# sql_name='database.sqlite'
# db=create_database(sql_name)
# if db.open():
#      print('Database opened successfully')
# else:
#      print(db.lastError().text())
#
# table_name=['scenario','prosumer','component','data','scalar','timeseries']
# update_database(table_name,db)


# class TableModel(QtCore.QAbstractTableModel):
#     def __init__(self, data):
#         super().__init__()
#         self._data = data
#     def data(self, index, role):
#         if role == Qt.DisplayRole:
#             value = self._data.iloc[index.row(), index.column()]
#             return str(value)
#     def rowCount(self, index):
#         return self._data.shape[0]
#     def columnCount(self, index):
#         return self._data.shape[1]
#     def headerData(self, section, orientation, role):
#         if role == Qt.DisplayRole:
#             if orientation == Qt.Horizontal:
#                 return str(self._data.columns[section])
#         if orientation == Qt.Vertical:
#             return str(self._data.index[section])
 # def scenario_datatable(self,tbname,sum_path):
    #     path=sum_path[tbname]
    #     result = requests.get(path)
    #     table = pd.DataFrame(result.json())
    #     try:
    #         self.datamodel = TableModel(table)
    #         self.ui.table_model1.setModel(self.datamodel)
    #     except Exception as exc:
    #         print('There was a problem : %s' % (exc))
    #     return table