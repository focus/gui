# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'MainWindowqnckYl.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

import imag.resource

class Ui_Prosumer_Modeling(object):
    def setupUi(self, Prosumer_Modeling):
        if not Prosumer_Modeling.objectName():
            Prosumer_Modeling.setObjectName(u"Prosumer_Modeling")
        Prosumer_Modeling.resize(830, 532)
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(Prosumer_Modeling.sizePolicy().hasHeightForWidth())
        Prosumer_Modeling.setSizePolicy(sizePolicy)
        Prosumer_Modeling.setMinimumSize(QSize(0, 0))
        Prosumer_Modeling.setMaximumSize(QSize(16777215, 16777215))
        icon = QIcon()
        icon.addFile(u":/imag/RWTH_Logo.svg", QSize(), QIcon.Normal, QIcon.Off)
        Prosumer_Modeling.setWindowIcon(icon)
        self.actionStart = QAction(Prosumer_Modeling)
        self.actionStart.setObjectName(u"actionStart")
        icon1 = QIcon()
        icon1.addFile(u":/imag/RWTH_Piktogramm_Play.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionStart.setIcon(icon1)
        self.actionSave = QAction(Prosumer_Modeling)
        self.actionSave.setObjectName(u"actionSave")
        icon2 = QIcon()
        icon2.addFile(u":/imag/RWTH_Piktogramm_Speicherkarte.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionSave.setIcon(icon2)
        self.actionExit = QAction(Prosumer_Modeling)
        self.actionExit.setObjectName(u"actionExit")
        icon3 = QIcon()
        icon3.addFile(u":/imag/RWTH_Piktogramm_Falsch.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionExit.setIcon(icon3)
        self.actionGenerator = QAction(Prosumer_Modeling)
        self.actionGenerator.setObjectName(u"actionGenerator")
        self.actionNew = QAction(Prosumer_Modeling)
        self.actionNew.setObjectName(u"actionNew")
        self.actionOpen = QAction(Prosumer_Modeling)
        self.actionOpen.setObjectName(u"actionOpen")
        self.centralwidget = QWidget(Prosumer_Modeling)
        self.centralwidget.setObjectName(u"centralwidget")
        self.centralwidget.setMinimumSize(QSize(800, 0))
        self.gridLayout = QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName(u"gridLayout")
        self.groupBox = QGroupBox(self.centralwidget)
        self.groupBox.setObjectName(u"groupBox")
        sizePolicy1 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.groupBox.sizePolicy().hasHeightForWidth())
        self.groupBox.setSizePolicy(sizePolicy1)
        self.verticalLayout_2 = QVBoxLayout(self.groupBox)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.btn_initialization = QPushButton(self.groupBox)
        self.btn_initialization.setObjectName(u"btn_initialization")

        self.verticalLayout_2.addWidget(self.btn_initialization)

        self.btn_Configuration = QPushButton(self.groupBox)
        self.btn_Configuration.setObjectName(u"btn_Configuration")

        self.verticalLayout_2.addWidget(self.btn_Configuration)

        self.btn_Strategie = QPushButton(self.groupBox)
        self.btn_Strategie.setObjectName(u"btn_Strategie")

        self.verticalLayout_2.addWidget(self.btn_Strategie)

        self.btn_Date = QPushButton(self.groupBox)
        self.btn_Date.setObjectName(u"btn_Date")

        self.verticalLayout_2.addWidget(self.btn_Date)

        self.btn_Componente = QPushButton(self.groupBox)
        self.btn_Componente.setObjectName(u"btn_Componente")

        self.verticalLayout_2.addWidget(self.btn_Componente)

        self.btn_Profile = QPushButton(self.groupBox)
        self.btn_Profile.setObjectName(u"btn_Profile")

        self.verticalLayout_2.addWidget(self.btn_Profile)

        self.btn_Optimization = QPushButton(self.groupBox)
        self.btn_Optimization.setObjectName(u"btn_Optimization")

        self.verticalLayout_2.addWidget(self.btn_Optimization)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_2.addItem(self.verticalSpacer)


        self.gridLayout.addWidget(self.groupBox, 0, 0, 3, 1)

        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.label = QLabel(self.centralwidget)
        self.label.setObjectName(u"label")

        self.verticalLayout.addWidget(self.label)

        self.widget = QWidget(self.centralwidget)
        self.widget.setObjectName(u"widget")

        self.verticalLayout.addWidget(self.widget)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.textBrowser = QTextBrowser(self.centralwidget)
        self.textBrowser.setObjectName(u"textBrowser")

        self.horizontalLayout.addWidget(self.textBrowser)

        self.pButtonClear = QPushButton(self.centralwidget)
        self.pButtonClear.setObjectName(u"pButtonClear")

        self.horizontalLayout.addWidget(self.pButtonClear)

        self.horizontalLayout.setStretch(0, 9)
        self.horizontalLayout.setStretch(1, 1)

        self.verticalLayout.addLayout(self.horizontalLayout)

        self.verticalLayout.setStretch(1, 7)
        self.verticalLayout.setStretch(2, 3)

        self.gridLayout.addLayout(self.verticalLayout, 0, 1, 1, 1)

        self.line = QFrame(self.centralwidget)
        self.line.setObjectName(u"line")
        self.line.setFrameShape(QFrame.HLine)
        self.line.setFrameShadow(QFrame.Sunken)

        self.gridLayout.addWidget(self.line, 1, 1, 1, 1)

        Prosumer_Modeling.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(Prosumer_Modeling)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 830, 22))
        self.menuStart = QMenu(self.menubar)
        self.menuStart.setObjectName(u"menuStart")
        self.menuAbout_us = QMenu(self.menubar)
        self.menuAbout_us.setObjectName(u"menuAbout_us")
        self.menuImport = QMenu(self.menubar)
        self.menuImport.setObjectName(u"menuImport")
        self.menuExport = QMenu(self.menubar)
        self.menuExport.setObjectName(u"menuExport")
        self.menuTools = QMenu(self.menubar)
        self.menuTools.setObjectName(u"menuTools")
        self.menuWindows = QMenu(self.menubar)
        self.menuWindows.setObjectName(u"menuWindows")
        self.menuEdit = QMenu(self.menubar)
        self.menuEdit.setObjectName(u"menuEdit")
        self.menuOptions = QMenu(self.menubar)
        self.menuOptions.setObjectName(u"menuOptions")
        Prosumer_Modeling.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(Prosumer_Modeling)
        self.statusbar.setObjectName(u"statusbar")
        Prosumer_Modeling.setStatusBar(self.statusbar)
        self.toolBar = QToolBar(Prosumer_Modeling)
        self.toolBar.setObjectName(u"toolBar")
        Prosumer_Modeling.addToolBar(Qt.TopToolBarArea, self.toolBar)

        self.menubar.addAction(self.menuStart.menuAction())
        self.menubar.addAction(self.menuEdit.menuAction())
        self.menubar.addAction(self.menuImport.menuAction())
        self.menubar.addAction(self.menuExport.menuAction())
        self.menubar.addAction(self.menuTools.menuAction())
        self.menubar.addAction(self.menuWindows.menuAction())
        self.menubar.addAction(self.menuOptions.menuAction())
        self.menubar.addAction(self.menuAbout_us.menuAction())
        self.menuStart.addAction(self.actionStart)
        self.menuStart.addAction(self.actionSave)
        self.menuStart.addAction(self.actionNew)
        self.menuStart.addAction(self.actionOpen)
        self.menuAbout_us.addAction(self.actionExit)
        self.menuTools.addAction(self.actionGenerator)
        self.toolBar.addAction(self.actionStart)
        self.toolBar.addAction(self.actionSave)
        self.toolBar.addAction(self.actionExit)
        self.toolBar.addAction(self.actionGenerator)

        self.retranslateUi(Prosumer_Modeling)

        QMetaObject.connectSlotsByName(Prosumer_Modeling)
    # setupUi

    def retranslateUi(self, Prosumer_Modeling):
        Prosumer_Modeling.setWindowTitle(QCoreApplication.translate("Prosumer_Modeling", u"MainWindow", None))
        self.actionStart.setText(QCoreApplication.translate("Prosumer_Modeling", u"Run", None))
        self.actionSave.setText(QCoreApplication.translate("Prosumer_Modeling", u"Save", None))
        self.actionExit.setText(QCoreApplication.translate("Prosumer_Modeling", u"Exit", None))
        self.actionGenerator.setText(QCoreApplication.translate("Prosumer_Modeling", u"Generator ", None))
        self.actionNew.setText(QCoreApplication.translate("Prosumer_Modeling", u"& New", None))
        self.actionOpen.setText(QCoreApplication.translate("Prosumer_Modeling", u"Open", None))
        self.groupBox.setTitle(QCoreApplication.translate("Prosumer_Modeling", u"Scenario", None))
        self.btn_initialization.setText(QCoreApplication.translate("Prosumer_Modeling", u"Initialization", None))
        self.btn_Configuration.setText(QCoreApplication.translate("Prosumer_Modeling", u"Configuration", None))
        self.btn_Strategie.setText(QCoreApplication.translate("Prosumer_Modeling", u"Strategies", None))
        self.btn_Date.setText(QCoreApplication.translate("Prosumer_Modeling", u"Date", None))
        self.btn_Componente.setText(QCoreApplication.translate("Prosumer_Modeling", u"Componente", None))
        self.btn_Profile.setText(QCoreApplication.translate("Prosumer_Modeling", u"Profile", None))
        self.btn_Optimization.setText(QCoreApplication.translate("Prosumer_Modeling", u"Optimisation", None))
        self.label.setText(QCoreApplication.translate("Prosumer_Modeling", u"Flow Chart", None))
        self.textBrowser.setHtml(QCoreApplication.translate("Prosumer_Modeling", u"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'SimSun'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>", None))
        self.pButtonClear.setText(QCoreApplication.translate("Prosumer_Modeling", u"clear", None))
        self.menuStart.setTitle(QCoreApplication.translate("Prosumer_Modeling", u"File", None))
        self.menuAbout_us.setTitle(QCoreApplication.translate("Prosumer_Modeling", u"About us ", None))
        self.menuImport.setTitle(QCoreApplication.translate("Prosumer_Modeling", u"Import", None))
        self.menuExport.setTitle(QCoreApplication.translate("Prosumer_Modeling", u"Export", None))
        self.menuTools.setTitle(QCoreApplication.translate("Prosumer_Modeling", u"Tools", None))
        self.menuWindows.setTitle(QCoreApplication.translate("Prosumer_Modeling", u"Window", None))
        self.menuEdit.setTitle(QCoreApplication.translate("Prosumer_Modeling", u"Edit", None))
        self.menuOptions.setTitle(QCoreApplication.translate("Prosumer_Modeling", u"Options", None))
        self.toolBar.setWindowTitle(QCoreApplication.translate("Prosumer_Modeling", u"toolBar", None))
    # retranslateUi

