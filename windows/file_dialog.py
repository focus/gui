
import sys

from PySide2.QtUiTools import QUiLoader
from PySide2.QtWidgets import  QApplication,QFileDialog

import os
os.chdir('/project/prosumer_modeling')




class open_file_Dialog:
    def __init__(self):
        self.ui = QUiLoader().load('GUI/ui_files/Open_file_dialog.ui')
        self.ui.qlineEdit_selectfile.setText('file path...')
        self.ui.setWindowTitle("Open file")
        self.ui.resize(600,300)


        self.ui.btn_select.clicked.connect(self.select_file)
        self.ui.btn_open.clicked.connect(self.open_file)
        self.ui.btn_cancel.clicked.connect(self.cancel_file)
        self.ui.btn_open.clicked.connect(self.open_file)

    def select_file(self):
        """Select file
        """
        filename, _ = QFileDialog.getOpenFileName(self.ui, "getOpenFileName",
                                                  '../',  # Starting path of the file
                                                  "All Files (*);;JSON Files (*.json)")  # Setup the file type
        self.ui.qlineEdit_selectfile.setText(filename)

    def open_file(self):
        """Open using the system's application
        """
        os.startfile(self.ui.qlineEdit_selectfile.text())

    def cancel_file(self):
        self.ui.close()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    app.setStyle('Fusion')
    ofl = open_file_Dialog()
    ofl .ui.show()
    sys.exit(app.exec_())