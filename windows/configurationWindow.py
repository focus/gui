import queue
import sys
import threading
import time

import pandas as pd
from PySide2 import QtWidgets, QtCore
from PySide2.QtCore import Signal, QThread, QMutex, QObject
from PySide2.QtGui import Qt, QBrush

import GUI.windows.window_db as window_db
import qtmodern
from PySide2.QtSql import QSqlDatabase
from PySide2.QtWidgets import QMainWindow, QApplication, QMessageBox, QAbstractItemView, QHeaderView, QTableWidgetItem, \
    QWidget
from PySide2.QtUiTools import QUiLoader

from GUI.windows.share import SW
import qtmodern.styles

import os
os.chdir('/project/prosumer_modeling')
path=os.getcwd()
sql_path=path+'//GUI//windows//windows_DB.sqlite'
win_db = QSqlDatabase("QSQLITE")
win_db.setDatabaseName(sql_path)

class TableModel(QtCore.QAbstractTableModel):
    def __init__(self, data,HEADERS):
        super(TableModel, self).__init__()
        self._data = data
        self.headers = HEADERS

    def data(self, index, role):
        if role == Qt.DisplayRole:
            # See below for the nested-list data structure.
            # .row() indexes into the outer list,
            # .column() indexes into the sub-list
            return self._data[index.row()][index.column()]

    def rowCount(self, index):
        # The length of the outer list.
        return len(self._data)

    def columnCount(self, index):
        # The following takes the first sub-list, and returns
        # the length (only works if all rows are an equal length)
        return len(self._data[0])
    def headerData(self, section, orientation, role=Qt.DisplayRole):
        if role!=Qt.DisplayRole:
            return None
        if orientation!=Qt.Vertical:
            return self.headers[section]
        return int(section+1)



class config_win(QtWidgets.QWidget):
    config_signal=Signal()
    data_signal=Signal(str)
    def __init__(self):
        super(config_win,self).__init__()
        # Load the UI definition from the file
        # Dynamically create a corresponding window object from the UI definition
        self.ui = QUiLoader().load('GUI/ui_files/Configuration.ui')
        self.ui.setWindowTitle("Configuration Window")
        self.ui.resize(2000, 800)
        self.init()

        self.ui.btn_save.clicked.connect(self.save_data)
        self.ui.add_column.clicked.connect(self.addColumn)
        self.ui.add_row.clicked.connect(self.addRow)
        self.ui.btn_cancel.clicked.connect(self.cancel_data)
        scenario_list = self.get_scenario_name()
        self.ui.cb_scenario.addItems(scenario_list)
        config_list = self.get_config_item()
        self.ui.cb_config.addItems(config_list)
        self.ui.cb_config.currentTextChanged.connect(self.config_display)

        # setting properties of Table vew
        # self.ui.tb_config.setSelectionBehavior(QAbstractItemView.SelectRows)
        # self.ui.tb_config.setSelectionMode(QAbstractItemView.SingleSelection)
        # self.ui.tb_config.setAlternatingRowColors(True)
        self.ui.tb_config.verticalHeader().setDefaultSectionSize(22)
        self.ui.tb_config.horizontalHeader().setDefaultSectionSize(60)
        self.ui.show()

    def init(self):
        config_name=self.ui.config_name.currentText() #Office
        add_config = window_db.get_tb(config_name)
        self.ui.injection_price.setText(str(add_config['injection_price']))
        self.ui.injection_pvpeak.setText(str(add_config['injection_pvpeak']))
        self.ui.gas_price.setText(str(add_config['gas_price']))
        self.ui.elec_price.setText(str(add_config['elec_price']))
        self.ui.heat_price.setText(str(add_config['heat_price']))
        self.ui.cooling_price.setText(str(add_config['cooling_price']))

        self.ui.injection_price_gas.setText(str(add_config['injection_price_gas'])) #====

        self.ui.injection_price_heat.setText(str(add_config['injection_price_heat']))

        self.ui.injection_price_cooling.setText(str(add_config['injection_price_cooling']))
        self.ui.elec_emission.setText(str(add_config['elec_emission']))
        self.ui.gas_emission.setText(str(add_config['gas_emission']))
        self.ui.yearly_interest.setText(str(add_config['yearly_interest']))
        self.ui.planning_horizon.setText(str(add_config['planning_horizon']))

    def get_scenario_name(self):
        scenario_list = ['']
        filePath = './input_files/'
        file_list = os.listdir(filePath)
        for i in file_list:
            if '.' not in i:
                scenario_list.append(i)
        return scenario_list

    def get_config_item(self):
        scenario_list = ['']
        config_filePath = './prosumer_library/database/'
        file_list = os.listdir(config_filePath)
        for i in file_list:
                scenario_list.append(i)
        return scenario_list

    def config_display(self):
        # select_scenario = self.ui.cb_scenario.currentText()
        select_config = self.ui.cb_config.currentText()
        matrix_filePath = './prosumer_library/database/' + select_config
        csv_data = pd.read_csv(matrix_filePath, low_memory=False)
        data = pd.DataFrame(csv_data)
        headers = list(data.columns)
        self.ui.tb_config.clear()
        self.on_btnSetHeader_clicked(headers)

        self.ui.tb_config.setColumnCount(data.shape[1])
        self.ui.tb_config.setRowCount(data.shape[0])
        data = data.values.tolist()

        for i in range(len(data)):
            item = data[i]
            row = self.ui.tb_config.rowCount()-1
            self.ui.tb_config.insertRow(row)
            for j in range(len(item)):
                item = QTableWidgetItem(str(data[i][j]))
                self.ui.tb_config.setItem(row, j, item)
        self.ui.tb_config.setRowHidden(1, True)
        # self.datamodel = TableModel(data, headers)
        # self.ui.tb_config.setModel(self.datamodel)
        # self.ui.tb_config.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)

    def on_btnSetHeader_clicked(self,headers):
        headerText = headers
        self.ui.tb_config.setColumnCount(len(headerText))  # column
        for i in range(len(headerText)):
            headerItem = QTableWidgetItem(headerText[i])
            font = headerItem.font()
            font.setBold(True)
            font.setPointSize(12)
            headerItem.setFont(font)
            headerItem.setForeground(QBrush(Qt.blue))
            self.ui.tb_config.setHorizontalHeaderItem(i, headerItem)
            self.ui.tb_config.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)

    def getConfig(self):

        injection_price=float(self.ui.injection_price.text())
        injection_pvpeak=float(self.ui.injection_pvpeak.text())
        gas_price=float(self.ui.gas_price.text())
        elec_price=float(self.ui.elec_price.text())
        heat_price=float(self.ui.heat_price.text())
        cooling_price=float(self.ui.cooling_price.text())
        injection_price_gas=float(self.ui.injection_price_gas.text())  # injection_price_gas

        injection_price_heat=float(self.ui.injection_price_heat.text())
        injection_price_cooling=float(self.ui.injection_price_cooling.text())
        elec_emission=float(self.ui.elec_emission.text())
        gas_emission=float(self.ui.gas_emission.text())
        yearly_interest=float(self.ui.yearly_interest.text())
        planning_horizon=float(self.ui.planning_horizon.text())
        return {'injection_price': injection_price,
                          # 2019 # changes with size!
                          # bis10kwp: 8.16 (01.01.21), bis 40: 7.93
                          # (https://www.solaranlagen-portal.com/photovoltaik/wirtschaftlichkeit/einspeiseverguetung)
                          'injection_pvpeak': injection_pvpeak,
                          'gas_price': gas_price,  # 2019 https://www.verivox.de/gas/verbraucherpreisindex/
                          'elec_price': elec_price,  # Januar 2021 https://www.vergleich.de/strompreise.html
                          'heat_price': heat_price,
                          'cooling_price': cooling_price,
                          'injection_price_gas':injection_price_gas,
                          'injection_price_heat': injection_price_heat,
                          'injection_price_cooling': injection_price_cooling,
                          'elec_emission': elec_emission,  # 2019
                          'gas_emission': gas_emission,
                          'yearly_interest':yearly_interest,  # letmathe
                          'planning_horizon': planning_horizon}

    def get_table(self):
        col_num = self.ui.tb_config.columnCount()
        row_num = self.ui.tb_config.rowCount()
        return col_num,row_num

    def addColumn(self):
        col, row = self.get_table()
        self.ui.tb_config.setColumnCount(col + 1)

    def addRow(self):
        col, row = self.get_table()
        self.ui.tb_config.setRowCount(row + 1)

    def save_data(self):
        # text
        col,row= self.get_table()
        text = self.ui.tb_config.itemAt(row-1, col).text()
        print(text)

        # add_config = self.getConfig()
        # config_name = self.ui.config_name.currentText()
        # window_db.update_config_parameter(add_config,config_name)
        self.ui.close()

    def cancel_data(self):
        self.ui.close()

if __name__ == '__main__':
    app = QApplication([])
    app.setStyle('Fusion')
    SW.config_win = config_win()
    # config=SW.config_win.getConfig()
    sys.exit(app.exec_())


