
from PySide2.QtCore import Qt, Signal
from PySide2.QtWidgets import QApplication, QMainWindow
from PySide2.QtUiTools import QUiLoader
import os
import sys
from GUI.windows.share import SW
from GUI.windows.initializationWindow import initi_win

os.chdir('/project/prosumer_modeling')

class profile_win(QMainWindow):
    profile_signal=Signal()
    def __init__(self, parent=None):
        super(profile_win, self).__init__(parent)
        # Load the UI definition from the file
        # Dynamically create a corresponding window object from the UI definition
        self.ui = QUiLoader().load('GUI/ui_files/Profile.ui')

        self.ui.setWindowTitle("Prosumer Profile Window")
        self.ui.resize(800, 300)

        self.default_profile()


    def default_profile(self):

        self.ui.rbtn_access1.setChecked(True)
        ini = initi_win()

        # ini_Prarm=ini.initialize_Parameter()
        # self.ui.start_time.setText(ini_Prarm[3])
        # self.ui.end_time.setText(str(ini_Prarm[4]))

        self.prediction()

        self.maximum()
        self.mean()
        self.ui.cbox_max.setChecked(False)

        # max_demand, mean_demand = ini.get_profile()
        # self.ui.cbox_max.setCheckState(Qt.Unchecked)
        # self.ui.cbox_max.stateChanged.connect(lambda: self.max_value(max_demand))
        # self.ui.cbox_mean.stateChanged.connect(lambda: self.mean_value(mean_demand))




        # if self.ui.cbox_max.setCheckState(Qt.Checked):
        #     self.max_value(max_demand)

        # self.ui.cbox_max.toggled.connect(self.max_value(max_demand))
        # self.ui.cbox_mean.stateChanged.connect(self.mean_value(mean_demand))

        #     self.ui.cbox_max.stateChanged.connect(self.max_value(max_demand))
            # self.ui.cbox_mean.stateChanged.connect(self.mean_value(mean_demand))


        # self.ui.summe_Profile.setText('2')
        # profile_item=int(self.ui.summe_Profile.text())
        # profile_item_list=[]
        # while profile_item>0:
        #     profile_item_list.append(str(profile_item))
        #     profile_item-=1
        # self.ui.profile_nr.addItems(profile_item_list)


    def maximum(self):
        self.ui.max_elec.setEnabled(False)
        self.ui.max_elec.setText('')
        self.ui.max_air.setEnabled(False)
        self.ui.max_bsrn.setEnabled(False)
        self.ui.max_hotwater.setEnabled(False)
        self.ui.max_therm.setEnabled(False)
    def max_value(self,max_demand):
        elec_demand=str(max_demand['elec_demand'])
        self.ui.max_elec.setText(elec_demand)
        air_temperature = str(max_demand['air_temperature'])
        self.ui.max_air.setText(air_temperature)
        irradiance = str(max_demand['irradiance'])
        self.ui.max_bsrn.setText(irradiance)
        hot_water_demand = str(max_demand['hot_water_demand'])
        self.ui.max_hotwater.setText(hot_water_demand)
        therm_demand = str(max_demand['therm_demand'])
        self.ui.max_therm.setText(therm_demand )
    def mean(self):
        self.ui.mean_elec.setEnabled(False)
        self.ui.mean_air.setEnabled(False)
        self.ui.mean_bsrn.setEnabled(False)
        self.ui.mean_hotwater.setEnabled(False)
        self.ui.mean_therm.setEnabled(False)
    def mean_value(self,mean_demand):
        elec_demand=str(mean_demand['elec_demand'])
        self.ui.mean_elec.setText(elec_demand)
        air_temperature = str(mean_demand['air_temperature'])
        self.ui.mean_air.setText(air_temperature)
        irradiance = str(mean_demand['irradiance'])
        self.ui.mean_bsrn.setText(irradiance)
        hot_water_demand = str(mean_demand['hot_water_demand'])
        self.ui.mean_hotwater.setText(hot_water_demand)
        therm_demand = str(mean_demand['therm_demand'])
        self.ui.mean_therm.setText(therm_demand)

    def prediction(self):
        self.ui.rbtn_perfect.setChecked(True)
        self.ui.rbtn_prophet.setChecked(False)
        self.ui.rbtn_samehourlastweek.setChecked(False)
        self.ui.rbtn_prophetpositive.setChecked(False)
        self.ui.rbtn_samehouryesterday.setChecked(False)


if __name__ == '__main__':
    app = QApplication([])
    app.setStyle('Fusion')
    # app.setWindowIcon(QIcon('images/RWTH_Logo.svg'))
    SW.profile_win = profile_win()
    SW.profile_win.ui.show()
    sys.exit(app.exec_())