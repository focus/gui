import queue
import threading

import pandas as pd
import time
from PySide2 import QtCore, QtWidgets
from PySide2.QtCore import Qt, Signal
from PySide2.QtGui import QImage, QPixmap, QIcon
from PySide2.QtWidgets import QMainWindow, QApplication, QMessageBox, QComboBox, QGraphicsPixmapItem, QGraphicsScene, \
    QHeaderView
from PySide2.QtUiTools import QUiLoader
import cv2
import os
import GUI.flowchart.icon as icon
os.chdir('/project/prosumer_modeling')
from GUI.windows.share import SW
import sys
import qtmodern.windows

class TableModel(QtCore.QAbstractTableModel):
    def __init__(self, data):
        super().__init__()
        self._data = data
    def data(self, index, role):
        if role == Qt.DisplayRole:
            value = self._data.iloc[index.row(), index.column()]
            return str(value)
    def rowCount(self, index):
        return self._data.shape[0]
    def columnCount(self, index):
        return self._data.shape[1]
    def headerData(self, section, orientation, role):
        if role == Qt.DisplayRole:
            if orientation == Qt.Horizontal:
                return str(self._data.columns[section])
        if orientation == Qt.Vertical:
            return str(self._data.index[section])



class comp_win(QMainWindow):
    comp_signal=Signal()
    def __init__(self,parent=None):
        super(comp_win,self).__init__(parent)
        # Load the UI definition from the file
        # Dynamically create a corresponding window object from the UI definition
        self.ui = QUiLoader().load('GUI/ui_files/Components_Window.ui')
        self.ui.setWindowIcon(QIcon('GUI/imag/RWTH_Logo.png'))
        self.ui.setWindowTitle('Scenario Components Database')
        self.ui.resize(1200, 800)
        self.set_sector()

        self.plant_init(self.ui.scenario_elec,self.ui.elec_equipment,self.ui.elec_minsize,self.ui.elec_maxsize,self.ui.elec_currentsize)
        self.plant_init(self.ui.scenario_heat,self.ui.heat_equipment,self.ui.heat_minsize,self.ui.heat_maxsize,self.ui.heat_currentsize)
        self.plant_init(self.ui.scenario_gas,self.ui.gas_equipment,self.ui.gas_minsize,self.ui.gas_maxsize,self.ui.gas_currentsize)
        self.plant_init(self.ui.scenario_cool, self.ui.cool_equipment, self.ui.cool_minsize, self.ui.cool_maxsize,self.ui.cool_currentsize)
        self.plant_init(self.ui.scenario_hydrogen,self.ui.hydrogen_equipment,self.ui.hydrogen_minsize,self.ui.hydrogen_maxsize,self.ui.hydrogen_currentsize)



    def plant_init(self,scenario,equipment,minsize,maxsize,currentsize):
        scenario.clear()
        scenario_list = self.get_scenario_name()
        scenario.addItems(['scenario'])
        scenario.addItems(scenario_list)
        scenario.currentTextChanged.connect(lambda :self.scenario_changed(scenario,equipment,minsize,maxsize,currentsize))
        # self.ui.elec_equipment.currentTextChanged.connect(self.scenario_changed)

    def get_current_tabWidget(self):
        currentTab = self.ui.tabs.currentWidget()
        return currentTab.objectName()
    def get_sector_matrix(self):
        sector = self.get_current_tabWidget()
        if sector=='Electricity':
            return 'elec'
        if sector=='Heat':
            return 'therm'
        if sector=='Gas':
            return 'gas'
        if sector=='Cool':
            return 'cool'
        if sector=='Hydrogen':
            return 'hydro'


    def scenario_changed(self,scenario,select_equipment,minsize,maxsize,currentsize):

        equipment = select_equipment.currentText()
        if equipment=='':
            return ''
        else:
            scenario_name=scenario.currentText()

            sector=self.get_sector_matrix()
            path=os.getcwd()+'/input_files/'+scenario_name+'/'+sector+'_matrix.csv'
            try:
                df=pd.read_csv(path)
            except Exception as e:
                # print(e)
                size = ['no data','no data','no data']
            else:
                try:
                    pf_equip_index=df['comp_type'].tolist().index(equipment)
                    sizename=['min_size', 'max_size', 'current_size']
                    size=[]
                    for i in sizename:
                        size.append(df.loc[pf_equip_index, i])
                except Exception as e:
                    # print(e)
                    size = ['no data', 'no data', 'no data']
            minsize.setText(str(size[0]))
            maxsize.setText(str(size[1]))
            currentsize.setText(str(size[2]))



    def set_sector(self):
        sector=['ELECTRICAL','HEAT','COOL','GAS','HYDROGEN']
        # Electricity
        self.insert_para('ELECTRICAL',self.ui.elec_equipment,self.ui.elec_model,self.ui.elec_gv,self.ui.elec_table)
        self.insert_para('HEAT', self.ui.heat_equipment, self.ui.heat_model,self.ui.heat_gv, self.ui.heat_table)
        self.insert_para('COOL', self.ui.cool_equipment, self.ui.cool_model,self.ui.cool_gv, self.ui.cool_table)
        self.insert_para('GAS', self.ui.gas_equipment, self.ui.gas_model,self.ui.gas_gv, self.ui.gas_table)
        self.insert_para('HYDROGEN', self.ui.hydrogen_equipment, self.ui.hydrogen_model,self.ui.hydrogen_gv, self.ui.hydrogen_table)
    def insert_para(self,sector,equipment,model,image,table):

        # sector='ELECTRICAL'
        equipment= self.component_type(sector)
        equipment.currentIndexChanged.connect(lambda: self.component_model(equipment,model,image))
        model.currentIndexChanged.connect(lambda: self.datatable(equipment,model,table))
        #
        # sector = 'HEAT'
        # self.ui.heat_equipment = self.component_type(sector)
        # self.ui.heat_equipment.currentIndexChanged.connect(self.heat_component_model)
        # self.ui.heat_model.currentIndexChanged.connect(self.heat_datatable)

    def component_type(self,sector):
        # self.ui.elec_equipment.clear()
        choose_component_sector = sector.lower()+'_components'
        filePath = 'Model_library/Component/model/'+choose_component_sector

        component_type_order= os.listdir(filePath)

        # print(component_type_order)
        component_type_order.remove('__pycache__')
        component_type_order.remove('__init__.py')
        component_type = []
        if '.py' in component_type_order[0]:
            component_type_order = component_type_order[0].split('.py')[0]
            self.add_equipment(sector,component_type_order=component_type_order)
        else:
            for i in component_type_order:
                filePath_order =filePath+'/'+ i
                component_type_item=os.listdir(filePath_order)
                component_type_item.remove('__init__.py')
                if '__pycache__' in component_type_item:
                    component_type_item.remove('__pycache__')

                if '.py' not in component_type_item[0]:
                    filePath_order=filePath_order+'/'+component_type_item[0]
                    component_type_item_2 = os.listdir(filePath_order)
                    component_type_item_2.remove('__init__.py')
                    component_type_item_2.remove('__pycache__')
                    for i in component_type_item_2:
                        i=i.split('.py')[0]
                        component_type.append(i)
                else:
                    for i in component_type_item:
                        i=i.split('.py')[0]
                        component_type.append(i)
            self.add_equipment(sector, component_type=component_type)
        if sector=='ELECTRICAL':
            return self.ui.elec_equipment
        elif sector == 'HEAT':
            return self.ui.heat_equipment
        elif sector=='COOL':
            return self.ui.cool_equipment
        elif sector=='GAS':
            return self.ui.gas_equipment
        elif sector == 'HYDROGEN':
            return self.ui.hydrogen_equipment


    def add_equipment(self,sector,component_type_order=None,component_type=None):
        if sector=='ELECTRICAL':
            self.ui.elec_equipment.addItems([component_type_order])
            self.ui.elec_equipment.addItems(component_type)
        elif sector == 'HEAT':
            self.ui.heat_equipment.addItems([component_type_order])
            self.ui.heat_equipment.addItems(component_type)
        elif sector=='COOL':
            self.ui.cool_equipment.addItems([component_type_order])
            self.ui.cool_equipment.addItems(component_type)
        elif sector == 'GAS':
            self.ui.gas_equipment.addItems([component_type_order])
            self.ui.gas_equipment.addItems(component_type)

    def component_model(self,equipment,model,image):
        self.comp_image_show(equipment,image)
        choose_component_type = equipment.currentText()
        model.clear()
        try:
            filePath = './Model_library/Component/data/'+choose_component_type
            component_model_order = os.listdir(filePath)
        except :
            model.addItems(["None"])
        else:
            component_model=[]
            for i in component_model_order:
                i = i.split('.csv')[0]
                component_model.append(i)
            model.addItems(component_model)

    def comp_image_show(self,equipment,image):
        choose_component_type = equipment.currentText()
        try:
            picture=icon.get_icons()
            image_path=picture[choose_component_type]
            img = cv2.imread(image_path)# Read images
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            x = img.shape[1]  # get image size
            y = img.shape[0]
            self.zoomscale = 1  # Image Scaling
            frame = QImage(img, x, y, QImage.Format_RGB888)
            pix = QPixmap.fromImage(frame)
            self.item = QGraphicsPixmapItem(pix)
            self.scene = QGraphicsScene()  # Create  scene
            self.scene.addItem(self.item)
            image.setScene(self.scene)
            image.show()
        except Exception as exc:
            print('There are no the image of %s '% (exc))

    def datatable(self,equipment,model,table):
        choose_component_type = equipment.currentText()
        choose_component_model = model.currentText()
        path='Model_library/Component/data/'+choose_component_type+'/'+choose_component_model+'.csv'
        try:
            csv_data=pd.read_csv(path)
            data=pd.DataFrame(csv_data)

            self.datamodel=TableModel(data)
            table.setModel(self.datamodel)
            table.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
            # table.verticalHeader().setSectionResizeMode(QHeaderView.Stretch)

        except Exception as exc:
            # print('There are no data of ' + choose_component_type)
            # print('This problem is : %s' % (exc))
            table.clearSpans()
            data = pd.DataFrame()
            self.datamodel = TableModel(data)
            table.setModel(self.datamodel)

    def get_scenario_name(self):
        scenario_list = []
        filePath = './input_files/'
        file_list = os.listdir(filePath)
        for i in file_list:
            if '.' not in i:
                scenario_list.append(i)
        return scenario_list


if __name__ == '__main__':
    app = QApplication([])
    app.setStyle('Fusion')
    SW.comp_win = comp_win()
    SW.comp_win.ui.show()
    sys.exit(app.exec_())