import sqlite3
from pprint import pprint

from PySide2.QtSql import QSqlDatabase, QSqlQuery
import pandas as pd
from sqlalchemy import create_engine
import GUI.cloud_database.scripts_db as db
import os
os.chdir('/project/prosumer_modeling')
path=os.getcwd()
sql_db_path=path+'//GUI//windows//windows_DB.sqlite'
# db.create_database(sql_path)
win_db = QSqlDatabase("QSQLITE")
win_db.setDatabaseName(sql_db_path)
win_db.open()
if win_db.open():
    print('Database opened successfully')
else:
    print(win_db.lastError().text())

# initialization table
def initialization_parameters():
    pass


def scenario_win_parameter(para,tbname="scenario_parameters"):

    db.create_SQLtable(tbname, db=win_db)
    df = pd.DataFrame(list(para.items()),columns = ['parameter','value'])
    db.df_insert_tb(df, tbname,db=win_db)

def get_scenario_win_parameter():
    para=db.get_SQLtable_column('scenario_parameters', 'value',databank=win_db)
    return para
# para={'data_source': 2, 'scenario_name': 'heatpump_household_AC', 'prosumer_name': 'household', 'prosumer_type': 'Single', 't_start': '2019-07-25 00:00:00', 't_horizon': '720', 't_step': '1'}
# scenario_win_parameter(para)
# para=get_scenario_win_parameter()
# print(para)




# strategies table
def strategies_table(list,sql_path=sql_db_path,tbname='strategies_parameters'):
    strategies={'strategies':list}
    df = pd.DataFrame(strategies)
    engine = create_engine('sqlite:///' + sql_path, echo=False)
    df.to_sql(tbname, engine, if_exists="replace")

def strategies_choose(colname='strategies',sql_path=sql_db_path,tbname='strategies_parameters'):
    conn = sqlite3.connect(sql_path)
    cursor = conn.cursor()
    sql = ''' select {} from {}'''.format(colname, tbname)
    results = cursor.execute(sql)
    col_value = results.fetchall()
    strategies = []
    for i in col_value:
        strategies.append(list(i)[0])
    return strategies

# Configuration and Scenario Table
add_config = {'injection_price': 0.0793,
              'gas_price': 0.0606,  # 2019 https://www.verivox.de/gas/verbraucherpreisindex/
              'elec_price': 0.3189,  # Januar 2021 https://www.vergleich.de/strompreise.html
              'heat_price': 0,
              'cooling_price': 0,
              'injection_price_gas': 0,
              'injection_price_heat': 0,
              'injection_price_cooling': 0,
              'elec_emission': 0.401,  # 2019
              'gas_emission': 0.21,
              'yearly_interest': 0.03,  # letmathe
              'planning_horizon': 20
                }
                         # 2019 # changes with size!
                         # bis10kwp: 8.16 (01.01.21), bis 40: 7.93
                         # (https://www.solaranlagen-portal.com/photovoltaik/wirtschaftlichkeit/einspeiseverguetung)
                         # 'injection/pvpeak': 0.7,


def add_df_to_sql(add_config,sql_path=sql_db_path,tbname='config_parameters'):
    '''
    add Configuration parameter as table in database
    datatype: dict -->dataframe to sql table
    '''
    df = pd.DataFrame.from_dict(add_config , orient='index',columns=['value'])
    df = df.reset_index().rename(columns = {'index':'parameter_name'})
    #write the pandas dataframe to a sqlite table
    engine = create_engine('sqlite:///'+sql_path, echo=False)
    df.to_sql(tbname,engine,if_exists="replace")


def get_all_config_parameter(sql_path=sql_db_path):
    '''
    add all scenario config parameters in windows database
    '''

    tbname='config_parameters'
    config_path=path+'/prosumer_library/database/'
    config_file=os.listdir(config_path)
    db.delete_SQLtable([tbname],sqlpath=sql_path)
    db.create_SQLtable(tbname, win_db)
    data={}
    tab_header=[]
    for i in config_file:
        config_data=pd.read_csv(config_path+i,)
        header = list(config_data)
        for h in header:
            if h not in tab_header:
                tab_header.append(i)
                db.add_SQLtable_cloumn('config_parameters', h, 'text', win_db)
        rowid= config_file.index(i)+1
        db.add_SQLtable_row(tbname, rowid, win_db)
        for index, value in config_data.iteritems():
            db.set_SQLtable_value(tbname, index, rowid, value.values[0], win_db)
get_all_config_parameter()

def get_config_nr(config_name):
    config_id=int
    if config_name=='Office':
        config_id=0
    else:
        print('there are no other configuration for scenario')
        config=None
    return config_id

def update_config_parameter(config_para,config_name):
    tbname = 'config_parameters'
    config_id=get_config_nr(config_name)+1
    df = pd.DataFrame.from_dict(config_para,orient='index',columns=[config_id] )
    df=(df.T)
    for index, value in df.iteritems():
        db.set_SQLtable_value(tbname, index, config_id, value.values[0], win_db)
# update_config_parameter(add_config,'Office')


def get_tb(config_name,tbname='config_parameters',sql_path=sql_db_path):
    '''
    read config table from sql database
    sql table -->dataframe-->dict
    '''
    with sqlite3.connect(sql_path) as con:
        c = con.cursor()
        tb = pd.read_sql("SELECT * FROM {}".format(tbname), con=con)
        key = list(tb)
        config_id=get_config_nr(config_name)
        config = dict(zip(key, tb.iloc[config_id]))
    return config

# pprint(get_tb('Office'))