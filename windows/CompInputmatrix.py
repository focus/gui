import queue
import threading
from csv import reader
import pandas as pd
import time
from PySide2 import QtCore, QtWidgets, QtSql
from PySide2.QtCore import Qt, Signal
from PySide2.QtGui import QImage, QPixmap, QIcon
from PySide2.QtWidgets import QMainWindow, QApplication, QMessageBox, QComboBox, QGraphicsPixmapItem, QGraphicsScene, \
    QHeaderView, QAbstractItemView
from PySide2.QtUiTools import QUiLoader
import cv2
import os
import GUI.flowchart.icon as icon
os.chdir('/project/prosumer_modeling')
import sys
import qtmodern.windows

class TableModel(QtCore.QAbstractTableModel):
    def __init__(self, data,HEADERS):
        super(TableModel, self).__init__()
        self._data = data
        self.headers = HEADERS

    def data(self, index, role):
        if role == Qt.DisplayRole:
            # See below for the nested-list data structure.
            # .row() indexes into the outer list,
            # .column() indexes into the sub-list
            return self._data[index.row()][index.column()]

    def rowCount(self, index):
        # The length of the outer list.
        return len(self._data)

    def columnCount(self, index):
        # The following takes the first sub-list, and returns
        # the length (only works if all rows are an equal length)
        return len(self._data[0])
    def headerData(self, section, orientation, role=Qt.DisplayRole):
        if role!=Qt.DisplayRole:
            return None
        if orientation!=Qt.Vertical:
            return self.headers[section]
        return int(section+1)


class comp_Inputmatrix(QMainWindow):
    comp_signal=Signal()
    def __init__(self,parent=None):
        super(comp_Inputmatrix,self).__init__(parent)
        # Load the UI definition from the file
        # Dynamically create a corresponding window object from the UI definition
        self.ui = QUiLoader().load('GUI/ui_files/Component_Inputmatrix.ui')
        self.ui.setWindowIcon(QIcon('GUI/imag/RWTH_Logo.png'))
        self.init()
        self.ui.resize(2000, 800)
    def init(self):
        self.ui.setWindowTitle('Component Input Matrix')
        scenario_list = self.get_scenario_name()
        self.ui.cb_scenario.addItems(scenario_list)
        self.ui.cb_scenario.currentTextChanged.connect(self.sector_changed)

        # setting properties of Table vew
        self.ui.tb_matrix.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.ui.tb_matrix.setSelectionMode(QAbstractItemView.SingleSelection)
        self.ui.tb_matrix.setAlternatingRowColors(True)
        self.ui.tb_matrix.verticalHeader().setDefaultSectionSize(22)
        self.ui.tb_matrix.horizontalHeader().setDefaultSectionSize(60)


    def get_scenario_name(self):
        scenario_list = []
        filePath = './input_files/'
        file_list = os.listdir(filePath)
        for i in file_list:
            if '.' not in i:
                scenario_list.append(i)
        return scenario_list
    def sector_changed(self):
        self.ui.cb_sector.clear()
        sector_list = self.get_sector_name()
        self.ui.cb_sector.addItems(sector_list)
        self.ui.cb_sector.currentTextChanged.connect(self.matrix_display)

    def get_sector_name(self):
        sector_list = ['']
        select_scenario = self.ui.cb_scenario.currentText()
        sector_filePath = './input_files/'+select_scenario+'/'
        file_list = os.listdir(sector_filePath)
        for i in file_list:
                sector_list.append(i)
        return sector_list

    def matrix_display(self):
        select_scenario = self.ui.cb_scenario.currentText()
        select_matrix = self.ui.cb_sector.currentText()
        matrix_filePath = './input_files/' + select_scenario + '/'+select_matrix
        csv_data=pd.read_csv(matrix_filePath, low_memory=False)
        data=pd.DataFrame(csv_data)
        headers=list(data.columns)
        data=data.values.tolist()
        # with open(matrix_filePath, 'r') as csv_file:
        #     csv_reader = reader(csv_file, delimiter='\t')
        #     list_data = list(csv_reader)
        #     print(list_data)
        # headers=list(list_data[0])
        # data=list_data[1:]

        self.datamodel = TableModel(data, headers)
        self.ui.tb_matrix.setModel(self.datamodel)
        self.ui.tb_matrix.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)





if __name__ == '__main__':
    app = QApplication([])
    app.setStyle('Fusion')
    Inputmatrix_win = comp_Inputmatrix()
    Inputmatrix_win.ui.show()
    sys.exit(app.exec_())