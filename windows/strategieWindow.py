import queue
import threading

from PySide2 import QtWidgets
from PySide2.QtCore import Qt, Signal
from PySide2.QtWidgets import QApplication
from PySide2.QtUiTools import QUiLoader
import os
from GUI.windows.share import SW
import GUI.windows.window_db as window_db

import sys


class stra_win(QtWidgets.QWidget):
    stra_signal = Signal()
    def __init__(self):
        super(stra_win, self).__init__()
        # Load the UI definition from the file
        # Dynamically create a corresponding window object from the UI definition
        self.ui = QUiLoader().load('GUI/ui_files/Strategie.ui')

        self.ui.setWindowTitle("Strategies Window")
        self.ui.resize(600, 400)

        #TODO default: all strategies are choosed
        self.allstategy_choosed()
        self.ui.cb1.stateChanged.connect(self.not_allstategy_choosed)
        self.ui.cb2.stateChanged.connect(self.changecb2)
        self.ui.cb3.stateChanged.connect(self.changecb2)
        self.ui.cb4.stateChanged.connect(self.changecb2)
        self.ui.cb5.stateChanged.connect(self.changecb2)
        self.ui.btn_save.clicked.connect(self.save_stategy)
        self.ui.btn_cancel.clicked.connect(self.cancel_stategy)
        # self.add_stategy=self.get_stategy()

    def allstategy_choosed(self):
        # self.ui.cb1.setChecked(True)
        # self.ui.cb1.setCheckState(Qt.Checked)
        self.ui.cb1.setChecked(True)
        self.ui.cb2.setChecked(True)
        self.ui.cb3.setChecked(True)
        self.ui.cb4.setChecked(True)
        self.ui.cb5.setChecked(True)

    def not_allstategy_choosed(self):

        if self.ui.cb1.isChecked()==True:
            # self.ui.cb1.setCheckState(Qt.Checked)
            self.allstategy_choosed()
        else:
            self.ui.cb1.setCheckState(Qt.Unchecked)
            self.ui.cb2.setChecked(False)
            self.ui.cb3.setChecked(False)
            self.ui.cb4.setChecked(False)
            self.ui.cb5.setChecked(False)

    def changecb2(self):

        if self.ui.cb2.isChecked() or self.ui.cb3.isChecked() or self.ui.cb4.isChecked() or self.ui.cb5.isChecked():
            self.ui.cb1.setTristate(False)
            self.ui.cb1.setCheckState(Qt.Unchecked)
            # pass
        else:
            # pass
            self.ui.cb1.setTristate(False)
            self.ui.cb1.setCheckState(Qt.Unchecked)

    def get_stategy(self):
        if self.ui.cb1.isChecked()==False:
            Add_stategy=[]
            if self.ui.cb2.isChecked()==True:
                Add_stategy.append('variable_operation_costs')
            if self.ui.cb3.isChecked()==True:
                Add_stategy.append('own_consumption')
            if self.ui.cb4.isChecked()==True:
                Add_stategy.append('annuity')
            if self.ui.cb5.isChecked()==True:
                Add_stategy.append('co2')
            return Add_stategy
        else:
            return ['variable_operation_costs', 'own_consumption', 'annuity', 'co2']

    def save_stategy(self):
        self.add_stategy=self.get_stategy()
        window_db.strategies_table(self.add_stategy)
        self.ui.close()

    def cancel_stategy(self):
        self.allstategy_choosed()
        self.add_stategy = self.get_stategy()
        # print(self.add_stategy)
        self.ui.close()

    def send_signal(self):
        self.stra_signal.emit()

    def stra_handle(self):
        while 1:
            info=self.stra_queue.get(1)
            if info=="show":
                self.send_signal()




if __name__ == '__main__':
    app = QApplication([])
    app.setStyle('Fusion')
    # qtmodern.styles.dark(app)
    # app.setWindowIcon(QIcon('images/RWTH_Logo.svg'))
    SW.str_win = stra_win()
    SW.str_win.ui.show()
    sys.exit(app.exec_())
