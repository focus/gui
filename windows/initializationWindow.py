import time

from PySide2 import QtCore,QtGui
from PySide2.QtGui import QIcon
from PySide2.QtWidgets import QMainWindow,QApplication, QMessageBox,QComboBox
from PySide2.QtUiTools import QUiLoader
from numpy import mean
import GUI.windows.window_db as window_db
# from share import SW
import sys
import qtmodern.styles
import qtmodern.windows
import os
# scenarios_path='..\input_files'
# scenarios_list=os.listdir(scenarios_path)
os.chdir('/project/prosumer_modeling')
# from prosumer_modeling.runme_db import Main
# print(os.getcwd())

class initi_win:
    def __init__(self) :

        # Dynamically create a corresponding window object from the UI definition
        self.ui = QUiLoader().load('GUI/ui_files/Initialization.ui')
        self.ui.setWindowTitle("Initialization Window")
        self.init()


        self.ui.btn_save.clicked.connect(self.save_scenario_parameters)
        self.ui.btn_cancel.clicked.connect(self.ui.close)

        self.ui.show()


    def init(self):
        '''
                   default values of scenario
                        data_source=2
                        input_files / 0['household']
                        2019 - 07 - 25
                        00: 00:00
                        720
                        1
                    '''
        self.ui.resize(800,600)
        # define the scenario and give names to prosumers
        self.ui.comBox.clear()
        scenario_list = self.get_scenario_name()
        self.ui.comBox.addItems(['heatpump_household_AC'])#test
        self.ui.comBox.addItems(scenario_list)

        # self.ui.comBox.addItems(scenarios_list)
        self.ui.prosumer_name.setText("household(DEFAULT)")
        # self.ui.prosumer_name.setPlaceholderText("Please give your initialize prosumer name")

        # Set the simulation time frame
        #Set a definite time: Date & Time
        self.ui.time_start.setDate(QtCore.QDate(2019, 7, 25))
        self.ui.time_start.setTime(QtCore.QTime(0, 0, 0))
        self.ui.time_start.setDisplayFormat("yyyy-MM-dd HH:mm:ss")
        self.ui.time_start.setCalendarPopup(True)
        # get current date and time
        # self.ui.time_start.setDate(QtCore.QDate.currentDate())
        # self.ui.time_start.setTime(QtCore.QTime.currentTime())

        # set horizon and step that ensures a string contains a valid integer
        self.ui.time_horizon.setValidator(QtGui.QIntValidator())
        self.ui.time_horizon.setText("720")  # time in [h]
        self.ui.time_step.setValidator(QtGui.QIntValidator())
        self.ui.time_step.setText("1")  # time step in [h]

    def get_scenario_name(self):
        scenario_list = []
        filePath = './input_files/'
        file_list = os.listdir(filePath)
        for i in file_list:
            if '.' not in i:
                scenario_list.append(i)
        return scenario_list

    def get_scenario_parameters(self):
        '''
        scenario_parameters update in database
        '''

        data_source=2
        scenario_name=self.ui.comBox.currentText()
        pro_name=self.ui.prosumer_name.text()
        pro_type = 'single prosumer'
        t_start=self.ui.time_start.text()
        t_horizon=self.ui.time_horizon.text()
        t_step=self.ui.time_step.text()

        scenario_para={'data_source':data_source,
                        'scenario_name':scenario_name,
                       'prosumer_name': pro_name,
                       'prosumer_type':pro_type,
                       't_start':t_start,
                       't_horizon':t_horizon,
                       't_step':t_step,
        }
        return scenario_para

    def save_scenario_parameters(self):
        scenario_parameters = self.get_scenario_parameters()
        window_db.scenario_win_parameter(scenario_parameters, tbname="scenario_parameters")
        self.ui.close()

    # def main_run(self):
    #     if self.control_Dialog()==False:
    #         self.ui.show()
    #         i=False
    #     else:
    #         # params=initi_win()
    #         data_source, scenario_path, prosumer_name, t_start, t_horizon, t_step=self.initialize_Parameter()
    #         main = Main(data_source, scenario_path, prosumer_name, t_start, t_horizon, t_step)
    #         return main

    # def get_profile(self):
    #     data_source, scenario_path, prosumer_name, t_start, t_horizon, t_step = self.initialize_Parameter()
    #     main = Main(data_source, scenario_path, prosumer_name, t_start, t_horizon, t_step)
    #     profile = main.scenario_get_profile(main.prosumer_name_list)
    #
    #     # max demand
    #     max_demand = {}
    #     for demand, value in profile.items():
    #         wert = value.values.tolist()
    #         max_value = None
    #         for num in wert:
    #             if (max_value is None or num > max_value):
    #                 max_value = num
    #         max_demand[demand] = max_value
    #     # print(max_demand)
    #     # mean demand
    #     mean_demand = {}
    #     for demand, value in profile.items():
    #         wert = value.values.tolist()
    #         mean_value = mean(wert)
    #         mean_demand[demand] = mean_value


        # {'elec_demand': 0.6774554047365016,
        # 'therm_demand': 0.28174220648907977,
        # 'hot_water_demand': 0.2054816438356393,
        # 'air_temperature': 36.191666666666684,
        # 'irradiance': 699.4661744187138}
        # return max_demand,mean_demand

    # check in whether initi_parameter in Scenario complete or not
    def control_Dialog(self):
        if len(self.ui.comBox.currentText())==0:
            msgBox = QMessageBox()
            msgBox.setIcon(QMessageBox.Information)
            msgBox.setWindowTitle("Tips")
            msgBox.setText("Please choose a scenario and setup the value of scenario ,thank you")
            msgBox.setStandardButtons(QMessageBox.Ok)
            # msgBox.buttonClicked.connect(self.msgButtonClick)

            returnValue = msgBox.exec()

            control_value=False
        else:
            control_value = True
        return control_value




if __name__ == '__main__':
    app = QApplication([])
    app.setStyle('Fusion')
    init_win = initi_win()
    sys.exit(app.exec_())





