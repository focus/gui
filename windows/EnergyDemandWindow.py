import subprocess

from PySide2.QtCore import Qt, Signal, QRunnable, QThreadPool, QObject
from PySide2.QtWidgets import QApplication, QMainWindow
from PySide2.QtUiTools import QUiLoader
import os
import sys


os.chdir('/project/prosumer_modeling')
class Signals(QObject):
    result=Signal(str)
    finished=Signal()

class Thread(QRunnable):
    """"
    Prosumer thread
    Inherits from QRunnable to handle prosumer thread setup, signals and wrap-up.

    :param args: Arguments to pass for the prosumer
    :param kwargs: Keywords to pass for the prosumer

    """
     # Create an instance of our signals class.
    def __init__(self,command):
        super().__init__()
        self.signals = Signals()
        self.command=command

    def run(self):
        try:

            output=subprocess.getoutput(self.command)
            self.signals.result.emit(output)
            self.signals.finished.emit()
        except Exception as e:
            print(e)
            # self.signals.error.emit(self.prosumer_name,str(e))
            # self.signals.status.emit(self.prosumer_name,STATUS_ERROR)
        else:
            pass
            # self.signals.result.emit(self.prosumer_name,result)
            # self.signals.status.emit(self.prosumer_name,STATUS_COMPLETE)

class energyDemand_win(QMainWindow):
    def __init__(self, parent=None):
        super(energyDemand_win, self).__init__(parent)
        # Load the UI definition from the file
        # Dynamically create a corresponding window object from the UI definition
        self.ui = QUiLoader().load('GUI/ui_files/EnergyDemand.ui')

        self.ui.setWindowTitle("Energy Demand Window")
        self.ui.resize(800, 300)

        self.default_profile()
        self.threadpool = QThreadPool()
        self.ui.btn_elec.clicked.connect(lambda: self.file_dialog(self.ui.btn_elec))
        self.ui.btn_heat.clicked.connect(lambda: self.file_dialog(self.ui.btn_heat))
        self.ui.btn_cool.clicked.connect(lambda: self.file_dialog(self.ui.btn_cool))
        self.ui.btn_hydrogy.clicked.connect(lambda: self.file_dialog(self.ui.btn_hydrogy))
        self.ui.in_elec.setText('GUI/swimming_pool/profile_database/Profile_elec.csv')
        self.ui.in_heat.setText('GUI/swimming_pool/profile_database/Profile_heat.csv')
        self.ui.in_cool.setText('')
        self.ui.in_hydrogy.setText('')


    def default_profile(self):
        self.ui.show()
        pass


    def file_dialog(self,btn):
        btn.setEnabled(False)
        self.thread = Thread("python GUI/windows/file_dialog.py")
        self.thread.signals.result.connect(lambda: self.thread_output(btn))
        self.thread.setAutoDelete(True)
        self.threadpool.start(self.thread)

    def thread_output(self,btn):
        btn.setEnabled(True)



if __name__ == '__main__':
    app = QApplication([])
    app.setStyle('Fusion')
    # app.setWindowIcon(QIcon('images/RWTH_Logo.svg'))
    energyDemand = energyDemand_win()
    sys.exit(app.exec_())
