import queue
import threading
from pprint import pprint

from PySide2 import QtWidgets
from PySide2.QtCore import Signal
from PySide2.QtWidgets import QApplication
from PySide2.QtUiTools import QUiLoader
from GUI.windows.share import SW
import sys
import os
import GUI.windows.window_db as window_db
os.chdir('/project/prosumer_modeling')


class opt_win(QtWidgets.QWidget):
    opt_signal = Signal()
    def __init__(self):
        super(opt_win, self).__init__()
        # Load the UI definition from the file
        # Dynamically create a corresponding window object from the UI definition
        self.ui = QUiLoader().load('GUI/ui_files/Optimization.ui')
        self.set_option()
        self.ui.setWindowTitle("Optimization Window")
        self.ui.resize(400, 400)
        self.ui.show()

        self.ui.btn_save.clicked.connect(self.save_opt)

    def get_stategy(self):
            Add_stategy=[]
            if self.ui.cb1.isChecked()==True:
                Add_stategy.append('variable_operation_costs')
            if self.ui.cb2.isChecked()==True:
                Add_stategy.append('own_consumption')
            if self.ui.cb3.isChecked()==True:
                Add_stategy.append('co2')
            if self.ui.cb4.isChecked()==True:
                Add_stategy.append('annuity')
                #['variable_operation_costs', 'own_consumption', 'annuity', 'co2']
            return Add_stategy

    def set_option(self):
        pass
    def get_option(self):
        if self.ui.rbt1.isChecked() == True:
            option='Heuristics'
            value=self.ui.value1.text()
            return option, value
        if self.ui.rbt2.isChecked() == True:
            option = 'MIPGap'
            value = self.ui.value2.text()
            return option, value
        if self.ui.rbt3.isChecked() == True:
            option = 'ImproveStartGap'
            value = self.ui.value3.text()
            return option, value
        if self.ui.rbt4.isChecked() == True:
            option ='MIPFocus'
            value = self.ui.value4.text()
            return option, value
        if self.ui.rbt5.isChecked() == True:
            option = 'Presolve'
            value = self.ui.value5.text()
            return option, value
        if self.ui.rbt6.isChecked() == True:
            option = 'NumericFocus'
            value = self.ui.value6.text()
            return option,value

    def save_opt(self):
        options_para=self.save_opimization()
        window_db.scenario_win_parameter(options_para,tbname='optimization_parameters')
        self.ui.close()

    def save_opimization(self):
        solver=self.ui.cbox.currentText()
        strategies=self.get_stategy()
        option,value=self.get_option()

        return { 'solver':solver,
                 'strategies':strategies,
                 'option':option,
                 'value':value
        }

if __name__ == '__main__':
    app = QApplication([])
    app.setStyle('Fusion')
    SW.opt_win = opt_win()
    result=SW.opt_win.save_opimization()
    pprint(result)
    sys.exit(app.exec_())