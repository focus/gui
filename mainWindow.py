# import re
import ast
import subprocess

import pandas as pd
from PySide2.QtWidgets import (QMainWindow, QApplication, QTreeWidgetItem, QStyledItemDelegate)
from PySide2.QtUiTools import QUiLoader
from PySide2 import QtCore, QtGui, QtWidgets
from PySide2.QtCore import Signal, Slot, QObject, QMutex, QRunnable, QThreadPool, QProcess, QAbstractListModel, QTimer, \
    QRect, QDateTime, QThread, QEventLoop
from PySide2.QtGui import QIcon, QPixmap, Qt, QColor, QBrush, QPen
from sqlalchemy import create_engine

from GUI.windows.file_dialog import open_file_Dialog


import GUI.imag.resource
import GUI.imag.Icon
import qtmodern.windows
import sys
from GUI.compDatabase import comp_db
import GUI.windows.window_db as window_db
import os
os.chdir('/project/prosumer_modeling')
path=os.getcwd()
sql_db_path=path+'//GUI//windows//windows_DB.sqlite'

STATUS_WAITING="waiting"
STATUS_RUNNING = "running..."
STATUS_ERROR = "error"
STATUS_COMPLETE = "finished"

STATUS_COLORS = {
    STATUS_RUNNING: "#33a02c",
    STATUS_ERROR: "#e31a1c",
    STATUS_COMPLETE: "#b2df8a",
}

DEFAULT_STATE = {"progress": 0, "status": STATUS_WAITING}


class Signals(QObject):
    '''
    defines the signals available from a running prosumer thread
    Supported signals are:

    finished
            No data
    error
            `tuple` (exctype, value, traceback.format_exc() )
    result
            `object` data returned from processing, anything
    progress
            `int` indicating % progress
    '''
    error=Signal(str,str)
    # result=Signal(str,object)
    result=Signal(str)
    finished=Signal()
    # finished=Signal(str)
    progress = Signal(str, int)
    status = Signal(str,str)

qmut1 = QMutex()
class Thread(QThread):

    def __init__(self):
        super(Thread,self).__init__()
        self.signals = Signals()

    def run(self):
        window_db.scenario_win_parameter(self.scenario_para)
        # window_db.add_df_to_sql(self.scenario_para, tbname='scenario_parameters')
        self.signals.result.emit('finished')

    def transfer(self,scenario_para):
        self.scenario_para=scenario_para


class Windows_Thread(QRunnable):
    """"
    Window thread
    Inherits from QRunnable to handle prosumer thread setup, signals and wrap-up.

    :param args: Arguments to pass for the prosumer
    :param kwargs: Keywords to pass for the prosumer

    """
     # Create an instance of our signals class.
    def __init__(self,command):
        super().__init__()
        self.signals = Signals()
        self.command=command

    @Slot()
    def run(self):
        try:
            output=subprocess.getoutput(self.command)
            self.signals.result.emit(output)
            self.signals.finished.emit()
        except Exception as e:
            # self.signals.error.emit(self.prosumer_name,str(e))
            # self.signals.status.emit(self.prosumer_name,STATUS_ERROR)
            print(e)
        else:
            pass

class Prosumer(QRunnable):
    """"
    Prosumer thread
    Inherits from QRunnable to handle prosumer thread setup, signals and wrap-up.

    :param args: Arguments to pass for the prosumer
    :param kwargs: Keywords to pass for the prosumer

    """
     # Create an instance of our signals class.
    def __init__(self,command):
        super().__init__()
        self.signals = Signals()
        # self.prosumer_name=args[0]
        self.command=command
        # self.signals.status.emit(self.prosumer_name,STATUS_WAITING)

    @Slot()
    def run(self):
        # qmut1.lock()
        # self.signals.status.emit(self.prosumer_name,STATUS_RUNNING)
        try:

            output=subprocess.getoutput(self.command)
            self.signals.result.emit(output)
            self.signals.finished.emit()
        except Exception as e:
            print(e)
            # self.signals.error.emit(self.prosumer_name,str(e))
            # self.signals.status.emit(self.prosumer_name,STATUS_ERROR)
        else:
            pass
            # self.signals.result.emit(self.prosumer_name,result)
            # self.signals.status.emit(self.prosumer_name,STATUS_COMPLETE)

        # self.signals.finished.emit(self.prosumer_name)
        # qmut1.unlock()

class EmittingStr(QtCore.QObject):
        textWritten = QtCore.Signal(str)

        def write(self, text):
            self.textWritten.emit(str(text))
            loop = QEventLoop()
            QTimer.singleShot(1000, loop.quit)
            loop.exec_()

class mainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        # Dynamically create a corresponding window object from the UI definition
        self.ui = QUiLoader().load('GUI/ui_files/MainWindow.ui')
        # static Load the UI definition from the file
        # self.ui=Ui_Prosumer_Modeling()
        # self.ui.setupUi(self)
        #todo: design UI Main Window
        self.setup_ui()
        # test Window in multiple threading
        self.timer=QTimer()
        self.timer.timeout.connect(self.showtime)
        self.timer.start(1000)

        self.threadpool = QThreadPool()
        print(
            "Multithreading with maximum %d threads" % self.
            threadpool.maxThreadCount()
        )
        #Button Action
        # self.ui.btn_save.clicked.connect(self.scenario_parameter)
        self.ui.actionStart.triggered.connect(self.run_Scenario)
        # self.ui.actionStart.triggered.connect(self.test)

        self.ui.btn_clear.clicked.connect(self.clearButton) # clear result and graphic View

        # self.prosumers=prosumerManager()
        # self.prosumers.status.connect(self.statusBar().showMessage)
        # self.ui.progress.setModel(self.prosumers)
        # delegate=ProgressBarDelegate()
        # self.ui.progress.setItemDelegate(delegate)

        #Todo every window as thread
        self.ui.btn_Configuration.clicked.connect(self.configuration_win_open)
        self.ui.btn_energydemand.clicked.connect(self.energydemand_win_open)
        self.ui.btn_Strategie.clicked.connect(self.strategie_win_open)
        self.ui.btn_Components.clicked.connect(self.components_win_open)
        # self.ui.btn_Profile.clicked.connect(self.profile_win_open)
        self.ui.btn_Optimization.clicked.connect(self.optimization_win_open)
        self.ui.actiondatabase.triggered.connect(self.comp_database)
        self.ui.btn_output.clicked.connect(self.result_open)
        self.ui.btn_inputmatrix.clicked.connect(self.comp_Inputmatrix)



    # Initialize the necessary widgets
    def setup_ui(self):
        self.ui.setWindowIcon(QIcon('GUI/imag/RWTH_Logo.png'))
        self.ui.setWindowTitle('Digital Twin of Urban Energy System')
        # choose a data bank from three data bank
        '''
        default db: it is currently seen as a local database
        local data bank: waiting for developing: local file upload-->use 
        cloud data bank: open source platform： already download but currently not to be use 
        '''
        self.ui.rbtn_default_db.setChecked(True)

        self.ui.rbtn_local_db.toggled.connect(self.file_dialog)
        self.ui.rbtn_cloud_db.toggled.connect(self.cloud_db)
        if self.ui.rbtn_default_db.isChecked()==True:
            self.data_source=2

        # default db
        # # self.ui.textEdit.setReadOnly(True)
        # self.ui.combox_scenario.clear()
        # scenario_list=self.get_scenario_name()
        # scenario_list=['heatpump_household_AC']  # only for test
        # self.ui.combox_scenario.addItems(scenario_list)
        # self.ui.prosumer_name.setPlaceholderText("Please give your prosumer name")
        #
        # # simulation time set up
        # self.ui.t_start.setDate(QtCore.QDate(2019, 7, 25))
        # self.ui.t_start.setTime(QtCore.QTime(0, 0, 0))
        # self.ui.t_start.setDisplayFormat("yyyy-MM-dd HH:mm:ss")
        # self.ui.t_start.setCalendarPopup(True)
        # # self.ui.t_horizon.setValidator(QtGui.QIntValidator())
        # # self.ui.t_step.setValidator(QtGui.QIntValidator())
        # self.ui.t_horizon.setPlaceholderText("Please give the interval of in integer in hour ")
        # self.ui.t_step.setPlaceholderText("Please give the step of time in integer in hour")
        #
        # self.ui.prosumer_name.setText("household")
        #
        # self.ui.t_horizon.setValidator(QtGui.QIntValidator())
        # self.ui.t_horizon.setText("720")  # time in [h]
        # self.ui.t_step.setValidator(QtGui.QIntValidator())
        # self.ui.t_step.setText("1")  # time step in [h]

        # self.init_treeView()
        self.ui.treeView.setItemsExpandable(False)
        self.ui.treeView.expandAll()

        # self.ui.graphicview.setStyleSheet("background:blue")
        self.ui.status.setText("Status: {}".format(STATUS_WAITING))

        self.ui.showMaximized()
        # sys.stdout = EmittingStr(textWritten=self.scenario_output)
        # sys.stderr = EmittingStr(textWritten=self.scenario_output)


        self.ui.show()

    def showtime(self):
        time = QDateTime.currentDateTime()
        timedisplay = time.toString("yyyy-MM-dd hh:mm:ss ")
        self.ui.time.setText(timedisplay)

    def get_scenario_parameters(self):
        '''
        scenario_parameters update in database
        '''
        data_source=self.data_source
        scenario_name=self.ui.combox_scenario.currentText()
        pro_name=self.ui.prosumer_name.text()
        pro_type = self.ui.prosumer_type.currentText()
        t_start=self.ui.t_start.text()
        t_horizon=self.ui.t_horizon.text()
        t_step=self.ui.t_step.text()

        scenario_para={'data_source':data_source,
                        'scenario_name':scenario_name,
                       'prosumer_name': pro_name,
                       'prosumer_type':pro_type,
                       't_start':t_start,
                       't_horizon':t_horizon,
                       't_step':t_step,
        }
        return scenario_para
        # self.scenario_parameters = self.get_scenario_parameters()
        # window_db.add_df_to_sql(scenario_para, tbname='scenario_parameters')
        # QApplication.processEvents()


    def get_scenario_name(self):
        scenario_list = []
        filePath = './input_files/'
        file_list = os.listdir(filePath)
        for i in file_list:
            if '.' not in i:
                scenario_list.append(i)
        return scenario_list


    def scenario_parameter(self):
        '''
        update scenario parameter to transport runme db
        '''

        scenario_parameters = self.get_scenario_parameters()

        self.ui.btn_save.setEnabled(False)
        self.prosumer_para_thread = Thread()
        self.prosumer_para_thread.transfer(scenario_parameters)
        self.prosumer_para_thread.signals.result.connect(self.scenario_para)
        self.prosumer_para_thread.start()
        # self.ui.btn_save.setEnabled(True)

        # window_db.add_df_to_sql(self.scenario_parameters, tbname='scenario_parameters')
    def scenario_para(self,s):
        print(s)
        self.prosumer_para_thread.terminate()
        self.ui.btn_save.setEnabled(True)

    # Running external commands & processes
    def run_Scenario(self):
        self.ui.actionStart.setEnabled(False)
        self.prosumer_thread = Prosumer("python runme_db.py")
        self.prosumer_thread.signals.result.connect(self.scenario_output)
        self.prosumer_thread.setAutoDelete(True)
        # self.threadpool.start(flowchart_thread)
        self.threadpool.start(self.prosumer_thread)

    def comp_database(self):
        self.ui.actiondatabase.setEnabled(False)
        self.comp_database_thread = Windows_Thread("python GUI/compDatabase.py")
        self.comp_database_thread.signals.result.connect(self.comp_database_output)
        self.comp_database_thread.setAutoDelete(True)
        self.threadpool.start(self.comp_database_thread)

    def comp_database_output(self):
        self.ui.actiondatabase.setEnabled(True)

    def get_image_path(self):
        scenario_para=window_db.get_scenario_win_parameter()
        scenario_name=scenario_para[1]
        diagram_save_path = os.getcwd() + '\\GUI\\flowchart\\pictures\\flowchart_' + scenario_name
        image_path = diagram_save_path + '.png'
        return image_path

    def Imageadd(self):
        image_path=self.get_image_path()
        if os.path.isfile(image_path):
            self.scene = QtWidgets.QGraphicsScene(self.ui.graphicview)
            pixmap = QPixmap(image_path)
            item = QtWidgets.QGraphicsPixmapItem(pixmap)
            self.scene.addItem(item)
            self.ui.graphicview.setScene(self.scene)

    def Imageremove(self):
        # self.Imageadd()
        image_path = self.get_image_path()
        if os.path.isfile(image_path):
            scene = QtWidgets.QGraphicsScene(self.ui.graphicview)
            pixmap = QPixmap(image_path)
            item = QtWidgets.QGraphicsPixmapItem(pixmap)
            scene.addItem(item)
            self.scene.removeItem(item)
            self.ui.graphicview.setScene(scene)

    def scenario_output(self,s):
        # self.ui.textEdit.clear()
        # scenario = self.ui.combox_scenario.currentText()
        # flowchart = Figure_Canvas(scenario)
        self.Imageadd()
        self.ui.actionStart.setEnabled(False)
        cursor = self.ui.textEdit.textCursor()
        cursor.movePosition(QtGui.QTextCursor.End)
        cursor.insertText("thread complete\n")

        # cursor.insertText(s)
        cursor.insertText('\n')
        self.ui.textEdit.setTextCursor(cursor)
        self.ui.textEdit.ensureCursorVisible()
        self.ui.actionStart.setEnabled(True)
        self.ui.status.setText("Status: {}".format(STATUS_COMPLETE))
        self.ui.actionStart.setEnabled(True)

    def init_treeView(self):
        # Set the number of columns
        self.ui.treeview.setColumnCount(1)
        # Set the title of the tree control header
        self.ui.treeview.setHeaderLabels([ 'Files name'])

        # Setting the root node
        root=[]
        root_namelist = ['Scenario', 'Components lib', 'Tools', 'Input Files', 'Result']
        for i in range(len(root_namelist)):
            root.append(QTreeWidgetItem(self.ui.treeview))
            root[i].setText(0, root_namelist[i])

        # Set child node 1
        child1 = QTreeWidgetItem(root[0])
        child1.setText(0, 'Prosumer')
        child1.setText(1, 'component')
        # Set child node 2
        child2 = QTreeWidgetItem(root[0])
        child2.setText(0, 'configuration')
        child2.setText(1, 'data')

        # Set child node 3
        child3 = QTreeWidgetItem(child2)
        child3.setText(0, 'xxx')
        child3.setText(1, 'xxxxx')

        #Load all properties of the root node with child controls
        for i in root:
            self.ui.treeview.addTopLevelItem(i)

        self.ui.treeview.clicked.connect(self.onClicked)
    #
        # Nodes all expanded
        self.ui.treeview.expandAll()

    def onClicked(self, qmodeLindex):
        item = self.ui.treeview.currentItem()
        print('Key=%s,value=%s' % (item.text(0), item.text(1)))

    # clear image and textedit zone
    def clearButton(self):
        self.ui.textEdit.clear()
        self.Imageremove()

    def configuration_win_open(self):
        self.ui.btn_Configuration.setEnabled(False)
        self.config_thread = Windows_Thread("python GUI/windows/configurationWindow.py")
        self.config_thread.signals.result.connect(self.conf_output)
        self.config_thread.setAutoDelete(True)
        self.threadpool.start(self.config_thread)
    def conf_output(self):
        self.ui.btn_Configuration.setEnabled(True)

    def strategie_win_open(self):
        self.ui.btn_Strategie.setEnabled(False)
        self.stra_thread = Windows_Thread("python GUI/windows/initializationWindow.py")
        self.stra_thread .signals.result.connect(self.stra_output)
        self.stra_thread.setAutoDelete(True)
        self.threadpool.start(self.stra_thread)
    def stra_output(self):
        self.ui.btn_Strategie.setEnabled(True)

    def comp_Inputmatrix(self):
        self.ui.btn_inputmatrix.setEnabled(False)
        self.inputmatrix_thread = Windows_Thread("python GUI/windows/CompInputmatrix.py")
        self.inputmatrix_thread.signals.result.connect(self.Inputmatrix_output)
        self.inputmatrix_thread.setAutoDelete(True)
        self.threadpool.start(self.inputmatrix_thread)

    def Inputmatrix_output(self):
        self.ui.btn_inputmatrix.setEnabled(True)

    def components_win_open(self):
        self.ui.btn_Components.setEnabled(False)
        self.comp_thread = Windows_Thread("python GUI/windows/componenteWindow.py")
        self.comp_thread.signals.result.connect(self.comp_output)
        self.comp_thread.setAutoDelete(True)
        self.threadpool.start(self.comp_thread)
    def comp_output(self):
        self.ui.btn_Components.setEnabled(True)

    def profile_win_open(self):
        self.ui.btn_Profile.setEnabled(False)
        self.profile_thread = Windows_Thread("python GUI/windows/profileWindow.py")
        self.profile_thread.signals.result.connect(self.prof_output)
        self.profile_thread.setAutoDelete(True)
        self.threadpool.start(self.profile_thread)
    def prof_output(self):
        self.ui.btn_Profile.setEnabled(True)

    def energydemand_win_open(self):
        self.ui.btn_energydemand.setEnabled(False)
        self.energydemand_thread = Windows_Thread("python GUI/windows/EnergyDemandWindow.py")
        self.energydemand_thread.signals.result.connect(self.energydemand_output)
        self.energydemand_thread.setAutoDelete(True)
        self.threadpool.start(self.energydemand_thread)
    def energydemand_output(self):
        self.ui.btn_energydemand.setEnabled(True)

    def optimization_win_open(self):
        self.ui.btn_Optimization.setEnabled(False)
        self.opt_thread = Windows_Thread("python GUI/windows/optimisationWindow.py")
        self.opt_thread.signals.result.connect(self.opt_output)
        self.opt_thread.setAutoDelete(True)
        self.threadpool.start(self.opt_thread)
    def opt_output(self):
        self.ui.btn_Optimization.setEnabled(True)

    def file_dialog(self):
        if self.ui.rbtn_default_db.isChecked() == False and \
                self.ui.rbtn_cloud_db.isChecked() == False:
            self.open_file_thread = Windows_Thread("python GUI/windows/file_dialog.py")
            self.open_file_thread.setAutoDelete(True)
            self.threadpool.start(self.open_file_thread)

    def cloud_db(self):
        if self.ui.rbtn_cloud_db.isChecked() == True:
            self.cloud_db_thread = Windows_Thread("python GUI/cloud_database/ui_database.py")
            self.cloud_db_thread.setAutoDelete(True)
            self.threadpool.start(self.cloud_db_thread)

    def result_open(self):
        # result_file_path=os.getcwd()+'/output_files/results_0.xlsx'
        # print(result_file_path)
        # if os.path.exists(result_file_path):
        #     os.startfile(result_file_path)
        self.ui.btn_output.setEnabled(False)
        self.eva_thread = Windows_Thread("python GUI/analyse/Evaluation.py")
        self.eva_thread.signals.result.connect(self.eva_output)
        self.eva_thread.setAutoDelete(True)
        self.threadpool.start(self.eva_thread)

    def eva_output(self):
        self.ui.btn_output.setEnabled(True)


if __name__ == '__main__':
    # app = QApplication([])
    app = QApplication(sys.argv)
    app.setStyle('Fusion')
    # qtmodern.styles.dark(app)
    main_win = mainWindow()
    qtmodern.windows.ModernWindow(main_win)
    sys.exit(app.exec_())



